#ifndef _OBJECT_MANAGER_H
#define _OBJECT_MANAGER_H

#include <vector>
#include "gameobject.h"

class ObjectManager {
public:
	void AddObject(GameObject* object);
	void CleanUp();

	void Update(float dt);
	void Render();
protected:
	typedef std::vector<GameObject* >::iterator GameObject_It;
	std::vector<GameObject* > objects_;
};
#endif