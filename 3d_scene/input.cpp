#include "input.h"

float Input::CalculateDrag(){
	int x = (mouse_.drag_x - mouse_.x);
	int y = (mouse_.drag_y - mouse_.y);
	
	return  (float)sqrt((float)((x*x) + (y*y)));
}

//Keyboard
void Input::set_key_down(WPARAM key) {
	keys_[key] =true;
	
}
void Input::set_key_up(WPARAM key) {
	keys_[key] = false;	
}

bool const Input::is_key_down(int key) const {
	return keys_[key];
}

//Mouse
void Input::set_mouse_x(int pos) {
	mouse_.x = pos;
}
void Input::set_mouse_y(int pos) {
	mouse_.y = pos;
}
int const Input::mouse_x() const {
	return mouse_.x;
}
int const Input::mouse_y() const {
	return mouse_.y;
}
// Mouse Drag
void Input::set_drag_x(int pos) {
	mouse_.drag_x = pos;
}
void Input::set_drag_y(int pos) {
	mouse_.drag_y = pos;
}
int const Input::drag_x() const {
	return mouse_.drag_x;
}
int const Input::drag_y() const {
	return mouse_.drag_y;
}
// Mouse Clicks
void Input::set_mouse_left_down(bool state) {
	mouse_.left = state;
}
void Input::set_mouse_right_down(bool state) {
	mouse_.right = state;
}
bool const Input::is_mouse_left_down() const {
	return mouse_.left;
}
bool const Input::is_mouse_right_down() const {
	return mouse_.right;
}
void Input::set_mouse_double_left_clicked(bool state) {
	mouse_.double_left = state;
}
void Input::set_mouse_double_right_clicked(bool state) {
	mouse_.double_right = state;
}
bool const Input::is_mouse_double_left_clicked() const {
	return mouse_.double_left;
}
bool const Input::is_mouse_double_right_clicked() const {
	return mouse_.double_right;
}
