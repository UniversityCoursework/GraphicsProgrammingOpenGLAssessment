#ifndef _FREE_CAMERA_H
#define _FREE_CAMERA_H

#include <windows.h>

#include "vector3.h"

#include "camera.h"

class FreeCam : public Camera {
public:
	FreeCam();
	FreeCam(LittleLot::Vector3 position, LittleLot::Vector3 rotation);
	
	void Update(float dt) override;

	void OnActivated() override;

protected:
	void ControlUpdate(float dt);

	LittleLot::Vector3 reset_position_;
	LittleLot::Vector3 reset_rotation_;


};

#endif
