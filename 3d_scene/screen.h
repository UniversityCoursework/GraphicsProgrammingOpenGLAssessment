#ifndef _SCREEN_H
#define _SCREEN_H

#include <atomic>
// predeclare so not included on every .h call
class ScreenManager;
class Camera;
class Font;

enum State {	
	kActive,
	kOverlayed,
	kHidden
};

class Screen {
public:	
	Screen(ScreenManager *screen_manager);

	virtual ~Screen(){};
	virtual void Init() = 0;
	virtual void CleanUp() = 0;
	
	// Allows each screen to decide if it should update anything when it is overlayed, 
	// i.e. continue loading assets for a load screen overlay.
	// or disable input if a menu screen, maybe stop rendering ui. in the game
	// If false, no updates or renders will be called.
	virtual void Pause(bool is_overlayed);
	virtual void Resume();
	virtual void Resize() = 0;

	virtual bool IsLoading(float &percentage) { return loading_; }

	virtual void Update(float dt) = 0;
	virtual void Render(Font &font) = 0;
	
	const State GetState(){ return state_; }

	
	void set_needs_deleted(bool val) { needs_deleted_ = val; }
	bool const needs_deleted() const { return needs_deleted_; }
protected:
	State state_;
	ScreenManager *screen_manager_;

	// Sets up 3D scene, should really only be 1 3d render at a time. Or create own opengl context...if you want to open that can of worms.
	// But allows easy overlay of 2D elements on lower 3d screen.
	void Begin3D(Camera &camera);
	// Sets render mode to 2DOrthographic, works in screen coordinates.
	void Begin2D();
	// Resets changed states.
	void End2D();

	bool needs_deleted_;
	std::atomic<bool> loading_;
	std::atomic<float> percentage_;
	
};
#endif