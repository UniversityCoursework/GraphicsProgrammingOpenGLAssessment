//Windows Example Code


// Includes
#include <windows.h>
#include <Windowsx.h> // for GET_X_LPARAM GET_Y_LPARAM for working with multiple monitors?
#include <stdio.h>
#include <tchar.h> // _T in wndclassex declartions, and CreateWindow() Expects wide character string

#include "input.h"
#include "scene3d.h"
#include "timer.h"

// Globals
HWND hwnd_;

Scene3D scene;
Timer timer;
Input input_;

//Prototypes
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void RegisterMyWindow(HINSTANCE);
BOOL InitialiseMyWindow(HINSTANCE, int);
int WINAPI WinMain(HINSTANCE, HINSTANCE, PSTR, int);


// Defines the window
void RegisterMyWindow(HINSTANCE hInstance) {

	WNDCLASSEX  wcex;

	wcex.cbSize = sizeof (wcex);							// size of byte structure
	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;		// class style (redraw on Height/Width change)
	wcex.lpfnWndProc = WndProc;								// pointer to window procedure
	wcex.cbClsExtra = 0;									// extra bytes
	wcex.cbWndExtra = 0;									
	wcex.hInstance = hInstance;								// handle to instance containing wndproc
	wcex.hIcon = 0;											// window icon
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);				// cursor icon
	wcex.hbrBackground = NULL;								// NULL allows OpenGL to render on resize, as a brush will clear the buffer //(HBRUSH)(COLOR_WINDOW + 1);		// background color
	wcex.lpszMenuName = NULL;								// menu resource
	wcex.lpszClassName = _T("3D APP");			// window classname
	wcex.hIconSm = 0;										// handle to small icon

	RegisterClassEx(&wcex);									// register the window
}

// Attempts to create the window and display it
BOOL InitialiseMyWindow(HINSTANCE hInstance, int nCmdShow) {
	


	hwnd_ = CreateWindow(
		_TEXT("3D APP"),					// class name( same as registered (wcex.lpszClassName) )
		_TEXT("3D APP"),					// window title 
		WS_OVERLAPPEDWINDOW,					// style of window
		CW_USEDEFAULT,							// position horizontal
		CW_USEDEFAULT,							// position vertical
		800,									// width
		600,									// height
		NULL,									// parent window
		NULL,									// handle to window
		hInstance,								// handle to instance associated with window
		NULL);									// pointer of value to pass to window
	if (!hwnd_) {
		return FALSE;
	}
	ShowWindow(hwnd_, nCmdShow);
	UpdateWindow(hwnd_);
	return TRUE;

}

// Entry point. The program start here
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int nCmdShow) {
	
	MSG         msg;
	RegisterMyWindow(hInstance);

	if (!InitialiseMyWindow(hInstance, nCmdShow))
		return FALSE;

	// passes the address of hwnd_ and input_ to the scene class, 
	// means shared edits/values with everything else that use input_
	scene.Initialise(&hwnd_, &input_);
	timer.Initialize();
	
	while (TRUE) {

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			// update Delta Time
			timer.Frame();

			// Window User input_ should go here?
			if (input_.is_key_down(VK_ESCAPE)) {		
				ShowCursor(true);
				int resp = MessageBox(hwnd_, _TEXT("Quit"), _TEXT("You sure?"), MB_YESNO);
				if (resp == IDYES) {
					return 0;
				}				
				input_.set_key_up(VK_ESCAPE);
				ShowCursor(false);
			}

			//toggle fullscreen on double left click
			if (input_.is_mouse_double_left_clicked()) {
				//toggle fullscreen	
				scene.ToggleFullScreen();
				input_.set_mouse_double_left_clicked(false);
			}
			// update & render
			scene.Update(timer.time());			
		}
	}
	
	return msg.wParam;
}

// handles window messages				
LRESULT CALLBACK WndProc(HWND hwnd_, UINT message, WPARAM wParam, LPARAM lParam) {

	switch (message) {

		case WM_CREATE:
			break;
		case WM_SIZE:
			scene.Resize();
			break;
		
		case WM_KEYDOWN:
			input_.set_key_down(wParam);
			break;
		case WM_KEYUP:
			input_.set_key_up(wParam);
			break;
			
		
		case WM_MOUSEMOVE:
			input_.set_mouse_x(GET_X_LPARAM(lParam));
			input_.set_mouse_y(GET_Y_LPARAM(lParam));
			break;

		case WM_LBUTTONDOWN:
			input_.set_mouse_left_down(true);			
			break;
		case WM_LBUTTONUP:
			input_.set_mouse_left_down(false);			
			break;

		case WM_RBUTTONDOWN:
			input_.set_mouse_right_down(true);
			break;
		case WM_RBUTTONUP:
			input_.set_mouse_right_down(false);
			break;

		case WM_LBUTTONDBLCLK:
			input_.set_mouse_double_left_clicked(true);
			break;
		case WM_RBUTTONDBLCLK:
			input_.set_mouse_double_right_clicked(true);
			break;
		
		case WM_DESTROY:
			PostQuitMessage(WM_QUIT);
			break;
	}

	return DefWindowProc(hwnd_, message, wParam, lParam);

}

