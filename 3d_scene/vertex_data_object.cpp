#include "vertex_data_object.h"

VertexDataObject::VertexDataObject() {
	vertex_data_ = nullptr;
	texture_ = nullptr;
}

VertexDataObject::VertexDataObject(VertexData * vertex_data, Texture *texture, Material material, LittleLot::Vector3 position, LittleLot::Vector3 rotation, LittleLot::Vector3 scale) {
	vertex_data_ = vertex_data;
	material_ = material;
	position_ = position;
	rotation_ = rotation;
	scale_ = scale;
	texture_ = texture;
}

VertexDataObject::VertexDataObject(VertexData * vertex_data, Texture *texture, Material material, LittleLot::Vector3 position, LittleLot::Vector3 rotation, float scale)
:VertexDataObject(vertex_data, texture,material,position,rotation,LittleLot::Vector3(scale,scale,scale)){
}

VertexDataObject::~VertexDataObject() {
	vertex_data_ = nullptr;
	texture_ = nullptr;
}

void VertexDataObject::Render() {
	glEnable(GL_NORMALIZE);

	glPushMatrix();
	if(texture_) {
		glBindTexture(GL_TEXTURE_2D, texture_->data());
	}
	material_.Apply();
	glTranslatef(position_.x, position_.y, position_.z);	
	glRotatef(rotation_.x, 1, 0, 0);
	glRotatef(rotation_.y, 0, 1, 0);
	glRotatef(rotation_.z, 0, 0, 1);
	glScalef(scale_.x, scale_.y, scale_.z);	
	RenderVertexData(*vertex_data_);
	ApplyDefaultMaterial();
	if(texture_) {
		glBindTexture(GL_TEXTURE_2D, NULL);
	}
	glPopMatrix();

	glDisable(GL_NORMALIZE);
}
