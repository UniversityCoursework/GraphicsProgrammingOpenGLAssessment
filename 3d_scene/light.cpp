#include "light.h"

Light::Light() {	
	light_id_ = GL_LIGHT0;
	set_ambient(0.2f, 0.2f, 0.2f, 0.6f);
	set_diffuse(1.0f, 1.0f, 1.0f, 1.0f);
	set_specular(1.0f, 1.0f, 1.0f, 1.0f);
	set_position(0, 0, 0);

	set_constant_attenuation(1.0f);
	set_linear_attenuation(.0f);
	set_quadratic_attenuation(0.0f);

	position_[3] = 1.0f;	
	light_type_ = kPoint;
	debug_render_ = false;
}

Light::Light(LightNumber light_number, LightType light_type, float x, float y, float z)
	:Light(){
	light_id_ = GL_LIGHT0 + light_number;		
	set_position(x, y, z);
	light_type_ = light_type;
	switch(light_type) {
		case kSpot:
			position_[3] = 1.0f;
			set_spot_direction(0.f, -1.0f, 0.f);
			set_spot_exponent(50.f);
			set_spot_cutoff(25.f);			
			break;
		case kDirectional:
			position_[3] = 0.0f;			
			break;
		case kPoint:
			position_[3] = 1.0f;
			break;
		default:			
			break;
	}	
	debug_render_ = false;
}

void Light::Render() {	
	glLightfv(light_id_, GL_AMBIENT, ambient_);
	glLightfv(light_id_, GL_DIFFUSE, diffuse_);
	glLightfv(light_id_, GL_SPECULAR, specular_);
	glLightfv(light_id_, GL_POSITION, position_);

	glLightf(light_id_, GL_CONSTANT_ATTENUATION, constant_attenuation_);
	glLightf(light_id_, GL_LINEAR_ATTENUATION, linear_attenuation_);
	glLightf(light_id_, GL_QUADRATIC_ATTENUATION, quadratic_attenuation_);
	if(light_type_ == kSpot) {
		glLightf(light_id_, GL_SPOT_CUTOFF, spot_cutoff_);
		glLightfv(light_id_, GL_SPOT_DIRECTION, spot_direction_);
		glLightf(light_id_, GL_SPOT_EXPONENT, spot_exponent_);
	}
	// Render a debug light sphere at the position of the light?
	if(debug_render_) {
		glPushMatrix();
		// TO DO: really need a Unlit Material so can always see it as a white sphere...
		GLfloat emission[] = {1.0f,1.0f,1.0f,1.0f};
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, diffuse_);		

		glTranslatef(position_[0], position_[1], position_[2]);
		gluSphere(gluNewQuadric(), 0.20, 10, 10);
		glTranslatef(0, -0.2f, 0);
		gluSphere(gluNewQuadric(), 0.10, 10, 10);
		GLfloat reset_emission[] = {0.0f,0.0f,0.0f,0.0f};
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, reset_emission);
		glPopMatrix();
	}	
}

// setters etc.

void Light::set_ambient(float r, float g, float b, float a) {
	ambient_[0] = r;
	ambient_[1] = g;
	ambient_[2] = b;
	ambient_[3] = a;
}

void Light::set_diffuse(float r, float g, float b, float a) {
	diffuse_[0] = r;
	diffuse_[1] = g;
	diffuse_[2] = b;
	diffuse_[3] = a;
}

void Light::set_specular(float r, float g, float b, float a) {
	specular_[0] = r;
	specular_[1] = g;
	specular_[2] = b;
	specular_[3] = a;
}

void Light::set_position(float x, float y, float z) {
	position_[0] = x;
	position_[1] = y;
	position_[2] = z;	
}

void Light::set_spot_direction(float x, float y, float z) {
	spot_direction_[0] = x;
	spot_direction_[1] = y;
	spot_direction_[2] = z;
}

void Light::set_spot_exponent(float exponent) {
	spot_exponent_ = exponent;
}

void Light::set_spot_cutoff(float cutoff) {
	spot_cutoff_ = cutoff;
}

void Light::set_constant_attenuation(float attenuation) {
	constant_attenuation_ = attenuation;
}

void Light::set_linear_attenuation(float attenuation) {
	linear_attenuation_ = attenuation;
}

void Light::set_quadratic_attenuation(float attenuation) {
	quadratic_attenuation_ = attenuation;
}

void Light::enable() {
	glEnable(light_id_);
}

void Light::disable() {
	glDisable(light_id_);
}

void Light::Debug(bool val) {
	debug_render_ = val;
}

