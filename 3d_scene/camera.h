#ifndef _CAMERA_H
#define _CAMERA_H

#include <windows.h>
#include "vector3.h"

class CameraManager;

class Camera {
public:
	LittleLot::Vector3 position() { return position_; }
	void set_position(LittleLot::Vector3 position) {
		position_ = position;
		update_required_ = true;
	}
	LittleLot::Vector3 forward() const { return forward_; }
	LittleLot::Vector3 look_at() const { return look_at_; }
	void set_look_at(LittleLot::Vector3 look_at) { look_at_ = look_at; }
	LittleLot::Vector3 up() const { return up_; }
	LittleLot::Vector3 right() const { return right_; }

	LittleLot::Vector3 rotation() const { return rotation_; }
	void set_rotation(LittleLot::Vector3 rotation) {
		rotation_ = rotation;
		update_required_ = true;
	}
	float pitch() const { return rotation_.x; }
	void set_pitch(float rotation) {
		rotation_.x = rotation;
		update_required_ = true;
	}
	float yaw() const { return rotation_.y; }
	void set_yaw(float rotation) {
		rotation_.y = rotation;
		update_required_ = true;
	}
	float roll() const { return rotation_.z; }
	void set_roll(float rotation) {
		rotation_.z = rotation;
		update_required_ = true;
	}

	float rotation_speed() const { return rotation_speed_; }
	void set_rotation_speed(float val) { rotation_speed_ = val; }
	float move_speed() const { return move_speed_; }
	void set_move_speed(float val) { if(val>0) move_speed_ = val; }
	
	virtual void Update(float dt) = 0;

	virtual void OnActivated()=0;

	void SetOwner(CameraManager *owner) { owner_ = owner; }
protected:
	void MoveForward(float dt);
	void MoveBack(float dt);
	void MoveLeft(float dt);
	void MoveRight(float dt);

	void MoveUp(float dt);
	void MoveDown(float dt);
	void MoveUpY(float dt);
	void MoveDownY(float dt);

	void YawLeft(float dt);
	void YawRight(float dt);
	void PitchUp(float dt);
	void PitchDown(float dt);
	void RollLeft(float dt);
	void RollRight(float dt);

	void MouseYawPitch(int mouse_x, int mouse_y, RECT screen_rect, float dt);
	void ResetMouseToCentreScreen(HWND &hwnd_, RECT screen_rect);

	LittleLot::Vector3 position_;
	LittleLot::Vector3 forward_;
	LittleLot::Vector3 look_at_;
	LittleLot::Vector3 up_;
	LittleLot::Vector3 right_;
	LittleLot::Vector3 rotation_;
	bool update_required_;

	float move_speed_;
	float rotation_speed_;

	CameraManager *owner_;
};

#endif
