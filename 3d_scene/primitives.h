#ifndef _PRIMITIVES_H
#define _PRIMITIVES_H

#include <vector>

#include "vertex_data.h"

VertexData BuildCube();
VertexData BuildSkyBox();
VertexData BuildQuad();
VertexData BuildPlane(int width, int height);
VertexData BuildDisc(int resolution);
// Builds a sphere using long/lat slices, with resolution = number of long/lat
VertexData BuildSphere(int resolution);

// Uses Icosahedron as start point, then sub-divides these triangles to the required depth.
// Maps to rectangular texture...
VertexData BuildGeodesicSphere(int depth);

// Uses Cube as start point and Subdivides Quads to required depth, then generates triangles for these quads.
// Maps to CubeMap texture.
VertexData BuildCubeSphere(int depth);

#endif
