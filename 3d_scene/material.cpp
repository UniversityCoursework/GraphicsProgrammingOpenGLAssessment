#include "material.h"

void ApplyDefaultMaterial() {
	GLfloat ambient[] = {0.2f,0.2f,0.2f,1.0f};
	GLfloat diffuse[] = {0.8f, 0.8f, 0.8f, 1.0f};
	GLfloat specular[] = {.0f, .0f, .0f, 1.0f};
	GLfloat shininess[] = {0.f};
	GLfloat emission[] = {0.0f, 0.0f, 0.0f, 1.0f};

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emission);
}

Material::Material() {
	SetByDefault(kDefault);
}

Material::Material(DefaultMaterials selection) {
	SetByDefault(selection);
}

void Material::SetByDefault(DefaultMaterials selection) {
	face_ = GL_FRONT_AND_BACK;
	// default no emission
	set_emission(0.f,0.f,0.f,0.f);
	switch(selection) {
		case kDefault:
			set_ambient(0.2f,0.2f,0.2f,1.0f);
			set_diffuse(0.8f,0.8f,0.8f,1.0f);
			set_specular(.0f,.0f,.0f, 1.f);
			set_shininess(0.f);
			break;
		case kBrass:
			set_ambient(0.329412f, 0.223529f, 0.027451f, 1.f);
			set_diffuse(0.780392f, 0.568627f, 0.113725f, 1.f);
			set_specular(0.992157f, 0.941176f, 0.807843f, 1.f);
			set_shininess(27.8974f);
			break;
		case kBronze:
			set_ambient(0.2125f, 0.1275f, 0.054f, 1.f);
			set_diffuse(0.714f, 0.4284f, 0.18144f, 1.f);
			set_specular(0.393548f, 0.271906f, 0.166721f, 1.f);
			set_shininess(25.6f);
			break;
		case kPolishedBronze:
			set_ambient(0.25f, 0.148f, 0.06475f, 1.f);
			set_diffuse(0.4f, 0.2368f, 0.1036f, 1.f);
			set_specular(0.774597f, 0.458561f, 0.200621f, 1.f);
			set_shininess(76.8f);
			break;
		case kChrome:
			set_ambient(0.25f, 0.25f, 0.25f, 1.f);
			set_diffuse(0.4f, 0.4f, 0.4f, 1.f);
			set_specular(0.774597f, 0.774597f, 0.774597f, 1.f);
			set_shininess(76.8f);
			break;
		case kCopper:
			set_ambient(0.19125f, 0.0735f, 0.0255f, 1.f);
			set_diffuse(0.7038f, 0.27048f, 0.0828f, 1.f);
			set_specular(0.256777f, 0.137622f, 0.086014f, 1.f);
			set_shininess(12.8f);
			break;
		case kGold:
			set_ambient(0.24725f, 0.1995f, 0.0745f, 1.f);
			set_diffuse(0.75164f, 0.60648f, 0.22648f, 1.f);
			set_specular(0.628281f, 0.555802f, 0.366065f, 1.f);
			set_shininess(51.2f);
			break;
		case kPewter:
			set_ambient(0.105882f, 0.058824f, 0.113825f, 1.f);
			set_diffuse(0.427451f, 0.470588f, 0.541176f, 1.f);
			set_specular(0.333333f, 0.333333f, 0.521569f, 1.f);
			set_shininess(9.84615f);
			break;
		case kSilver:
			set_ambient(0.19225f, 0.19225f, 0.19225f, 1.f);
			set_diffuse(0.50754f, 0.50754f, 0.50754f, 1.f);
			set_specular(0.508273f, 0.508273f, 0.508273f, 1.f);
			set_shininess(51.2f);
			break;
		case kEmerald:
			set_ambient(0.0215f, 0.1745f, 0.0215f, 0.55f);
			set_diffuse(0.07568f, 0.61424f, 0.07568f, 0.55f);
			set_specular(0.633f, 0.727811f, 0.633f, 0.55f);
			set_shininess(76.8f);
			break;
		case kJade:
			set_ambient(0.135f, 0.2225f, 0.1575f, 0.95f);
			set_diffuse(0.54f, 0.89f, 0.63f, 0.95f);
			set_specular(0.316228f, 0.316228f, 0.316288f, 0.95f);
			set_shininess(12.8f);
			break;
		case kObsidian:
			set_ambient(0.05375f, 0.05f, 0.06625f, 0.82f);
			set_diffuse(0.18275f, 0.17f, 0.22525f, 0.82f);
			set_specular(0.332741f, 0.328634f, 0.346435f, 0.82f);
			set_shininess(38.4f);
			break;
		case kRuby:
			set_ambient(0.1745f, 0.01175f, 0.01175f, 0.55f);
			set_diffuse(0.61424f, 0.04136f, 0.04136f, 0.55f);
			set_specular(0.727811f, 0.626959f, 0.626959f, 0.55f);
			set_shininess(76.8f);
			break;
		case kTurquoise:
			set_ambient(0.1f, 0.18725f, 0.1745f, 0.8f);
			set_diffuse(0.396f, 0.74151f, 0.69102f, 0.8f);
			set_specular(0.297254f, 0.30829f, 0.306678f, 0.8f);
			set_shininess(12.8f);
			break;
		case kBlackPlastic:
			set_ambient(0.f, 0.f, 0.f, 1.f);
			set_diffuse(0.01f, 0.01f, 0.01f, 1.f);
			set_specular(0.5f, 0.5f, 0.5f, 1.f);
			set_shininess(32.f);
			break;
		case kBlackRubber:
			set_ambient(0.02f, 0.02f, 0.02f, 1.f);
			set_diffuse(0.01f, 0.01f, 0.01f, 1.f);
			set_specular(0.5f, 0.5f, 0.5f, 1.f);
			set_shininess(10.f);
			break;		
		default:
			break;
	}
}

void Material::Apply() {
	glMaterialfv(face_, GL_AMBIENT, ambient_);
	glMaterialfv(face_, GL_DIFFUSE, diffuse_);
	glMaterialfv(face_, GL_SPECULAR, specular_);
	glMaterialfv(face_, GL_SHININESS, &shininess_);
	glMaterialfv(face_, GL_EMISSION, emission_);
}

void Material::set_face(GLenum face) {
	face_ = face;
}

void Material::set_ambient(float r, float g, float b, float a) {
	ambient_[0] = r;
	ambient_[1] = g;
	ambient_[2] = b;
	ambient_[3] = a;
}

void Material::set_diffuse(float r, float g, float b, float a) {
	diffuse_[0] = r;
	diffuse_[1] = g;
	diffuse_[2] = b;
	diffuse_[3] = a;
}

void Material::set_specular(float r, float g, float b, float a) {
	specular_[0] = r;
	specular_[1] = g;
	specular_[2] = b;
	specular_[3] = a;
}

void Material::set_emission(float r, float g, float b, float a) {
	emission_[0] = r;
	emission_[1] = g;
	emission_[2] = b;
	emission_[3] = a;
}

void Material::set_shininess(float shininess) {
	shininess_ = shininess;
}

Material & Material::operator=(const Material & other) {
	ambient_[0] = other.ambient_[0];
	ambient_[1] = other.ambient_[1];
	ambient_[2] = other.ambient_[2];
	ambient_[3] = other.ambient_[3];

	diffuse_[0] = other.diffuse_[0];
	diffuse_[1] = other.diffuse_[1];
	diffuse_[2] = other.diffuse_[2];
	diffuse_[3] = other.diffuse_[3];

	specular_[0] = other.specular_[0];
	specular_[1] = other.specular_[1];
	specular_[2] = other.specular_[2];
	specular_[3] = other.specular_[3];

	emission_[0] = other.emission_[0];
	emission_[1] = other.emission_[1];
	emission_[2] = other.emission_[2];
	emission_[3] = other.emission_[3];
	
	shininess_ = other.shininess_;

	face_ = other.face_;
	return *this;
}
