#ifndef _SECURITY_CAMERA_H
#define _SECURITY_CAMERA_H

#include <windows.h>

#include "vector3.h"

#include "camera.h"

class SecurityCam : public Camera {
public:
	SecurityCam(LittleLot::Vector3 position, LittleLot::Vector3 lower_limit, LittleLot::Vector3 upper_limit, bool is_controlled = true);
	SecurityCam(LittleLot::Vector3 position, LittleLot::Vector3 lower_limit, LittleLot::Vector3 upper_limit, float lerp_amount,float delay, bool is_controlled = false);
	
	bool is_controlled() { return is_controlled_; }
	void set_is_controlled(bool val) { is_controlled_ = val; }
	
	void Update(float dt) override;

	void OnActivated() override;

protected:
	LittleLot::Vector3 lower_rotation_;
	LittleLot::Vector3 upper_rotation_;	
	float delay_;
	float delay_timer_;
	float lerp_amount_;
	float lerp_current_;

	bool is_controlled_;		
};

#endif

