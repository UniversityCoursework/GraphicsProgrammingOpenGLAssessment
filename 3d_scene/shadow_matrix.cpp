#include "shadow_matrix.h"

void GenerateShadowMatrix(float matrix[16], float light_pos[4], LittleLot::Vector3 P, LittleLot::Vector3 Q, LittleLot::Vector3 R) {

	LittleLot::Vector3 PQ = (Q - P);
	PQ.Normalise();
	LittleLot::Vector3 PR = (R - P);
	PR.Normalise();
	LittleLot::Vector3 normal = PR.CrossProduct(PQ);

	//Equation of plane is ax + by + cz = d
	//a, b and c are the coefficients of the normal to the plane (i.e. normal = ai + bj + ck)
	//If (x0, y0, z0) is any point on the plane, d = a*x0 + b*y0 + c*z0
	//i.e. d is the dot product of any point on the plane (using P here) and the normal to the plane
	float a, b, c, d;
	a = normal.x;
	b = normal.y;
	c = normal.z;
	d = normal.DotProduct(P);

	//Origin of projection is at x, y, z. Projection here originating from the light source's position
	float x, y, z;

	x = light_pos[0];
	y = light_pos[1];
	z = light_pos[2];

	//This is the general perspective transformation matrix from a point (x, y, z) onto the plane ax + by + cz = d
	matrix[0] = d - (b * y + c * z);
	matrix[1] = a * y;
	matrix[2] = a * z;
	matrix[3] = a;

	matrix[4] = b * x;
	matrix[5] = d - (a * x + c * z);
	matrix[6] = b * z;
	matrix[7] = b;

	matrix[8] = c * x;
	matrix[9] = c * y;
	matrix[10] = d - (a * x + b * y);
	matrix[11] = c;

	matrix[12] = -d * x;
	matrix[13] = -d * y;
	matrix[14] = -d * z;
	matrix[15] = -(a * x + b * y + c * z);
}
// requires use of EndShadowRender to finalise settings, (i.e. reset stuff)
void BeginShadowRender(Light &light, LittleLot::Vector3 plane_1, LittleLot::Vector3 plane_2, LittleLot::Vector3 plane_3) {
	float shadow_matrix[16];
	GLfloat light_pos[4];
	light.get_position(light_pos);

	GenerateShadowMatrix(shadow_matrix, light_pos, plane_1, plane_2, plane_3);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
		
	glPushMatrix();	
	glMultMatrixf((GLfloat *)shadow_matrix);	
}

void EndShadowRender() {	
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);	
}
