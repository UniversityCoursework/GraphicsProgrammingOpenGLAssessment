#include "pause_screen.h"

#include "screen_manager.h"
#include "font.h"
#include "utils.h"

enum ButtonName {
	kResume = 0,	
	kExit
};

const float button_width = 180;
const float button_height = 30;
const float button_offset = 62;
const float title_offset = 120;

PauseScreen::PauseScreen(ScreenManager * screen_manager)
:MenuScreen(screen_manager){
}

PauseScreen::~PauseScreen() {
}

void PauseScreen::Init() {
	background_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
	background_.set_width((float)screen_manager_->ScreenWidth() / 2.f);
	background_.set_height((float)screen_manager_->ScreenHeight() / 4.f);

	Colour bg_colour;
	bg_colour.set_rgba_255(93, 19, 114,0.8f);
	background_.set_colour(bg_colour);

	Colour button_bg, button_highlight;
	button_bg.set_rgba_255(121, 190, 0);
	button_highlight.set_rgba(0.5f, 0.5f, 0.5f, 0.6f);

	current_button_ = 0;
	buttons_.push_back(Button(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f - button_offset), button_width, button_height, button_bg, button_highlight, "Resume"));
	buttons_.push_back(Button(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f), button_width, button_height, button_bg, button_highlight, "Exit"));
}

void PauseScreen::CleanUp() {
}

void PauseScreen::Resize() {
	background_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
	background_.set_width((float)screen_manager_->ScreenWidth() / 2.f);
	background_.set_height((float)screen_manager_->ScreenHeight() / 4.f);

	buttons_[kResume].set_position(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f - button_offset));
	buttons_[kExit].set_position(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f));
	
}

void PauseScreen::Update(float dt) {
	switch(state_) {
		case kActive: {
				UpdateMenu(dt);
				break;
			}
		case kOverlayed: {
				// any background animations, updates etc.
				break;
			}
		default:
			break;
	}
}

void PauseScreen::Render(Font & font) {
	switch(state_) {
		case kActive:
			Begin2D();
			background_.DrawSprite();			
			font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() / 2.f, 10), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Pause");
			RenderButtons(font);
			End2D();
			break;
		case kOverlayed:
			break;
		default:
			break;
	}

}

void PauseScreen::UseButton(int index) {
	switch(ButtonName(index)) {
		case kResume: {
				screen_manager_->PopScreen();
				break;
			}
		case kExit: {
				screen_manager_->PopScreen(2);
				break;
			}
		default:
			break;
	}
}
