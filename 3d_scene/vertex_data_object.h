#ifndef _VERTEX_DATA_OBJECT_H
#define _VERTEX_DATA_OBJECT_H

#include "vector3.h"

#include "model.h"
#include "vertex_data.h"
#include "gameobject.h"
#include "texture.h"
#include "material.h"

class VertexDataObject : public GameObject {
public:
	VertexDataObject();
	VertexDataObject(VertexData* vertex_data, Texture *texture,Material material, LittleLot::Vector3 position, LittleLot::Vector3 rotation, LittleLot::Vector3 scale);
	VertexDataObject(VertexData* vertex_data, Texture *texture,Material material, LittleLot::Vector3 position, LittleLot::Vector3 rotation, float scale);
	~VertexDataObject();

	void set_vertex_data(VertexData *vertex_data) { vertex_data_ = vertex_data; }
	VertexData* const vertex_data() const { return vertex_data_; }

	void set_texture(Texture *texture) { texture_ = texture; }
	Texture* const texture() const { return texture_; }

	void set_material(Material material) { material_ = material; }
	Material const material() const { return material_; }

	virtual void Render() override;
protected:
	VertexData *vertex_data_;
	Texture* texture_;
	Material material_;
};

#endif
