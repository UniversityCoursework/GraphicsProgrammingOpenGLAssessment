#ifndef _COLOUR_H
#define _COLOUR_H

class Colour {
public:
	Colour() {
		r_ = 1.f, g_ = 1.f, b_ = 1.f, a_ = 1.f;
	}
	inline Colour::Colour(const float r, const float g, const float b, const float a = 1.f) {
		r_ = r;
		g_ = g;
		b_ = b;
		a_ = a;
	}
	
	float const r() const { return r_; }
	float const g() const { return g_; }
	float const b() const { return b_; }
	float const a() const { return a_; }
	void set_rgba(float r, float g, float b, float a = 1.0f) {
		r_ = r;
		g_ = g;
		b_ = b;
		a_ = a;
	}
	// divides rgb values by 255, allows using numbers straight from adobe etc.
	// alpha still 0->1
	void set_rgba_255(float r, float g, float b, float a = 1.0f) {
		r_ = r / 255.f;
		g_ = g / 255.f;
		b_ = b / 255.f;
		a_ = a;
	}
protected:
	float r_, g_, b_, a_;
};


#endif
