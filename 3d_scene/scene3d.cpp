#include "Scene3D.h"

#include <iostream>     // std::cout
#include <sstream>      // std::stringstream
#include "math_utils.h"

#include "start_screen.h"

Scene3D::~Scene3D() {	
	screen_manager_.CleanUp();
	
}
bool Scene3D::CreatePixelFormat(HDC hdc_) {
	PIXELFORMATDESCRIPTOR pfd = { 0 };
	int pixelformat;

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);	// Set the size of the structure
	pfd.nVersion = 1;							// Always set this to 1
												// Pass in the appropriate OpenGL flags
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.dwLayerMask = PFD_MAIN_PLANE;			// standard mask (this is ignored anyway)
	pfd.iPixelType = PFD_TYPE_RGBA;				// RGB and Alpha pixel type
	pfd.cColorBits = COLOUR_DEPTH;				// Here we use our #define for the color bits
	pfd.cDepthBits = COLOUR_DEPTH;				// Ignored for RBA
	pfd.cAccumBits = 0;							// nothing for accumulation
	pfd.cStencilBits = COLOUR_DEPTH;						// nothing for stencil

												//Gets a best match on the pixel format as passed in from device
	if ((pixelformat = ChoosePixelFormat(hdc_, &pfd)) == false) {
		MessageBox(NULL, _TEXT("ChoosePixelFormat failed"), _TEXT("Error"), MB_OK);
		return false;
	}

	//sets the pixel format if its ok. 
	if (SetPixelFormat(hdc_, pixelformat, &pfd) == false) {
		MessageBox(NULL, _TEXT("SetPixelFormat failed"), _TEXT("Error"), MB_OK);
		return false;
	}
	
	return true;
}

void Scene3D::ResizeGLWindow(int width, int height) { // Initialize The GL Window
	if (height == 0) {// Prevent a Divide By Zero error
		height = 1;// Make the Height Equal One
	}

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//calculate aspect ratio
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.01f, 1500.0f);

	glMatrixMode(GL_MODELVIEW);// Select The Modelview Matrix
	glLoadIdentity();// Reset The Modelview Matrix
}

void Scene3D::InitializeOpenGL(int width, int height) {
	hdc_ = GetDC(*hwnd_);//  sets  global HDC

	if (!CreatePixelFormat(hdc_))//  sets  pixel format
		PostQuitMessage(0);

	hrc_ = wglCreateContext(hdc_);	//  creates  rendering context from  hdc_
	wglMakeCurrent(hdc_, hrc_);		//	Use this HRC.

	ResizeGLWindow(width, height);	// Setup the Screen
}

void Scene3D::Initialise(HWND* wnd, Input* in) {	
	hwnd_ = wnd;
	input_ = in;

	GetClientRect(*hwnd_, &screen_rect_);	//get rect into our handy global rect
	InitializeOpenGL(screen_rect_.right, screen_rect_.bottom); // initialise openGL

														   //OpenGL settings
	glShadeModel(GL_SMOOTH);							// enable Smooth Shading
	glClearColor(0.39f, 0.58f, 93.0f, 1.0f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// set up blend mode
														//Also, do any other setting variables here for your app if you wish.

	glEnable(GL_LIGHTING);	

	SetVSync(true);

	wireframe_render = false;
	readable_frame_rate_ = -1;
	font_.Load("bin/fonts/old_face");
	
	screen_manager_.Init(input_, &screen_rect_, hwnd_,hrc_,hdc_);

	Screen* game = new StartScreen(&screen_manager_);
	screen_manager_.PushScreen(game);

}

void Scene3D::Update(float dt) {

	readable_frame_rate_ -= dt;
	if (readable_frame_rate_ < 0) {		
		// convert to fps, not fpms
		frame_rate_ = 1000.0f / dt;
		ticks_ = dt;
		readable_frame_rate_ = 100.0f;		
	}	
	// Do important update here
	// Handle user input_, update variables etc	
	if (input_->is_key_down(VK_F11)) {
		ToggleWireFrame();
		UpdateRenderMode();
		input_->set_key_up(VK_F11);
	}
	if(input_->is_key_down('F')) {
		ToggleCullFace();
		
		UpdateRenderMode();
		input_->set_key_up('F');
	}
	
	screen_manager_.Update(dt);	
	// Render scene once all updating has been complete
	Render();
}

void Scene3D::Render() {	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);	// Clear The Screen, The Depth Buffer, And The Stencil Buffer.
	
	screen_manager_.Render(font_);
	
	// 2D Ortho Hud Rendering
	glDisable(GL_LIGHTING);
	glMatrixMode(GL_PROJECTION);
		// change to ortho for 2d hud
		glLoadIdentity();
		// set to window size
		gluOrtho2D(0, screen_rect_.right, screen_rect_.bottom, 0);
	glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glDisable(GL_DEPTH_TEST);					
		glEnable(GL_BLEND);
		font_.RenderText(LittleLot::Vector2(10, 10), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "FPS: %.2f", frame_rate_);
		font_.RenderText(LittleLot::Vector2(6, 30), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "Ticks: %.3f ms", ticks_);
		font_.RenderText(LittleLot::Vector2(10, 50), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F11 - Toggle WireFrame");
					
		glDisable(GL_BLEND);						
		glEnable(GL_DEPTH_TEST);
		
	
	SwapBuffers(hdc_);				// Swap the frame buffers.	
}

void Scene3D::UpdateRenderMode() {
	// polygon mode
	if(wireframe_render) {
		glPolygonMode(GL_FRONT, GL_LINE);
		glPolygonMode(GL_BACK, GL_LINE);
		
	} else {

		glPolygonMode(GL_FRONT, GL_FILL);
		glPolygonMode(GL_BACK, GL_FILL);
	}
	if(cullface_) {
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	} else {
		glDisable(GL_CULL_FACE);
	}
}

void Scene3D::Resize() {
	if(hwnd_ == NULL)
		return;

	GetClientRect(*hwnd_, &screen_rect_);
	ResizeGLWindow(screen_rect_.right, screen_rect_.bottom);
	screen_manager_.Resize();
	Render();
	
}

void Scene3D::ToggleFullScreen() {

	// get the current window styles
	DWORD dwStyle = GetWindowLong(*hwnd_, GWL_STYLE);
	if ((dwStyle & WS_OVERLAPPEDWINDOW)) {

		SaveWindowPosition();
		SetToFullScreen();		
	}
	else {
		SetToWindowed();
	}
	Resize();	
}

void Scene3D::SaveWindowPosition() {
	// get window position and save it
	GetWindowRect(*hwnd_, &screen_rect_);
	prev_left_ = screen_rect_.left;
	prev_top_ = screen_rect_.top;

	prev_width_ = screen_rect_.right - prev_left_;
	prev_height_ = screen_rect_.bottom - prev_top_;
}

void Scene3D::SetToFullScreen() {
	// get primary monitor
	HMONITOR monitor = MonitorFromWindow(*hwnd_, MONITOR_DEFAULTTONEAREST);
	MONITORINFO info;
	info.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitor, &info);

	int monitor_width = info.rcMonitor.right - info.rcMonitor.left;
	int monitor_height = info.rcMonitor.bottom - info.rcMonitor.top;
	int monitor_left = info.rcMonitor.left;
	int monitor_top = info.rcMonitor.top;

	// change window styles of current window
	SetWindowLongPtr(*hwnd_, GWL_STYLE, WS_POPUP | WS_VISIBLE);

	// change size/position of current window
	SetWindowPos(*hwnd_, 0, monitor_left, monitor_top, monitor_width, monitor_height, SWP_NOZORDER | SWP_NOACTIVATE);
}

void Scene3D::SetToWindowed() {

	SetWindowLongPtr(*hwnd_, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);

	SetWindowPos(*hwnd_, 0, prev_left_, prev_top_, prev_width_, prev_height_, SWP_NOZORDER | SWP_NOACTIVATE);
}

void Scene3D::SetVSync(bool sync) {
	// Function pointer for the wgl extention function we need to enable/disable	
	typedef BOOL(APIENTRY *PFNWGLSWAPINTERVALPROC)(int);
	PFNWGLSWAPINTERVALPROC wglSwapIntervalEXT = 0;
	// try to get the extension
	wglSwapIntervalEXT = (PFNWGLSWAPINTERVALPROC)wglGetProcAddress("wglSwapIntervalEXT");

	// use it if it exists, otherwise meh...
	if (wglSwapIntervalEXT)
		wglSwapIntervalEXT(sync);

}