#ifndef _MATH_UTILS_H
#define _MATH_UTILS_H

#define PI 3.14159265358979323846264338327950288419716939937510f
#include "vector3.h"

#include <algorithm>

namespace LittleLot {

inline float DegreesToRadians(float angleindegrees) {
	return angleindegrees* ((float)PI / 180.0f);
}
inline float RadiansToDegrees(float angleinradians) {
	return angleinradians*(180.0f / (float)PI);
}
/// <summary> Lerps between 2 floats, i.e.Smooth movement </summary>
/// <param name = 'time'> between 0 and 1 </param>
inline float Lerp(float start, float end, float time) {
	// This method is mathematically correct, but due to floating point error the other is prefered. As rounding is done before the multiplication so accuracy is lost?
	//return start + (end - start) *time;

	return start*(1.0f - time) + time*end;
}
// lerps between 2 vectors increment amount time. 
// @param time usaully between 0 and 1
inline Vector3 Lerp(Vector3 start, Vector3 end, float time) {
	Vector3 result;
	result.x = Lerp(start.x, end.x, time);
	result.y = Lerp(start.y, end.y, time);
	result.z = Lerp(start.z, end.z, time);
	return result;
}

// Returns whether negative or positive
// @return -1 or 1
// @return 0 +ve
inline int Sign(float val) {
	if(val >= 0) {
		return 1;
	} else {
		return -1;
	}
}

// Returns a param clamped between two other params
// T must have (std::max),(std::min) operators for ordering written, e.g. float,int, etc.
template <typename T>
T Clamp(const T& n, const T& lower, const T& upper) {
	return (std::max)(lower, (std::min)(n, upper));
}

inline Vector3 ClampBetweenVectors(Vector3 tobeClamped, Vector3 topleft, Vector3 bottomright) {	
	tobeClamped.x = Clamp(tobeClamped.x, topleft.x, bottomright.x);
	tobeClamped.y = Clamp(tobeClamped.y, topleft.y, bottomright.y);
	tobeClamped.z = Clamp(tobeClamped.z, topleft.z, bottomright.z);
	return tobeClamped;
}

}
#endif
