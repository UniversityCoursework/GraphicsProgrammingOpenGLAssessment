#ifndef _CHARACTER_CONTROLLER_H
#define _CHARACTER_CONTROLLER_H
#include "model_object.h"
#include "input.h"
#include "utils.h"
#include "math_utils.h"

class CharacterController {
public:	
	CharacterController();
	CharacterController(ModelObject *model, float movespeed, float rotation_speed_, Input *input, HWND* hwnd_, RECT *rect_);
	~CharacterController();

	void UpdateInput(float dt);
	void SetCharacterBounds(LittleLot::Vector3 start, LittleLot::Vector3 end);

	LittleLot::Vector3 forward() const { return forward_; }
	LittleLot::Vector3 right() const { return right_; }
	
	void OnActivated();
protected:
	ModelObject *model_;
	float movespeed_;
	float rotation_speed_;
	LittleLot::Vector3 forward_;
	LittleLot::Vector3 right_;
	bool update_required_;

	LittleLot::Vector3 start_;
	LittleLot::Vector3 end_;

	void MouseYaw(int mouse_x, RECT screen_rect, float dt);
	void ResetMouseToCentreScreen(HWND &hwnd_, RECT screen_rect);

	void MoveForward(float dt);
	void MoveBackward(float dt);

	void MoveLeft(float dt);
	void MoveRight(float dt);

	HWND* hwnd_;
	RECT* rect_;
	Input *input_;
};

#endif