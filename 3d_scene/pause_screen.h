#ifndef _PAUSE_SCREEN_H
#define _PAUSE_SCREEN_H

#include "menu_screen.h"
#include "button.h"

#include "sprite_2d.h"

class PauseScreen :	public MenuScreen {
public:
	PauseScreen(ScreenManager *screen_manager);
	~PauseScreen();

	void Init() override;
	void CleanUp() override;
	
	void Resize() override;

	void Update(float dt) override;
	void Render(Font &font) override;

protected:
	Sprite2D background_;

	void UseButton(int index) override;
};

#endif

