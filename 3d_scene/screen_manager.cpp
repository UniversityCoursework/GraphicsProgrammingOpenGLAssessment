#include "screen_manager.h"

#include "utils.h"

void ScreenManager::Init(Input *input, RECT *screen_rect, HWND *hwnd, HGLRC hrc, HDC hdc) {
	input_ = input;
	screen_rect_ = screen_rect;
	hwnd_ = hwnd;
	screen_resized_ = false;
	thread_farm_.Init(hrc, hdc, DEFAULT_THREADS, DEFAULT_HRC_THREADS);
}

void ScreenManager::CleanUp() {
	thread_farm_.CleanUp();
	// delete all the remaining screens in the vector
	for (Screen_It screen = screens_.begin(); screen != screens_.end(); screen++){
		(*screen)->CleanUp();
		delete (*screen);
		(*screen) = nullptr;
	}
	screens_.clear();
}
void ScreenManager::PushScreen(Screen* screen) {
	screens_.push_back(screen);
	screen->Init();	
}

void ScreenManager::PopScreen() {
	screens_.back()->set_needs_deleted(true);

	//bool success = true;
	//// Bounds check ensures never deletes the last screen...as causes havoc, mayhem and dead kittens.
	//if(!screens_.empty()){	 
	//	Screen* screen = screens_.back();
	//	screen->CleanUp();
	//	screens_.pop_back();
	//	delete screen;
	//	screen = nullptr;
	//	// Make sure dont try to resume non existent screen
	//	if(!screens_.empty()) {			
	//		screens_.back()->Resume();
	//	} else {
	//		success = false;
	//	}
	//}
	//if(!success) {
	//	LittleLot::OutputDebugText("ScreenManager","PopScreen()","Last screen none left to pop");
	//}
}
void ScreenManager::PopScreen(int number_of_screens) {
	// safer than actually popping off the screen in run time
	Screen_It screen;
	screen = screens_.end() - 1;
	for(int i = 0; i < number_of_screens; i++) {
		(*(screen - i))->set_needs_deleted(true);
		//PopScreen();
	}
}
void ScreenManager::Update(float dt) {
	// Update only active screens	
	int size = screens_.size();
	for (int i = 0; i <size; i++) {
		if(screens_[i]->GetState() != kHidden) {			
			screens_[i]->Update(dt);			
		} 	
	}

	// delete any redundant screens
	for(Screen_It screen = screens_.begin(); screen != screens_.end();) {
		if((*screen)->needs_deleted()) {
			if(!screens_.empty()) {
				(*screen)->CleanUp();
				delete (*screen);
				(*screen) = nullptr;
				screen = screens_.erase(screen);
				if(!screens_.empty()) {
					screens_.back()->Resume();
				}
			}
		} else {
			++screen;
		}
	}
	screen_resized_ = false;
}
void ScreenManager::Render(Font &font){
	// Render only non-hidden screens
	for (Screen_It screen = screens_.begin(); screen != screens_.end(); screen++){
		if((*screen)->GetState()!=kHidden){	
			if(screen_resized_) {
				(*screen)->Resize();
			}
			(*screen)->Render(font);
		} 
	}
}


