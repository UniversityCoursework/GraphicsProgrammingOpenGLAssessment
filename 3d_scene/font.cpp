#include "font.h"

#include <sstream>
#include <cstdarg>
#include <string>
#include <cstdio>
#include <cstdlib>

#include "SOIL.h"
#include "utils.h"
#include "math_utils.h"

Font::Font() :
	font_texture_(NULL) {
}

Font::~Font() {
	glDeleteTextures(1, &font_texture_);
}

void Font::Load(const char* font_name) {
	std::string font_filename(font_name);
	font_filename += ".fnt";
	bool success = false;
	// load .fnt
	std::ifstream font_file(font_filename);
	// check valid
	if (font_file.is_open()) {
		success = ParseFont(font_file, character_set);
	}
	if (!success){
		// failed to open font file		
		LittleLot::OutputDebugText("Font", font_name, "Failed to Parse fnt file");
		return;
	}
		
	// load texture
	std::string font_texture_name(font_name);
	font_texture_name += "_0.png";
	font_texture_ = SOIL_load_OGL_texture(
		font_texture_name.c_str(),
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB |
		SOIL_FLAG_COMPRESS_TO_DXT
		);

	if (font_texture_ == 0) {		
		// failed to open texture file
		LittleLot::OutputDebugText("Font", font_name, "Failed to Load Texture");
		return;
	}	
}

bool Font::ParseFont(std::istream& Stream, Font::Charset& CharsetDesc) {
	std::string Line;
	std::string Read, Key, Value;
	std::size_t i;
	while (!Stream.eof()) {
		std::stringstream LineStream;
		std::getline(Stream, Line);
		LineStream << Line;

		// Read the line's type, defined at start of each line
		LineStream >> Read;
		if (Read == "common") {
			// This holds common data, size heights etc
			while (!LineStream.eof()) {
				std::stringstream Converter;
				LineStream >> Read;
				i = Read.find('=');
				Key = Read.substr(0, i);
				Value = Read.substr(i + 1);

				// Assign the correct value
				Converter << Value;
				if (Key == "lineHeight")
					Converter >> CharsetDesc.LineHeight;
				else if (Key == "base")
					Converter >> CharsetDesc.Base;
				else if (Key == "scaleW")
					Converter >> CharsetDesc.Width;
				else if (Key == "scaleH")
					Converter >> CharsetDesc.Height;
			}
		} else if (Read == "char") {
			// This is data for a specific char
			unsigned short CharID = 0;

			while (!LineStream.eof()) {
				std::stringstream Converter;
				LineStream >> Read;
				i = Read.find('=');
				Key = Read.substr(0, i);
				Value = Read.substr(i + 1);

				// Assign the correct value
				Converter << Value;
				if (Key == "id")
					Converter >> CharID;
				else if (Key == "x")
					Converter >> CharsetDesc.Chars[CharID].x;
				else if (Key == "y")
					Converter >> CharsetDesc.Chars[CharID].y;
				else if (Key == "width")
					Converter >> CharsetDesc.Chars[CharID].Width;
				else if (Key == "height")
					Converter >> CharsetDesc.Chars[CharID].Height;
				else if (Key == "xoffset")
					Converter >> CharsetDesc.Chars[CharID].XOffset;
				else if (Key == "yoffset")
					Converter >> CharsetDesc.Chars[CharID].YOffset;
				else if (Key == "xadvance")
					Converter >> CharsetDesc.Chars[CharID].XAdvance;
			}
		}
	}

	return true;
}

char * Font::FormatArgsToChar(const char * text, va_list args) {
	int len;
	char * text_buffer;
	// gets the size of the combined text and args, so dont have buffer overflow.
	len = _vscprintf(text, args) + 1; // _vscprintf doesn't count /0		
	text_buffer = new char[len];

	// basically does the formating of printf but binds it to the text buffer instead
	vsprintf_s(text_buffer, len, text, args);
	return text_buffer;
}

void Font::RenderText(const LittleLot::Vector2& pos, const float scale, const LittleLot::Vector3 colour, const TextJustification justification, const char * text, ...) {
	if (!text)
		return;

	va_list args;
	va_start(args, text);
	char * text_buffer = FormatArgsToChar(text, args);	
	va_end(args);

	GLuint character_count = strlen(text_buffer);
	float string_length = GetStringLength(text_buffer);
	
	LittleLot::Vector2 cursor = LittleLot::Vector2(pos.x, pos.y);

	switch (justification) {
		case kTJ_CENTRE:
			cursor.x -= string_length*0.5f*scale;
			break;
		case kTJ_RIGHT:
			cursor.x -= string_length*scale;
			break;
		case kTJ_VerticalCentre:
			cursor.x -= string_length*0.5f*scale;
			cursor.y -= (character_set.LineHeight + 1.0f)*0.5f*scale;
			break;
		case kTJ_VerticalLeft:
			cursor.y -= (character_set.LineHeight + 1.0f)*0.5f*scale;
			break;
		case kTJ_VerticalRight:
			cursor.x -= string_length*scale;
			cursor.y -= (character_set.LineHeight + 1.0f)*0.5f*scale;
			break;
		default:
			break;
	}
	glBindTexture(GL_TEXTURE_2D, font_texture_);
	glColor4f(colour.x, colour.y, colour.z, 1.0f);
	// Loop through for each character
	for (GLuint character_index = 0; character_index < character_count; ++character_index) {
		GLint CharX = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].x;
		GLint CharY = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].y;
		GLint Width = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].Width;
		GLint Height = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].Height;
		GLint OffsetX = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].XOffset;
		GLint OffsetY = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].YOffset;

		LittleLot::Vector2 uv_pos((float)CharX / (float)character_set.Width, ((float)(CharY) / (float)character_set.Height));
		LittleLot::Vector2 uv_size((float)(Width) / (float)character_set.Width, (float)(Height) / (float)character_set.Height);
		LittleLot::Vector2 size(((float)Width)*scale, ((float)Height)*scale);
		LittleLot::Vector2 quad_pos = LittleLot::Vector2(cursor.x + ((float)OffsetX*scale) , cursor.y + scale*((float)OffsetY));
						
		glBegin(GL_TRIANGLES);

			glTexCoord2f(uv_pos.x, uv_pos.y);
			glVertex2f(quad_pos.x, quad_pos.y);

			glTexCoord2f(uv_pos.x, uv_pos.y + uv_size.y);
			glVertex2f(quad_pos.x, quad_pos.y + size.y);

			glTexCoord2f(uv_pos.x + uv_size.x, uv_pos.y);
			glVertex2f(quad_pos.x + size.x, quad_pos.y);
			//+			
			glTexCoord2f(uv_pos.x + uv_size.x, uv_pos.y + uv_size.y);
			glVertex2f(quad_pos.x + size.x, quad_pos.y + size.y);

			glTexCoord2f(uv_pos.x + uv_size.x, uv_pos.y);
			glVertex2f(quad_pos.x + size.x, quad_pos.y);

			glTexCoord2f(uv_pos.x, uv_pos.y + uv_size.y);
			glVertex2f(quad_pos.x, quad_pos.y + size.y);
		glEnd();

		cursor.x += ((float)character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].XAdvance)*scale;
	}
	delete[] text_buffer;
}

void Font::Render3DText(const LittleLot::Vector3 & pos, const LittleLot::Vector3& rotation, float scale, const LittleLot::Vector3 colour, const TextJustification justification, const char * text, ...) {
	if (!text)
		return;
	scale *= 0.01f;
	

	va_list args;
	va_start(args, text);
	char * text_buffer = FormatArgsToChar(text, args);
	va_end(args);

	GLuint character_count = strlen(text_buffer);
	float string_length = GetStringLength(text_buffer);

	LittleLot::Vector2 cursor = LittleLot::Vector2(0, 0);

	switch (justification) {
	case kTJ_CENTRE:
		cursor.x -= string_length*0.5f*scale;
		break;
	case kTJ_RIGHT:
		cursor.x -= string_length*scale;
		break;
	case kTJ_VerticalCentre:
		cursor.x -= string_length*0.5f*scale;
		cursor.y -= (character_set.LineHeight + 1.0f)*0.5f*scale;
		break;
	case kTJ_VerticalLeft:
		cursor.y -= (character_set.LineHeight + 1.0f)*0.5f*scale;
		break;
	case kTJ_VerticalRight:
		cursor.x -= string_length*scale;
		cursor.y -= (character_set.LineHeight + 1.0f)*0.5f*scale;
		break;
	default:
		break;
	}
	glBindTexture(GL_TEXTURE_2D, font_texture_);
	glColor4f(colour.x, colour.y, colour.z, 1.0f);
	for (GLuint character_index = 0; character_index < character_count; ++character_index) {
		GLint CharX = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].x;
		GLint CharY = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].y;
		GLint Width = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].Width;
		GLint Height = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].Height;
		GLint OffsetX = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].XOffset;
		GLint OffsetY = character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].YOffset;

		LittleLot::Vector2 uv_pos((float)CharX / (float)character_set.Width, ((float)(CharY) / (float)character_set.Height));
		LittleLot::Vector2 uv_size((float)(Width) / (float)character_set.Width, (float)(Height) / (float)character_set.Height);
		LittleLot::Vector2 size(((float)Width)*scale, ((float)Height)*scale);
		LittleLot::Vector2 quad_pos = LittleLot::Vector2(cursor.x + ((float)OffsetX*scale), cursor.y + scale*((float)OffsetY));

		glPushMatrix();

		glTranslatef(pos.x, pos.y, pos.z);

		glRotatef(rotation.x, 1.f, 0.f, 0.f);
		glRotatef(rotation.y, 0.f, 1.f, 0.f);
		glRotatef(rotation.z, 0.f, 0.f, 1.f);

		// need flip easier than converting everything from the 2d 0 top left system used before
		glScalef(1, -1, 1);
		glBegin(GL_TRIANGLES);
			glTexCoord2f(uv_pos.x, uv_pos.y);
			glVertex3f(quad_pos.x, quad_pos.y,0.f);

			glTexCoord2f(uv_pos.x, uv_pos.y + uv_size.y);
			glVertex3f(quad_pos.x, quad_pos.y + size.y, 0.f);

			glTexCoord2f(uv_pos.x + uv_size.x, uv_pos.y);
			glVertex3f(quad_pos.x + size.x, quad_pos.y, 0.f);
			//+			
			glTexCoord2f(uv_pos.x + uv_size.x, uv_pos.y + uv_size.y);
			glVertex3f(quad_pos.x + size.x, quad_pos.y + size.y, 0.f);

			glTexCoord2f(uv_pos.x + uv_size.x, uv_pos.y);
			glVertex3f(quad_pos.x + size.x, quad_pos.y, 0.f);

			glTexCoord2f(uv_pos.x, uv_pos.y + uv_size.y);
			glVertex3f(quad_pos.x, quad_pos.y + size.y, 0.f);
		glEnd();
		glPopMatrix();

		cursor.x += ((float)character_set.Chars[static_cast<GLuint>(text_buffer[character_index])].XAdvance)*scale;
	}
	delete[] text_buffer;
}

void Font::Begin3DText(){
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
}
void Font::End3DText(){
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
}

// Goes through each letter in text, and finds the xadvance and adds it to the length.
// to give the actual visual string length
float Font::GetStringLength(const char * text) {
	float length = 0.0f;
	if (text) {
		GLuint string_length = strlen(text);

		for (GLuint character_index = 0; character_index < string_length; ++character_index)
			length += ((float)character_set.Chars[static_cast<GLuint>(text[character_index])].XAdvance);
	}

	return length;
}

