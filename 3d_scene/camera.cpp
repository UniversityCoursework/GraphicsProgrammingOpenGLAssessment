#include "camera.h"

#include <cmath>

void Camera::MoveForward(float dt) {
	set_position(position() + forward()*move_speed_*dt);
	update_required_ = true;
}
void Camera::MoveBack(float dt) {
	set_position(position() - forward()*move_speed_*dt);
	update_required_ = true;
}

void Camera::MoveLeft(float dt) {
	set_position(position() + right()*move_speed_*dt);
	update_required_ = true;
}
void Camera::MoveRight(float dt) {
	set_position(position() - right()*move_speed_*dt);
	update_required_ = true;
}
void Camera::YawLeft(float dt) {
	set_yaw(yaw() - rotation_speed_*dt);
	update_required_ = true;
}
void Camera::YawRight(float dt) {
	set_yaw(yaw() + rotation_speed_*dt);
	update_required_ = true;
}
void Camera::PitchUp(float dt) {
	set_pitch(pitch() + rotation_speed_*dt);
	update_required_ = true;
}
void Camera::PitchDown(float dt) {
	set_pitch(pitch() - rotation_speed_*dt);
	update_required_ = true;
}

void Camera::RollLeft(float dt) {
	set_roll(roll() + rotation_speed_*dt);
	update_required_ = true;
}
void Camera::RollRight(float dt) {
	set_roll(roll() - rotation_speed_*dt);
	update_required_ = true;
}
void Camera::MoveUp(float dt) {
	set_position(position() + up()*move_speed_*dt);
	update_required_ = true;
}
void Camera::MoveDown(float dt) {
	set_position(position() - up()*move_speed_*dt);
	update_required_ = true;
}


void Camera::MoveUpY(float dt) {
	set_position(position() + LittleLot::Vector3(0, 1, 0)*move_speed_*dt);
	update_required_ = true;
}
void Camera::MoveDownY(float dt) {
	set_position(position() - LittleLot::Vector3(0, 1, 0)*move_speed_*dt);
	update_required_ = true;
}
void Camera::MouseYawPitch(int mouse_x, int mouse_y, RECT screen_rect, float dt) {
	float cen_x = (float)screen_rect.right / 2.0f;
	float cen_y = (float)screen_rect.bottom / 2.0f;
	float error = 0.6f;
	float diff_x = mouse_x - cen_x;
	float diff_y = mouse_y - cen_y;

	if(abs(diff_y) > error) {
		set_pitch(pitch() - rotation_speed()*diff_y*dt);
	}
	if(abs(diff_x) > error) {
		set_yaw(yaw() + rotation_speed()*diff_x*dt);
	}
	update_required_ = true;
}
void Camera::ResetMouseToCentreScreen(HWND &hwnd_, RECT screen_rect) {
	// stops camera from 1st person camera being turned onfirst frame
	POINT temp;
	temp.x = screen_rect.right / 2;
	temp.y = screen_rect.bottom / 2;
	ClientToScreen(hwnd_, &temp);
	SetCursorPos(temp.x, temp.y);
}