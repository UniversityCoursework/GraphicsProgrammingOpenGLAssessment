#ifndef _BARRIER_H
#define _BARRIER_H

#include <atomic>
#include <condition_variable>

class Barrier {
public:
	void Init(int barrier_count);
	void NotifyOne();
	void NotifyAll();
	void Wait();
protected:
	std::atomic<int> count_;
	std::mutex mutex_;
	std::condition_variable cond_;

};

#endif
