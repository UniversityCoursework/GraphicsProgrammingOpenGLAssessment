#include "start_screen.h"

#include "screen_manager.h"
#include "font.h"
#include "vector2.h"

#include "loading_screen.h"
#include "earth_room.h"
#include "model_room.h"

enum ButtonName {
	kEarthRoom = 0,
	kModelRoom
};

const float button_width = 180;
const float button_height = 15;
const float button_offset = 32;
const float title_offset = 80;

const float test_display_offset_x=140;
const float test_display_offset_y=10;


StartScreen::StartScreen(ScreenManager * screen_manager)
	:MenuScreen(screen_manager){
}

void StartScreen::Init() {
	background_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
	background_.set_width((float)screen_manager_->ScreenWidth() / 2.f);
	background_.set_height((float)screen_manager_->ScreenHeight() / 4.f);

	Colour bg_colour;
	bg_colour.set_rgba_255(93, 19, 114);
	background_.set_colour(bg_colour);

	Colour button_bg,button_highlight;
	button_bg.set_rgba_255(121, 190, 0);
	button_highlight.set_rgba(0.5f, 0.5f, 0.5f, 0.6f);

	current_button_ = 0;
	buttons_.push_back(Button(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f - button_offset), button_width, button_height, button_bg, button_highlight, "Earth Room"));

	buttons_.push_back(Button(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f + button_offset), button_width, button_height, button_bg, button_highlight, "Model Room"));

}

void StartScreen::CleanUp() {
}

void StartScreen::Resume() {
	state_ = kActive;
	Resize();
}

void StartScreen::Resize() {
	background_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
	background_.set_width((float)screen_manager_->ScreenWidth() / 2.f);
	background_.set_height((float)screen_manager_->ScreenHeight() / 4.f);
	buttons_[kEarthRoom].set_position(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f - button_offset));
	
	buttons_[kModelRoom].set_position(LittleLot::Vector2(screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f + button_offset));
}

void StartScreen::Update(float dt) {
	switch(state_) {
		case kActive: {		
				
				UpdateMenu(dt);

								
				break;
			}
		case kOverlayed: {
				// any background animations, updates etc.
				break;
			}
		default:
			break;
	}
}

void StartScreen::UseButton(int index) {
	switch(ButtonName(index)) {
		case kEarthRoom: {
				screen_manager_->PushScreen(new EarthRoom(screen_manager_));
				Pause(false);				
				break;
			}
		case kModelRoom: {
				screen_manager_->PushScreen(new ModelRoom(screen_manager_));
				Pause(false);				
				break;
			}
		default:
			break;
	}
}
void StartScreen::Render(Font & font) {	
	switch(state_) {
		case kActive:			
			Begin2D();					
			background_.DrawSprite();
			font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() / 2.f, screen_manager_->ScreenHeight() / 2.f - title_offset), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Start Screen");
			RenderButtons(font);
			End2D();
			break;
		case kOverlayed:

			
			break;
		default:
			break;
	}
	
}

