#ifndef _THREAD_FARM_H
#define _THREAD_FARM_H

#include <functional>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <atomic>

#include <windows.h>

#define DEFAULT_THREADS 2
#define DEFAULT_HRC_THREADS 2

class ThreadFarm {
	typedef std::vector<std::function<void()>>::iterator Function_It;
public:
	
	// Requires Initialisation with HRC/HDC once created.
	ThreadFarm();
	~ThreadFarm();

	// If there is no HRC's available it will try to run non-hrc functions on available threads. 
	// Usaully have more threads than HRC's
	// Maximum generally, #105 on ati, #128 on nvidia 
	// http://stackoverflow.com/questions/4951370/is-there-a-limit-to-how-many-opengl-rendering-contexts-you-can-create-simultaneo	
	// Hrc used to create shared contexts, hdc use to make them current for a thread.
	void Init(HGLRC hrc, HDC hdc, int num_of_threads, int num_of_hrc);

	// Initialize a bunch of worker threads, and start working through task queue
	// external use of barriers, etc to find when stuff done is best.
	void StartThreads();

	// tells all threads to stop running and joins them.
	// deletes hrcs, and any remaining tasks in the queue.
	void CleanUp();
	
	// If disabling hrc, ensure no opengl commands are called.
	void QueueTask(std::function<void()> function, bool requires_hrc = true);

protected:

	// Works its way through the hrc task queue, terminating when queue empty.
	void RenderContextConsumer(HGLRC hrc);
	// Works its way throuhg the task queue, terminating when queue empty.
	void Consumer();

	std::function<void()> GetHrcTask();
	std::function<void()> GetTask();

	// Tells all threads to stop running and joins them.
	void CleanUpThreads();
	// Deletes all the HRC's, ensure not in use before calling
	void CleanUpHrcs();
	// Deletes all tasks remaining in the queue.
	void CleanUpTasks();

	std::vector<std::thread> threads_;

	std::queue<std::function<void()>> hrc_tasks_;
	std::queue<std::function<void()>> tasks_;

	std::vector<HGLRC> hrcs_;	

	std::mutex hrcs_mutex_;	
	std::condition_variable hrc_cond_;
	std::mutex tasks_mutex_;	
	std::condition_variable task_cond_;

	HDC hdc_;
	HGLRC hrc_;

	int num_of_threads_;
	int num_of_hrc_;
	bool is_running_;
};

#endif

