#ifndef _START_SCREEN_H
#define _START_SCREEN_H


#include "sprite_2d.h"
#include "menu_screen.h"
#include <string>

class StartScreen :public MenuScreen {
public:
	StartScreen(ScreenManager *screen_manager);
		

	~StartScreen() {};
	void Init() override;
	void CleanUp() override;

	void Resume() override;

	void Resize() override;

	void Update(float dt) override;
	void Render(Font &font) override;

protected:
	void UseButton(int index) override;

	Sprite2D background_;
	std::vector<std::string> descriptions_;	
};

#endif
