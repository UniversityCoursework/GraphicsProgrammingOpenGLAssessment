#include "free_camera.h"

// local, headers only needed locally stops from being called into everything else when header is called

#include "math_utils.h"
#include "input.h"
#include "camera_manager.h"
#include "utils.h"

FreeCam::FreeCam() {
	update_required_ = true;
	position_ = LittleLot::Vector3::kZero;
	rotation_ = LittleLot::Vector3::kZero;
	move_speed_ = 0.005f;
	rotation_speed_ = 0.01f;	
}
FreeCam::FreeCam(LittleLot::Vector3 position, LittleLot::Vector3 rotation) {
	update_required_ = true;
	position_ = position;
	reset_position_ = position;
	rotation_ = rotation;
	reset_rotation_ = rotation;
	move_speed_ = 0.005f;
	rotation_speed_ = 0.01f;	
}

void FreeCam::Update(float dt) {
	ControlUpdate(dt);
	if (update_required_) {
		if (pitch() > 89.0f)
			set_pitch(89.0f);
		if (pitch() < -89.0f)
			set_pitch(-89.0f);
		float cos_r, cos_p, cos_y;
		float sin_r, sin_p, sin_y;
		// pre calculated regulary used variables
		cos_y = cosf(LittleLot::DegreesToRadians(yaw()));
		cos_p = cosf(LittleLot::DegreesToRadians(pitch()));
		cos_r = cosf(LittleLot::DegreesToRadians(roll()));

		sin_y = sinf(LittleLot::DegreesToRadians(yaw()));
		sin_p = sinf(LittleLot::DegreesToRadians(pitch()));
		sin_r = sinf(LittleLot::DegreesToRadians(roll()));

		// forward vector direction of the camera
		forward_.x = sin_y * cos_p;
		forward_.y = sin_p;
		forward_.z = cos_p * -cos_y;

		// up vector 
		up_.x = -cos_y * sin_r - sin_y * sin_p * cos_r;
		up_.y = cos_p * cos_r;
		up_.z = -sin_y * sin_r - sin_p * cos_r * -cos_y;

		right_ = forward_.CrossProduct(up_);
		look_at_ = position_ + forward_;
		update_required_ = false;
	}
	
}

void FreeCam::OnActivated() {
	LittleLot::DisplayCursor(false);
	// checks if the main windows has focus, i.e. no popups.	
	ResetMouseToCentreScreen(*owner_->GetWindowHandle(), owner_->GetScreenRect());	
}

void FreeCam::ControlUpdate(float dt) {	
	// reset
	if(owner_->GetInput()->is_key_down(VK_SPACE)) {
		set_position(reset_position_);
		set_rotation(reset_rotation_);
	}
	// movement
	if(owner_->GetInput()->is_key_down('W')) {
		MoveForward(dt);
	}
	if(owner_->GetInput()->is_key_down('S')) {
		MoveBack(dt);
	}
	if(owner_->GetInput()->is_key_down('Q')) {
		MoveUpY(dt);
	}
	if(owner_->GetInput()->is_key_down('E')) {
		MoveDownY(dt);
	}
	if(owner_->GetInput()->is_key_down('A')) {
		MoveRight(dt);
	}
	if(owner_->GetInput()->is_key_down('D')) {
		MoveLeft(dt);
	}

	// checks if the main windows has focus, i.e. no popups.
	if(GetActiveWindow() == *owner_->GetWindowHandle()) {
		MouseYawPitch(owner_->GetInput()->mouse_x(), owner_->GetInput()->mouse_y(), owner_->GetScreenRect(), dt);
		ResetMouseToCentreScreen(*owner_->GetWindowHandle(), owner_->GetScreenRect());
	}
}
