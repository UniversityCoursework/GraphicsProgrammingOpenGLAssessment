#ifndef _STATIC_CAMERA_H
#define _STATIC_CAMERA_H

#include <windows.h>

#include "vector3.h"

#include "camera.h"

class StaticCam : public Camera {
public:	
	StaticCam(LittleLot::Vector3 position, LittleLot::Vector3 rotation);

	void Update(float dt) override;

	void OnActivated() override;

protected:
};

#endif

