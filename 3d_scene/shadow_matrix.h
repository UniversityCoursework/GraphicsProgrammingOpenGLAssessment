#ifndef _SHADOW_MATRIX_H
#define _SHADOW_MATRIX_H

#include "vector3.h"
#include "light.h"

void GenerateShadowMatrix(float matrix[16], float light_pos[4], LittleLot::Vector3 P, LittleLot::Vector3 Q, LittleLot::Vector3 R);

void BeginShadowRender(Light &light,LittleLot::Vector3 plane_1, LittleLot::Vector3 plane_2, LittleLot::Vector3 plane_3);

void EndShadowRender();
#endif