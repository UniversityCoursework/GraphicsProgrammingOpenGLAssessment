#ifndef _MENU_SCREEN_H
#define _MENU_SCREEN_H

#include "screen.h"
#include "button.h"

#include <vector>

class MenuScreen : public Screen {
public:
	MenuScreen(ScreenManager *screen_manager);

protected:

	void UpdateMenu(float dt);
	virtual void UseButton(int index) = 0;

	int current_button_;
	std::vector<Button> buttons_;
	typedef std::vector<Button>::iterator Button_It;

	void RenderButtons(Font &font);
	
	bool mouse_down_;
};
#endif
