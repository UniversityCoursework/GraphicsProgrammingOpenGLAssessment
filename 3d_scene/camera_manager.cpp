#include "camera_manager.h"

#include "camera.h"
#include "input.h"
#include "utils.h"

void CameraManager::Init(Input * input, RECT * screen_rect, HWND * hwnd) {
	input_ = input;
	screen_rect_ = screen_rect;
	hwnd_ = hwnd;
}

void CameraManager::AddCamera(std::string tag, Camera * camera,bool replace) {
	std::map<std::string, Camera*>::iterator it;
	// check if it is already in list
	it = cameras_.find(tag);
	if(it == cameras_.end()) {	
		// not in list therfore add it
		camera->SetOwner(this);
		cameras_[tag] = camera;
		// if no camera set make this one current
		if(cameras_[current_camera_tag_] == nullptr) {
			current_camera_tag_ = tag;
			GetCurrentCamera()->OnActivated();
		}
	} else {
		LittleLot::OutputDebugText("Camera", tag.c_str(), (replace ? "Already Exists Will be replaced" : "Already Exists Will not be replaced"));
		if(replace) {
			// we replacing the current camera?
			if(it->second == cameras_[current_camera_tag_]) {
				// then need to assign the new camera
				current_camera_tag_ = tag;
			}
			// delete old camera
			delete it->second;
			camera->SetOwner(this);
			cameras_[tag] = camera;
		}
	}
}

void CameraManager::DeleteCamera(std::string tag) {
	std::map<std::string, Camera*>::iterator it;
	// check if it is already in list
	it = cameras_.find(tag);
	if(it != cameras_.end()) {
		// in the map
		// check if current camera
		if(cameras_[current_camera_tag_] == it->second) {
			current_camera_tag_ = "";
		}
		delete it->second;
		cameras_.erase(it);
	} else {
		// otherwise error
		LittleLot::OutputDebugText("Camera", tag.c_str(), "DeleteCamera(), Doesnt Exist");
	}	
}

void CameraManager::ChangeCamera(std::string tag) {
	std::map<std::string, Camera*>::iterator it;
	// check if it is already in list
	it = cameras_.find(tag);
	if(it != cameras_.end()) {		
		current_camera_tag_ = tag;
		GetCurrentCamera()->OnActivated();
	} else {		
		LittleLot::OutputDebugText("Camera", tag.c_str(),"ChangeCamera(), Doesnt Exist");
	}
}

void CameraManager::CleanUp() {
	// delete all the remaining camera in the map
	for(std::map<std::string, Camera*>::iterator camera = cameras_.begin(); camera != cameras_.end();) {		
		delete camera->second;
		camera->second = nullptr;
		camera  = cameras_.erase(camera);
	}	
}

void CameraManager::Update(float dt) {
	cameras_[current_camera_tag_]->Update(dt);
	
}
