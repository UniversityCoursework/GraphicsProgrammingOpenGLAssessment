#include "security_camera.h"

#include "math_utils.h"
#include "camera_manager.h"
#include "input.h"
#include "utils.h"

#include <cmath>

SecurityCam::SecurityCam(LittleLot::Vector3 position, LittleLot::Vector3 lower_limit, LittleLot::Vector3 upper_limit, bool is_controlled) {
	is_controlled_ = is_controlled;
	position_ = position;
	rotation_ = lower_limit;

	lower_rotation_ = lower_limit;
	upper_rotation_ = upper_limit;
	
	update_required_ = true;	
	
	
	move_speed_ = 0.005f;
	rotation_speed_ = 0.0001f;
	
	lerp_amount_ = 0.01f;
	delay_timer_ = 0;
}

SecurityCam::SecurityCam(LittleLot::Vector3 position, LittleLot::Vector3 lower_limit, LittleLot::Vector3 upper_limit, float lerp_amount, float delay, bool is_controlled)
	: SecurityCam(position, lower_limit, upper_limit, is_controlled) {		
	lerp_amount_ = lerp_amount;
	delay_ = delay*1000;
}

void SecurityCam::Update(float dt) {
	if(is_controlled_) {
		// checks if the main windows has focus, i.e. no popups.
		if(GetActiveWindow() == *owner_->GetWindowHandle()) {
			MouseYawPitch(owner_->GetInput()->mouse_x(), owner_->GetInput()->mouse_y(), owner_->GetScreenRect(), dt);
			ResetMouseToCentreScreen(*owner_->GetWindowHandle(), owner_->GetScreenRect());
		}
		// limit to angles in upper and lower
		rotation_ = LittleLot::ClampBetweenVectors(rotation_, lower_rotation_, upper_rotation_);
	} else {	
		lerp_current_ += lerp_amount_*(dt / 1000);

		if(lerp_current_ < 0) {
			delay_timer_ += dt;
			if(delay_timer_ > delay_) {
				lerp_current_ = 0;
				lerp_amount_ *= -1;
				delay_timer_ = 0;
			}
		}else if(lerp_current_ > 1.0f) {
			delay_timer_ += dt;
			if(delay_timer_ > delay_) {
				lerp_current_ = 1;
				lerp_amount_ *= -1;				
				delay_timer_ = 0;
			}			
		} else {			
			rotation_ = LittleLot::Lerp(lower_rotation_, upper_rotation_, lerp_current_);
		}		
		update_required_ = true;
	}
	if(update_required_) {
		float cos_r, cos_p, cos_y;
		float sin_r, sin_p, sin_y;
		// pre calculated regulary used variables
		cos_y = cosf(LittleLot::DegreesToRadians(yaw()));
		cos_p = cosf(LittleLot::DegreesToRadians(pitch()));
		cos_r = cosf(LittleLot::DegreesToRadians(roll()));

		sin_y = sinf(LittleLot::DegreesToRadians(yaw()));
		sin_p = sinf(LittleLot::DegreesToRadians(pitch()));
		sin_r = sinf(LittleLot::DegreesToRadians(roll()));

		// forward vector direction of the camera
		forward_.x = sin_y * cos_p;
		forward_.y = sin_p;
		forward_.z = cos_p * -cos_y;

		// up vector 
		up_.x = -cos_y * sin_r - sin_y * sin_p * cos_r;
		up_.y = cos_p * cos_r;
		up_.z = -sin_y * sin_r - sin_p * cos_r * -cos_y;

		look_at_ = position_ + forward_;
		update_required_ = false;
	}
}

void SecurityCam::OnActivated() {	
	if(is_controlled_) {		
		LittleLot::DisplayCursor(false);
	} else {
		LittleLot::DisplayCursor(true);
	}
}
