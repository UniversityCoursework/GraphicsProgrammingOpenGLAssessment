#include "model_room.h"


#include "screen_manager.h"
#include "free_camera.h"
#include "math_utils.h"

#include "security_camera.h"
#include "tp_camera.h"
#include "model_object.h"


#include "pause_screen.h"
#include "loading_screen.h"

#include "utils.h"

ModelRoom::ModelRoom(ScreenManager * screen_manager) 
:Screen(screen_manager){
}

void ModelRoom::Init() {
	loading_ = true;

	screen_manager_->PushScreen(new LoadingScreen(screen_manager_));
	Pause(false);

	// set up lighting, seems to need to be in main thread
	point_light_ = Light(kZero, kPoint, 0, 5, 0);
	point_light_.set_constant_attenuation(.1f);
	point_light_.set_linear_attenuation(.1f);
	point_light_.Debug(true);

	point_light_.enable();

	spot_light_ = Light(kTwo, kSpot, 0, 0, 0);
	spot_light_.set_spot_cutoff(40);
	spot_light_.set_spot_exponent(0.1f);
	spot_light_.set_diffuse(1, 0, 0);
	spot_light_.Debug(true);
	spot_light_.enable();

	spot_light_2_ = Light(kThree, kSpot, 0, 0, 0);
	spot_light_2_.set_spot_cutoff(40);
	spot_light_2_.set_spot_exponent(0.1f);
	spot_light_2_.set_diffuse(0, 1, 0);
	spot_light_2_.Debug(true);
	spot_light_2_.enable();

	texture_load_.Init(10);
	object_load_.Init(20);
	creation_done_.Init(3);

	// textures
	screen_manager_->thread_farm().QueueTask([&] {
		main_skybox_.LoadTexture("bin/textures/space_cubemap.png");
		percentage_ = percentage_ + 0.2f;
		texture_load_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		crate_tile_.LoadTexture("bin/textures/crate.png");
		percentage_ = percentage_ + 0.1f;
		texture_load_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		brick_.LoadTexture("bin/textures/brick_1.png");
		percentage_ = percentage_ + 0.1f;
		texture_load_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		rock_floor_.LoadTexture("bin/textures/mountain.png");
		texture_load_.NotifyAll();
		percentage_ = percentage_ + 0.1f;
	});
	screen_manager_->thread_farm().QueueTask([&] {
		t_tree_.LoadTexture("bin/models/tree_texture.png");
		tree_01_.AssignTexture(t_tree_);
		tree_02_.AssignTexture(t_tree_);
		tree_03_.AssignTexture(t_tree_);
		tree_04_.AssignTexture(t_tree_);
		tree_pine_.AssignTexture(t_tree_);
		percentage_ = percentage_ + 0.01f;
		texture_load_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		t_rock_.LoadTexture("bin/models/rock_texture.png");;
		rock_set_01_.AssignTexture(t_rock_);
		rock_set_02_.AssignTexture(t_rock_);
		rock_set_03_.AssignTexture(t_rock_);
		rock_set_04_.AssignTexture(t_rock_);
		percentage_ = percentage_ + 0.01f;
		texture_load_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		shock_trooper_.LoadTexture("bin/models/shock_trooper_texture.png");
		texture_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	});

	screen_manager_->thread_farm().QueueTask([&] {
		mech_.LoadTexture("bin/models/mech_sniper_texture.png");
		texture_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	});
	screen_manager_->thread_farm().QueueTask([&] {
		sheep_.LoadTexture("bin/models/sheep_texture.png");
		texture_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	});
	screen_manager_->thread_farm().QueueTask([&] {
		campfire_.LoadTexture("bin/models/campfire_texture.png");
		texture_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	});

	// objects
	screen_manager_->thread_farm().QueueTask([&] {
		campfire_.Load("bin/models/campfire.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		tree_01_.Load("bin/models/tree_01.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	
	screen_manager_->thread_farm().QueueTask([&] {
		tree_02_.Load("bin/models/tree_02.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		tree_03_.Load("bin/models/tree_03.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		tree_04_.Load("bin/models/tree_04.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		tree_pine_.Load("bin/models/tree_pine.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		sheep_.Load("bin/models/sheep.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		rock_set_01_.Load("bin/models/rock_set_01.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		rock_set_02_.Load("bin/models/rock_set_02.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		rock_set_03_.Load("bin/models/rock_set_03.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		rock_set_04_.Load("bin/models/rock_set_04.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		shock_trooper_.Load("bin/models/shock_trooper.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.1f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		mech_.Load("bin/models/mech_sniper.obj");
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.1f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		skybox_ = BuildSkyBox();
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		plane_ = BuildPlane(16, 10);
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		cube_ = BuildCube();
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		wall_1_ = BuildPlane(8, 4);
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		wall_2_ = BuildPlane(4, 4);
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		floor_ = BuildPlane(32, 16);
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		quad_ = BuildQuad();
		object_load_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);

	// set up model scene
	// messy positioning of assets // tried generation from planes vertex positions, with random offsets and some centreing
	// didnt like it so just positioned by hand
	// shoulda really have made an orientation matrix...
	screen_manager_->thread_farm().QueueTask([&] {
		texture_load_.Wait();
		object_load_.Wait();
		BuildTableTop();
		creation_done_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		texture_load_.Wait();
		object_load_.Wait();
		BuildRoom();
		creation_done_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	}, false);

	screen_manager_->thread_farm().QueueTask([&] {
		texture_load_.Wait();
		object_load_.Wait();

		// set up cameras
		camera_manager_.Init(screen_manager_->GetInput(), screen_manager_->GetScreenRect(), screen_manager_->GetWindowHandle());

		camera_manager_.AddCamera("Free Camera", new FreeCam(LittleLot::Vector3(0, 15, 15), LittleLot::Vector3(-45, 0, 0)));
		Camera *cam = new SecurityCam(LittleLot::Vector3(-30, 27, 14), LittleLot::Vector3(-35, -5, 0), LittleLot::Vector3(-35, 90, 0));
		cam->set_yaw(45);
		cam->set_pitch(-35);
		cam->set_rotation_speed(0.0008f);
		camera_manager_.AddCamera("Security Camera", cam);

		// third person model,controller and camera.

		character_model_.set_model(&shock_trooper_);
		character_model_.set_position(LittleLot::Vector3(4, 0, 0));
		character_model_.set_rotation(LittleLot::Vector3(0, -90, 0));
		character_model_.set_scale(LittleLot::Vector3(0.2f, 0.2f, 0.2f));

		character_controller_ = CharacterController(&character_model_, 0.001f, 0.002f, screen_manager_->GetInput(), screen_manager_->GetWindowHandle(), screen_manager_->GetScreenRect());
		character_controller_.SetCharacterBounds(LittleLot::Vector3(-7.9f, 0, -4.9f),
												 LittleLot::Vector3(7.9f, 0, 4.9f));
		camera_manager_.AddCamera("OffSet Third Person Camera", new TPCamera(character_model_.position(), character_model_.rotation(), 2.5f, LittleLot::Vector3(0, .8f, 0), 45, 0.003f));

		creation_done_.NotifyAll();
		percentage_ = percentage_ + 0.01f;
	});

	screen_manager_->thread_farm().QueueTask([&] {
		creation_done_.Wait();

		// ------------------------- move all the static stuff into display lists, increases fps quite alot...
		table_top_list_ = glGenLists(1);
		glNewList(table_top_list_, GL_COMPILE);
		table_top_objects_.Render();
		glEndList();

		room_list_ = glGenLists(2);
		glNewList(room_list_, GL_COMPILE);
		static_objects_.Render();
		glEndList();
		percentage_ = percentage_ + 0.01f;
		// Allow camera to call any needed functions
		//camera_manager_.Update(0);
		loading_ = false;		
	});

	screen_manager_->thread_farm().StartThreads();
	
}
void ModelRoom::CleanUp() {
	camera_manager_.CleanUp();
	dynamic_objects_.CleanUp();
	table_top_objects_.CleanUp();
	static_objects_.CleanUp();
	point_light_.disable();
	spot_light_.disable();
	spot_light_2_.disable();
	glDeleteLists(room_list_, 1);
	glDeleteLists(table_top_list_, 1);
}

bool ModelRoom::IsLoading(float & percentage) {
	percentage = percentage_;
	return loading_;
}

void ModelRoom::Resize() {
}

void ModelRoom::Resume() {
	state_ = kActive;
	camera_manager_.GetCurrentCamera()->OnActivated();
}

void ModelRoom::Update(float dt) {
	switch(state_) {
		case kActive: {
				if(screen_manager_->GetInput()->is_key_down('P')) {
					screen_manager_->PushScreen(new PauseScreen(screen_manager_));
					screen_manager_->GetInput()->set_key_up('P');
					LittleLot::DisplayCursor(true);
					Pause(true);
				}

				if(screen_manager_->GetInput()->is_key_down(VK_F1)) {
					camera_manager_.ChangeCamera("Free Camera");
				}
				if(screen_manager_->GetInput()->is_key_down(VK_F2)) {
					camera_manager_.ChangeCamera("Security Camera");
				}
				if (screen_manager_->GetInput()->is_key_down(VK_F3)) {
					camera_manager_.ChangeCamera("OffSet Third Person Camera");
					character_controller_.OnActivated();
				}
				dynamic_objects_.Update(dt);
				if (camera_manager_.GetCurrentCameraTag() == "OffSet Third Person Camera") {
					character_controller_.UpdateInput(dt);
				}
				camera_manager_.Update(dt);
				break;
			}
		case kOverlayed: {
				break;
			}
		case kHidden: {
				break;
			}
		default:
			break;
	}
}

void ModelRoom::Render(Font & font) {
	switch(state_) {
		case kActive:
			RenderScene(font);
			break;
		case kOverlayed:
			RenderScene(font);
			break;
		case kHidden:
			break;
		default:
			break;
	}
	
}

void ModelRoom::RenderScene(Font & font) {
	Begin3D(*camera_manager_.GetCurrentCamera());

	//Skybox
	glPushMatrix();		
		glTranslatef(camera_manager_.GetCurrentCamera()->position().x, camera_manager_.GetCurrentCamera()->position().y, camera_manager_.GetCurrentCamera()->position().z);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, main_skybox_.data());			
		glDisable(GL_DEPTH_TEST);			
			RenderVertexData(skybox_);		
		glEnable(GL_DEPTH_TEST);
		glBindTexture(GL_TEXTURE_2D, NULL);		
	glPopMatrix();
	
	glEnable(GL_BLEND);
		glColor4f(0.8f, 0.8f, 1.0f, 0.2f);
		window_.Render();
	glDisable(GL_BLEND);

	RenderWorld();
	
	font.Begin3DText();
	font.Render3DText(LittleLot::Vector3(0, 12, -15.98f), LittleLot::Vector3(0, 0, 0), 6.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "TableTop Gamers... AKA Basement Dwellers.");
	
	font.Render3DText(LittleLot::Vector3(-26, 2, 0), LittleLot::Vector3(0, 90, 0), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Spot Light");
	font.Render3DText(LittleLot::Vector3(26, 2, 0), LittleLot::Vector3(0, -90, 0), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Spot Light");
	font.Render3DText(LittleLot::Vector3(0, 6, 0), LittleLot::Vector3(0, 0, 0), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Point Light");
	font.Render3DText(LittleLot::Vector3(0, 4, 0), LittleLot::Vector3(0, 0, 0), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Planar Shadows");	
	font.Render3DText(LittleLot::Vector3(31.98f, 11,0), LittleLot::Vector3(0, -90, 0), 4.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Window - Your the centre of the universe...");
	font.Render3DText(LittleLot::Vector3(31.98f, 10, 0), LittleLot::Vector3(0, -90, 0), 2.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "or you blew it all up and light hasnt caught up yet.");
	font.End3DText();
	
	Begin2D();
	font.RenderText(LittleLot::Vector2(10, 80), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F1 - Free Cam");
	font.RenderText(LittleLot::Vector2(10, 100), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F2 - Security Camera, Controlled");
	font.RenderText(LittleLot::Vector2(10, 120), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "f3 - OffSet Third Person Camera");

	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 40), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "pitch %.3f", camera_manager_.GetCurrentCamera()->pitch());
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 60), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "yaw %.3f", camera_manager_.GetCurrentCamera()->yaw());
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 80), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "roll %.3f", camera_manager_.GetCurrentCamera()->roll());
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 100), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "X: %.3f", camera_manager_.GetCurrentCamera()->position().x);
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 120), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "Y: %.3f", camera_manager_.GetCurrentCamera()->position().y);
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 140), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "Z: %.3f", camera_manager_.GetCurrentCamera()->position().z);
	
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() - 40),
		1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE,
		"Current Camera: %s", camera_manager_.GetCurrentCameraTag().c_str());

	End2D();

	if(camera_manager_.GetCurrentCameraTag() == "Security Camera") {
		RenderGrayScale();
	}
}

void ModelRoom::RenderWorld() {
	
	glEnable(GL_LIGHTING);
	point_light_.Render();
	spot_light_.Render();
	spot_light_2_.Render();
		
	room_floor_.Render();
	
	// table shadows onto floor
	BeginShadowRender(point_light_,
					  LittleLot::Vector3(-1, -4, -1),
					  LittleLot::Vector3(-1, -4, 1),
					  LittleLot::Vector3(1, -4, -1));
		
		glColor4f(0, 0, 0, 1);
		table_.Render();
		character_model_.Render();			
		glCallList(table_top_list_);		
	EndShadowRender();

	// render actual table top scene	
	table_.Render();
	ForbiddenCrates();		
	dynamic_objects_.Render();	
		
	// stencil so only shadows on table
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glEnable(GL_STENCIL_TEST);
		glStencilFunc(GL_ALWAYS, 1, 1);
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

		glDisable(GL_DEPTH_TEST);
			// cut hole
			RenderVertexData(plane_);		
		glEnable(GL_DEPTH_TEST);

		// render shadows in it
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glStencilFunc(GL_EQUAL, 1, 1);
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);	
		BeginShadowRender(point_light_,
						  LittleLot::Vector3(-1, 0, -1),
						  LittleLot::Vector3(-1, 0, 1),
						  LittleLot::Vector3(1, 0, -1));			

			glColor4f(0, 0, 0, 1);
			glCallList(table_top_list_);
			character_model_.Render();			
		EndShadowRender();
	glDisable(GL_STENCIL_TEST);

	glCallList(table_top_list_);

	character_model_.Render();

	glCallList(room_list_);
}

void ModelRoom::RenderGrayScale() {
	glDisable(GL_DEPTH_TEST);

	GLint viewport[4];
	// Get viewport data.
	glGetIntegerv(GL_VIEWPORT, viewport);

	// height*width for array of pixels on screen
	GLubyte *pixels = new GLubyte[viewport[2] * viewport[3]];
	// Green works, read pixel data 
	glReadPixels(0, 0, viewport[2], viewport[3], GL_GREEN, GL_UNSIGNED_BYTE, pixels);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	// viewport the size of the screen.
	gluOrtho2D(0.0, (GLfloat)viewport[2], 0.0, (GLfloat)viewport[3]);
	// rectangle the size of the screen.
	glDrawPixels(viewport[2], viewport[3], GL_LUMINANCE, GL_UNSIGNED_BYTE, pixels);
	glPopMatrix();

	glEnable(GL_DEPTH_TEST);
	delete[] pixels;
}

void ModelRoom::ForbiddenCrates() {
	glEnable(GL_BLEND);		
		Material material;
		material.set_diffuse(.0f, .6f, .0f, 1.f);
		material.Apply();
		RenderVertexData(plane_);
		glBindTexture(GL_TEXTURE_2D, crate_tile_.data());

		material.set_diffuse(.0f, .2f, .0f, .2f);
		material.Apply();
		RenderVertexData(plane_);	
		glBindTexture(GL_TEXTURE_2D, NULL);
		ApplyDefaultMaterial();
	glDisable(GL_BLEND);
}

void ModelRoom::BuildTableTop() {
	// trees
	// back
	table_top_objects_.AddObject(new ModelObject(&tree_01_, LittleLot::Vector3(0, 0, -4), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_02_, LittleLot::Vector3(1, 0, -4), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(2, 0, -4), LittleLot::Vector3(0, 45, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(3, 0, -4), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(4, 0, -4), LittleLot::Vector3(0, 0, 0), 0.3f));

	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(0.2f, 0, -3), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_01_, LittleLot::Vector3(0.8f, 0, -2.8f), LittleLot::Vector3(0, 172, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(1.2f, 0, -3.2f), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(1.6f, 0, -2.6f), LittleLot::Vector3(0, 32, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(2.f, 0, -2.9f), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(2.f, 0, -2.9f), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_01_, LittleLot::Vector3(3.1f, 0, -2.9f), LittleLot::Vector3(0, 64, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(3.2f, 0, -2.2f), LittleLot::Vector3(0, 45, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_02_, LittleLot::Vector3(3.8f, 0, -2.9f), LittleLot::Vector3(0, 90, 0), 0.3f));

	// forward left
	table_top_objects_.AddObject(new ModelObject(&tree_01_, LittleLot::Vector3(0, 0, 4), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_02_, LittleLot::Vector3(-1, 0, 4), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(-2, 0, 4), LittleLot::Vector3(0, 45, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(-3, 0, 4), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(-4, 0, 4), LittleLot::Vector3(0, 0, 0), 0.3f));

	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(-0.2f, 0, 3), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_01_, LittleLot::Vector3(-0.8f, 0, 2.8f), LittleLot::Vector3(0, 172, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(-1.2f, 0, 3.2f), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(-1.6f, 0, 2.6f), LittleLot::Vector3(0, 32, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(-2.f, 0, 2.9f), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_04_, LittleLot::Vector3(-2.f, 0, 2.9f), LittleLot::Vector3(0, 0, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_01_, LittleLot::Vector3(-3.1f, 0, 2.9f), LittleLot::Vector3(0, 64, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(-3.2f, 0, 2.2f), LittleLot::Vector3(0, 45, 0), 0.3f));
	table_top_objects_.AddObject(new ModelObject(&tree_02_, LittleLot::Vector3(-3.8f, 0, 2.9f), LittleLot::Vector3(0, 90, 0), 0.3f));

	// campfire 
	table_top_objects_.AddObject(new ModelObject(&campfire_, LittleLot::Vector3(0, 0, 0), LittleLot::Vector3(0, 180, 0), 0.1f));
	// scattered trees	
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(0, 0, -2), LittleLot::Vector3(45, 45, 0), 0.1f));
	table_top_objects_.AddObject(new ModelObject(&tree_03_, LittleLot::Vector3(1, 0, -3), LittleLot::Vector3(86, 45, 0), 0.3f));
	
	//sheep
	table_top_objects_.AddObject(new ModelObject(&sheep_, LittleLot::Vector3(-2, 0, -2), LittleLot::Vector3(0, 72, 0), 0.2f));
	table_top_objects_.AddObject(new ModelObject(&sheep_, LittleLot::Vector3(3, 0, 3), LittleLot::Vector3(0, -64, 0), 0.2f));

	//rocks
	table_top_objects_.AddObject(new ModelObject(&rock_set_01_, LittleLot::Vector3(0, 0, 3.4f), LittleLot::Vector3(0, 90, 0), 0.1f));
	table_top_objects_.AddObject(new ModelObject(&rock_set_02_, LittleLot::Vector3(1, 0, 2.8f), LittleLot::Vector3(0, 45, 0), 0.1f));
	table_top_objects_.AddObject(new ModelObject(&rock_set_03_, LittleLot::Vector3(2, 0, 3.6f), LittleLot::Vector3(0, -90, 0), 0.1f));
	table_top_objects_.AddObject(new ModelObject(&rock_set_04_, LittleLot::Vector3(3, 0, 1.2f), LittleLot::Vector3(0, 90, 0), 0.1f));

	table_top_objects_.AddObject(new ModelObject(&rock_set_01_, LittleLot::Vector3(0, 0, -3.4f), LittleLot::Vector3(0, 90, 0), 0.1f));
	table_top_objects_.AddObject(new ModelObject(&rock_set_02_, LittleLot::Vector3(-1, 0, -2.8f), LittleLot::Vector3(0, 45, 0), 0.1f));
	table_top_objects_.AddObject(new ModelObject(&rock_set_03_, LittleLot::Vector3(-2, 0, -3.6f), LittleLot::Vector3(0, -90, 0), 0.1f));
	table_top_objects_.AddObject(new ModelObject(&rock_set_04_, LittleLot::Vector3(-3, 0, -1.2f), LittleLot::Vector3(0, 90, 0), 0.1f));

	// shock troopers
	for(int x = 0; x < 2; x++) {
		for(int i = 0; i < 5; i++) {
			table_top_objects_.AddObject(new ModelObject(&shock_trooper_, LittleLot::Vector3(-(5 + (float)x), 0, i - 2.f), LittleLot::Vector3(0, 90, 0), 0.2f));
			table_top_objects_.AddObject(new ModelObject(&shock_trooper_, LittleLot::Vector3((5 + (float)x), 0, i - 2.f), LittleLot::Vector3(0, -90, 0), 0.2f));
		}
	}
	//mechs
	table_top_objects_.AddObject(new ModelObject(&mech_, LittleLot::Vector3(-7, 0, 0), LittleLot::Vector3(0, 90, 0), 0.2f));
	table_top_objects_.AddObject(new ModelObject(&mech_, LittleLot::Vector3(7, 0, 0), LittleLot::Vector3(0, -90, 0), 0.2f));
}

void ModelRoom::BuildRoom() {
	
	// seperated out to allow unique rendering order
	room_floor_ = VertexDataObject(&floor_, &rock_floor_, Material(kPewter), LittleLot::Vector3(0, -4, 0), LittleLot::Vector3(0, 0, 0), 2);
	
	static_objects_.AddObject(new VertexDataObject(&floor_, &brick_, Material(kPewter), LittleLot::Vector3(0, 28, 0), LittleLot::Vector3(180, 0, 0), 2));
	// walls
	static_objects_.AddObject(new VertexDataObject(&wall_1_, &brick_, Material(kPewter), LittleLot::Vector3(0, 12, -16), LittleLot::Vector3(90, 0, 0), 8));
	static_objects_.AddObject(new VertexDataObject(&wall_1_, &brick_, Material(kPewter), LittleLot::Vector3(0, 12, 16), LittleLot::Vector3(-90, 0, 0), 8));

	static_objects_.AddObject(new VertexDataObject(&wall_2_, &brick_, Material(kPewter), LittleLot::Vector3(32, 12, 0), LittleLot::Vector3(90, 0, 90), 8));
	static_objects_.AddObject(new VertexDataObject(&wall_2_, &brick_, Material(kPewter), LittleLot::Vector3(-32, 12, 0), LittleLot::Vector3(-90, 0, -90), 8));

	// independent rendering order even though static
	window_ = VertexDataObject(&cube_, nullptr, Material(kPewter), LittleLot::Vector3(31.98f, 4, 0), LittleLot::Vector3(0, -90, 0), LittleLot::Vector3(16, 8, 1));

	// security camera
	static_objects_.AddObject(new VertexDataObject(&cube_, nullptr, Material(DefaultMaterials::kSilver), LittleLot::Vector3(-31, 27, 15), LittleLot::Vector3(0, 45, -35), LittleLot::Vector3(2, 1, 1)));
	static_objects_.AddObject(new VertexDataObject(&cube_, nullptr, Material(DefaultMaterials::kSilver), LittleLot::Vector3(-31.5f, 27.5f, 15.5f), LittleLot::Vector3(0, 45, 0), LittleLot::Vector3(1, 1, 1)));

	// Table, again independent rendering order, from the rest of the tabletop, for shadows etc.
	table_ = VertexDataObject(&cube_, nullptr, Material(kPewter), LittleLot::Vector3(0, -2, 0), LittleLot::Vector3(0, 0, 0), LittleLot::Vector3(16, 3.9f, 10));
	SuspendedLight* suspended_light_;

	suspended_light_ = new SuspendedLight(&cube_, Material(DefaultMaterials::kPolishedBronze), &spot_light_,LittleLot::Vector3(-26, 28, 0));
	suspended_light_->set_beam_length(24);
	
	dynamic_objects_.AddObject(suspended_light_);

	suspended_light_ = new SuspendedLight(&cube_, Material(DefaultMaterials::kObsidian), &spot_light_2_, LittleLot::Vector3(26, 28, 0));
	suspended_light_->set_beam_length(24);

	dynamic_objects_.AddObject(suspended_light_);
		
}
