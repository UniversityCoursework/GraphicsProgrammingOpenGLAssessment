#ifndef _MODEL_OBJECT_H
#define _MODEL_OBJECT_H

#include "vector3.h"

#include "model.h"
#include "vertex_data.h"
#include "gameobject.h"

class ModelObject : public GameObject {
public:
	ModelObject();
	ModelObject(Model* model, LittleLot::Vector3 position, LittleLot::Vector3 rotation, LittleLot::Vector3 scale);
	ModelObject(Model* model, LittleLot::Vector3 position, LittleLot::Vector3 rotation, float scale);
	~ModelObject();
	
	void set_model(Model *model) { model_ = model; }
	Model* const model() const { return model_; }

	virtual void Render() override;

protected:
	Model *model_;

};

#endif
#pragma once
