#include "screen.h"

#include "screen_manager.h"
#include "camera.h"

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

Screen::Screen(ScreenManager * screen_manager) {
	screen_manager_ = screen_manager;
	needs_deleted_ = false;
	loading_ = false;
	state_ = kActive;
	percentage_ = 0;
}

void Screen::Begin3D(Camera &camera) {
	// 3D Scene rendering
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// reset projection for 3d perspective view
	gluPerspective(45.0f, (GLfloat)screen_manager_->ScreenWidth() / (GLfloat)screen_manager_->ScreenHeight(), 0.01f, 1500.0f);
	glMatrixMode(GL_MODELVIEW);	// Select The Modelview Matrix
	glLoadIdentity();			// Reset The Modelview Matrix

								//set camera looking down the -z axis,  6 units away from the center
	gluLookAt(camera.position().x, camera.position().y, camera.position().z,
			  camera.look_at().x, camera.look_at().y, camera.look_at().z,
			  camera.up().x, camera.up().y, camera.up().z); //Where we are, What we look at, and which way is up
}

void Screen::Begin2D() {
	glDisable(GL_LIGHTING);
	// 2D Ortho Hud Rendering
	glMatrixMode(GL_PROJECTION);
	// change to ortho for 2d hud
	glLoadIdentity();
	// set to window size
	gluOrtho2D(0, (GLfloat)screen_manager_->ScreenWidth(), (GLfloat)screen_manager_->ScreenHeight(), 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);	
}

void Screen::End2D() {
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);	
}

void Screen::Pause(bool is_overlayed) {
	if(is_overlayed) {
		state_ = kOverlayed;
	} else {
		state_ = kHidden;
	}
}

void Screen::Resume() {
	state_ = kActive;
}
