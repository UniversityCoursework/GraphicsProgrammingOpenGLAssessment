#ifndef _CAMERA_MANAGER_H
#define _CAMERA_MANAGER_H

#include <map>
#include <algorithm>    // std::find_if
#include <string>
#include <windows.h>

class Camera;
class Input;

class CameraManager {	
public:
	CameraManager() {}
	void Init(Input *input, RECT *screen_rect, HWND *hwnd);
	void AddCamera(std::string tag,Camera *camera,bool replace = false);
	void DeleteCamera(std::string tag);
	void ChangeCamera(std::string tag);
	void CleanUp();

	void Update(float dt);

	Input* const GetInput() const { return input_; }

	long const ScreenWidth() const { return screen_rect_->right; }
	long const ScreenHeight() const { return screen_rect_->bottom; }

	RECT const GetScreenRect() const { return *screen_rect_; }
	HWND* const GetWindowHandle() const { return hwnd_; }

	Camera* const GetCurrentCamera() { return cameras_[GetCurrentCameraTag()]; }


	std::string const GetCurrentCameraTag() const { return current_camera_tag_; }
	
protected:	
	std::map<std::string, Camera*> cameras_;
	std::string current_camera_tag_;
	Input *input_;
	RECT *screen_rect_;
	HWND *hwnd_;
};
#endif
