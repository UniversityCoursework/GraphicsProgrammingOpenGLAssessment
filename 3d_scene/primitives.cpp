#include "primitives.h"

#include "math_utils.h"
#include "vector2.h"

#include <math.h>

// Adds a point at v0 with normal matching v0, and texture tx0 to the object.
void AddPoint(VertexData &sphere, LittleLot::Vector3 v0, LittleLot::Vector2 tx0) {
	sphere.verts_.push_back(v0.x);
	sphere.verts_.push_back(v0.y);
	sphere.verts_.push_back(v0.z);

	sphere.normals_.push_back(v0.x);
	sphere.normals_.push_back(v0.y);
	sphere.normals_.push_back(v0.z);

	sphere.tex_coords_.push_back(tx0.x);
	sphere.tex_coords_.push_back(tx0.y);
}

void AddPoint(VertexData &sphere, LittleLot::Vector3 v0, LittleLot::Vector2 tx0, LittleLot::Vector3 n0) {
	sphere.verts_.push_back(v0.x);
	sphere.verts_.push_back(v0.y);
	sphere.verts_.push_back(v0.z);

	sphere.normals_.push_back(n0.x);
	sphere.normals_.push_back(n0.y);
	sphere.normals_.push_back(n0.z);

	sphere.tex_coords_.push_back(tx0.x);
	sphere.tex_coords_.push_back(tx0.y);
}

// Adds 2 triangles to the vertexdata from the provided 4 points, going anti clockwise from top left, normailising the vertex points for the normals.
void AddQuad(VertexData &sphere, LittleLot::Vector3 v0, LittleLot::Vector3 v1, LittleLot::Vector3 v2, LittleLot::Vector3 v3,
			 LittleLot::Vector2 tx0, LittleLot::Vector2 tx1, LittleLot::Vector2 tx2, LittleLot::Vector2 tx3) {
	v0.Normalise();
	v1.Normalise();
	v2.Normalise();
	v3.Normalise();

	AddPoint(sphere, v0, tx0);
	AddPoint(sphere, v1, tx1);
	AddPoint(sphere, v3, tx3);

	AddPoint(sphere, v2, tx2);
	AddPoint(sphere, v3, tx3);
	AddPoint(sphere, v1, tx1);
}

void AddQuad(VertexData &object, LittleLot::Vector3 v0, LittleLot::Vector3 v1, LittleLot::Vector3 v2, LittleLot::Vector3 v3,
	LittleLot::Vector2 tx0, LittleLot::Vector2 tx1, LittleLot::Vector2 tx2, LittleLot::Vector2 tx3,
	LittleLot::Vector3 n0, LittleLot::Vector3 n1, LittleLot::Vector3 n2, LittleLot::Vector3 n3) {
	
	AddPoint(object, v0, tx0,n0);
	AddPoint(object, v1, tx1, n1);
	AddPoint(object, v3, tx3, n3);

	AddPoint(object, v2, tx2, n2);
	AddPoint(object, v3, tx3, n3);
	AddPoint(object, v1, tx1, n1);
}


// Divide a quad into 4 child quads, with texcoords, Divides recursivly until resolution = 0
void DivideFace(VertexData &object,
				LittleLot::Vector3 v0, LittleLot::Vector3 v1, LittleLot::Vector3 v2, LittleLot::Vector3 v3,
				LittleLot::Vector2 tx0, LittleLot::Vector2 tx1, LittleLot::Vector2 tx2, LittleLot::Vector2 tx3,
				int resolution) {
	if(resolution <= 0) {
		AddQuad(object, v0, v1, v2, v3, tx0, tx1, tx2, tx3);
		return;
	}
	/*
	3	e2	2
	e3	e5	e4
	0	e1	1
	*/
	// edge vectors
	LittleLot::Vector3 e1 = (v1 - v0);
	LittleLot::Vector3 e2 = (v2 - v3);
	LittleLot::Vector3 e3 = (v3 - v0);
	LittleLot::Vector3 e4 = (v2 - v1);

	/*
	3	p2	2
	p3	p5	p4
	0	p1	1
	*/
	LittleLot::Vector3 p1 = (e1 * 0.5f) + v0;
	LittleLot::Vector3 p2 = (e2 * 0.5f) + v3;
	LittleLot::Vector3 p3 = (e3 * 0.5f) + v0;
	LittleLot::Vector3 p4 = (e4 * 0.5f) + v1;

	LittleLot::Vector3 e5 = (p2 - p1);
	LittleLot::Vector3 p5 = (e5 * 0.5f) + p1;

	// uv edge
	LittleLot::Vector2 txe1 = (tx1 - tx0);
	LittleLot::Vector2 txe2 = (tx2 - tx3);
	LittleLot::Vector2 txe3 = (tx3 - tx0);
	LittleLot::Vector2 txe4 = (tx2 - tx1);

	LittleLot::Vector2 txp1 = (txe1 * 0.5f) + tx0;
	LittleLot::Vector2 txp2 = (txe2 * 0.5f) + tx3;
	LittleLot::Vector2 txp3 = (txe3 * 0.5f) + tx0;
	LittleLot::Vector2 txp4 = (txe4 * 0.5f) + tx1;

	LittleLot::Vector2 txe5 = (txp2 - txp1);
	LittleLot::Vector2 txp5 = (txe5 * 0.5f) + txp1;

	DivideFace(object, v0, p1, p5, p3, tx0, txp1, txp5, txp3, resolution - 1);
	DivideFace(object, p3, p5, p2, v3, txp3, txp5, txp2, tx3, resolution - 1);
	DivideFace(object, p1, v1, p4, p5, txp1, tx1, txp4, txp5, resolution - 1);
	DivideFace(object, p5, p4, v2, p2, txp5, txp4, tx2, txp2, resolution - 1);
}

// Get Uv coords, for normal provided on sphere.
void GetTextureCoord(LittleLot::Vector3 normal, float &u, float &v) {
	u = 1.0f - atan2(normal.z, normal.x) / (2.f * PI);
	v = 0.5f - asin(normal.y) / PI;
}

// Used for spheres, to correct for zig/zipper at edge casses, i.e. u, 0.9 -> 0 Then Adds the 3 points and their uv's
void AddTriangle(VertexData &sphere, LittleLot::Vector3 v1, LittleLot::Vector3 v2, LittleLot::Vector3 v3) {
	float u1, u2, u3;
	float v_1, v_2, v_3;

	// Edge cases on U 0.9 -> 0.0
	float tex_add1 = 0;
	float tex_add2 = 0;
	float tex_add3 = 0;

	// Zigzag unzipper // basically offsets at edge points to remove error from shared points
	GetTextureCoord(v1, u1, v_1);
	GetTextureCoord(v2, u2, v_2);
	GetTextureCoord(v3, u3, v_3);

	if(u2 - u1 > 0.2 || u3 - u1 > 0.2) {
		tex_add1 = 1;
	}
	if(u1 - u2 > 0.2 || u3 - u2 > 0.2) {
		tex_add2 = 1;
	}
	if(u1 - u3 > 0.2 || u2 - u3 > 0.2) {
		tex_add3 = 1;
	}

	AddPoint(sphere, v1, LittleLot::Vector2(u1 + tex_add1, v_1));
	AddPoint(sphere, v2, LittleLot::Vector2(u2 + tex_add2, v_2));
	AddPoint(sphere, v3, LittleLot::Vector2(u3 + tex_add3, v_3));
	
	
}

// Subdivide a given triangle by splitting each edge in half
void SubDivideTriangle(VertexData &sphere, LittleLot::Vector3 v1, LittleLot::Vector3 v2, LittleLot::Vector3 v3, int depth) {
	// if depth zero add the triangle
	if(depth == 0) {
		AddTriangle(sphere, v3, v2, v1);
		return;
	}
	// generate midpoints of triangle
	LittleLot::Vector3 v12 = (v1 + v2) / 2;
	LittleLot::Vector3 v23 = (v2 + v3) / 2;
	LittleLot::Vector3 v31 = (v3 + v1) / 2;
	v12.Normalise();
	v23.Normalise();
	v31.Normalise();
	// subdivide the 4 triangles
	/*
	/\
	/\/\
	*/
	SubDivideTriangle(sphere, v1, v12, v31, depth - 1);
	SubDivideTriangle(sphere, v12, v2, v23, depth - 1);
	SubDivideTriangle(sphere, v31, v23, v3, depth - 1);
	SubDivideTriangle(sphere, v12, v23, v31, depth - 1);
}

VertexData BuildCube() {
	GLuint indices[] = {
		0,1,2,
		3,2,1,

		4,5,6,
		7,6,5,

		8,9,10,
		11,10,9,

		12,13,14,
		15,14,13,

		16,17,18,
		19,18,17,

		20,21,22,
		23,22,21
	};

	float verts[] = {
		-0.5f,  0.5f,  0.5f,	// front
		-0.5f, -0.5f,  0.5f,
		0.5f,  0.5f,  0.5f,

		0.5f, -0.5f,  0.5f,	//+ front

		0.5f,  0.5f, -0.5f,	// back
		0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,

		-0.5f, -0.5f, -0.5f,	//+ back

		0.5f,  0.5f,  0.5f,	// right
		0.5f, -0.5f,  0.5f,
		0.5f,  0.5f, -0.5f,

		0.5f, -0.5f, -0.5f,	//+ right

		-0.5f,  0.5f, -0.5f,	// left
		-0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f,  0.5f,

		-0.5f, -0.5f,  0.5f,	//+ left

		-0.5f,  0.5f, -0.5f,	// top
		-0.5f,  0.5f,  0.5f,
		0.5f,  0.5f, -0.5f,

		0.5f,  0.5f,  0.5f,	//+ top

		-0.5f, -0.5f,  0.5f,		// bottom
		-0.5f, -0.5f, -0.5f,
		0.5f, -0.5f,  0.5f,

		0.5f, -0.5f, -0.5f,		//+ bottom			
	};

	float normals[] = {
		0.0, 0.0, 1.0,		// front 
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,

		0.0, 0.0, -1.0,		// back 
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,

		1.0, 0.0, 0.0,		// right 
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,

		-1.0, 0.0, 0.0,		// left 
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,

		0.0, 1.0, 0.0,		// top
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,

		0.0, -1.0, 0.0,		// bottom
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,

	};

	float tex_coords[] = {
		0.0, 0.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		//1
		0.0, 0.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		//2
		0.0, 0.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		//3
		0.0, 0.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		//4
		0.0, 0.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		//5
		0.0, 0.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		//6			
	};

	VertexData object;

	object.indices_.reserve(sizeof(indices) / sizeof(GLuint));
	for(int i = 0; i < sizeof(indices) / sizeof(GLuint); i++) {
		object.indices_.push_back(indices[i]);
	}
	object.verts_.reserve(sizeof(verts) / sizeof(float));
	for(int i = 0; i < sizeof(verts) / sizeof(float); i++) {
		object.verts_.push_back(verts[i]);
	}
	object.normals_.reserve(sizeof(normals) / sizeof(float));
	for(int i = 0; i < sizeof(normals) / sizeof(float); i++) {
		object.normals_.push_back(normals[i]);
	}
	object.tex_coords_.reserve(sizeof(tex_coords) / sizeof(float));
	for(int i = 0; i < sizeof(tex_coords) / sizeof(float); i++) {
		object.tex_coords_.push_back(tex_coords[i]);
	}
	object.uses_indices_ = true;
	return object;
}

VertexData BuildSkyBox() {
	GLuint indices[] = {
		2,1,0,
		1,2,3,

		6,5,4,
		5,6,7,

		10,9,8,
		9,10,11,

		14,13,12,
		13,14,15,

		18,17,16,
		17,18,19,

		22,21,20,
		21,22,23
	};

	float verts[] = {
		-0.5f,  0.5f,  0.5f,	// front
		-0.5f, -0.5f,  0.5f,
		0.5f,  0.5f,  0.5f,

		0.5f, -0.5f,  0.5f,	//+ front

		0.5f,  0.5f, -0.5f,	// back
		0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,

		-0.5f, -0.5f, -0.5f,	//+ back

		0.5f,  0.5f,  0.5f,	// right
		0.5f, -0.5f,  0.5f,
		0.5f,  0.5f, -0.5f,

		0.5f, -0.5f, -0.5f,	//+ right

		-0.5f,  0.5f, -0.5f,	// left
		-0.5f, -0.5f, -0.5f,
		-0.5f,  0.5f,  0.5f,

		-0.5f, -0.5f,  0.5f,	//+ left

		-0.5f,  0.5f, -0.5f,	// top
		-0.5f,  0.5f,  0.5f,
		0.5f,  0.5f, -0.5f,

		0.5f,  0.5f,  0.5f,	//+ top

		-0.5f, -0.5f,  0.5f,		// bottom
		-0.5f, -0.5f, -0.5f,
		0.5f, -0.5f,  0.5f,

		0.5f, -0.5f, -0.5f,		//+ bottom			
	};

	float normals[] = {
		0.0, 0.0, 1.0,		// front 
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,

		0.0, 0.0, -1.0,		// back 
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,

		1.0, 0.0, 0.0,		// right 
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,

		-1.0, 0.0, 0.0,		// left 
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,

		0.0, 1.0, 0.0,		// top
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,

		0.0, -1.0, 0.0,		// bottom
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,

	};

	float tex_coords[] = {
		0.25, 0.25,
		0.25, 0.5,
		0.5, 0.25,
		0.5, 0.5,
		//1
		0.75, 0.25,
		0.75, 0.5,
		1.0, 0.25,
		1.0, 0.5,
		//2
		0.5, 0.25,
		0.5, 0.5,
		0.75, 0.25,
		0.75, 0.5,
		//3
		0.0, 0.25,
		0.0, 0.5,
		0.25, 0.25,
		0.25, 0.5,
		//4
		0.25, 0.0,
		0.25, 0.25,
		0.5, 0.0,
		0.5, 0.25,
		//5
		0.25, 0.5,
		0.25, 0.75,
		0.5, 0.5,
		0.5, 0.75,
		//6			
	};

	VertexData object;

	object.indices_.reserve(sizeof(indices) / sizeof(GLuint));
	for(int i = 0; i < sizeof(indices) / sizeof(GLuint); i++) {
		object.indices_.push_back(indices[i]);
	}
	object.verts_.reserve(sizeof(verts) / sizeof(float));
	for(int i = 0; i < sizeof(verts) / sizeof(float); i++) {
		object.verts_.push_back(verts[i]);
	}
	object.normals_.reserve(sizeof(normals) / sizeof(float));
	for(int i = 0; i < sizeof(normals) / sizeof(float); i++) {
		object.normals_.push_back(normals[i]);
	}
	object.tex_coords_.reserve(sizeof(tex_coords) / sizeof(float));
	for(int i = 0; i < sizeof(tex_coords) / sizeof(float); i++) {
		object.tex_coords_.push_back(tex_coords[i]);
	}

	object.uses_indices_ = true;
	return object;
}

VertexData BuildQuad() {
	GLuint indices[] = {
		0,1,2,
		3,2,1,
	};
	float verts[] = {
		-0.5f,  0.5f,  0.0f,	// front
		-0.5f, -0.5f,  0.0f,
		0.5f,  0.5f,  0.0f,
		0.5f, -0.5f,  0.0f	//+ front
	};
	float normals[] = {
		0.0, 0.0, 1.0,		// front 
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
	};
	float tex_coords[] = {
		0.0, 0.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
	};
	VertexData object;

	object.indices_.reserve(sizeof(indices) / sizeof(GLuint));
	for(int i = 0; i < sizeof(indices) / sizeof(GLuint); i++) {
		object.indices_.push_back(indices[i]);
	}
	object.verts_.reserve(sizeof(verts) / sizeof(float));
	for(int i = 0; i < sizeof(verts) / sizeof(float); i++) {
		object.verts_.push_back(verts[i]);
	}
	object.normals_.reserve(sizeof(normals) / sizeof(float));
	for(int i = 0; i < sizeof(normals) / sizeof(float); i++) {
		object.normals_.push_back(normals[i]);
	}
	object.tex_coords_.reserve(sizeof(tex_coords) / sizeof(float));
	for(int i = 0; i < sizeof(tex_coords) / sizeof(float); i++) {
		object.tex_coords_.push_back(tex_coords[i]);
	}
	object.uses_indices_ = true;
	return object;
}

VertexData BuildPlane(int width, int height){
	float verts[] = {
		-0.5f, 0.f, -0.5f,
		-0.5f, 0.f, 0.5f,
		0.5f, 0.f, 0.5f,
		0.5f, 0.f, -0.5f
	};
	float normals[] = {
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
	};
	float tex_coords[] = {
		0.0, 0.0,
		0.0, 1.0,
		1.0, 1.0,
		1.0, 0.0
	};
	VertexData plane;
	// plus 1 as x offsets to left when plane drawn, so for true centre add one and divide by 2
	float start_z = (float)(height) / 2.f;
	float start_x = (float)(width) / 2.f;
	float end_z = (float)(height) / 2.f;
	float end_x = (float)(width) / 2.f;

	start_z -= 0.5f;
	end_z += 0.5f;
	start_x -= 0.5f;
	end_x += 0.5f;

	for (float z = -start_z; z < end_z; z++) {
		for (float x = -start_x; x < end_x; x++) {
			AddQuad(plane,
				LittleLot::Vector3(verts[0] + x, verts[1], verts[2] + z),
				LittleLot::Vector3(verts[3] + x, verts[4], verts[5] + z),
				LittleLot::Vector3(verts[6] + x, verts[7], verts[8] + z),
				LittleLot::Vector3(verts[9] + x, verts[10], verts[11] + z),

				LittleLot::Vector2(tex_coords[0],tex_coords[1]),
				LittleLot::Vector2(tex_coords[2], tex_coords[3]),
				LittleLot::Vector2(tex_coords[4], tex_coords[5]),
				LittleLot::Vector2(tex_coords[6], tex_coords[7]),

				LittleLot::Vector3(normals[0], normals[1], normals[2]),
				LittleLot::Vector3(normals[3], normals[4], normals[5]),
				LittleLot::Vector3(normals[6], normals[7], normals[8]),
				LittleLot::Vector3(normals[9], normals[10], normals[11])
				);
		}
	}
	return plane;
}

VertexData BuildDisc(int resolution) {
	float interval_angle = 2 * PI / resolution;

	VertexData object;

	object.verts_.push_back(0.0f);//x
	object.verts_.push_back(0.0f);//y
	object.verts_.push_back(0.0f);//z

	object.tex_coords_.push_back(0.5);
	object.tex_coords_.push_back(0.5);

	object.normals_.push_back(0.0f);
	object.normals_.push_back(0.0f);
	object.normals_.push_back(1.0f);

	float theta = 0;
	float x = 0, y = 0, z = 0;
	float u = 0, v = 0, d = 2;
	for(int num_sides = 0; num_sides < resolution; num_sides++) {
		x = cos(theta);
		y = sin(theta);
		z = 0;

		u = (x / d) + 0.5f;
		v = (y / d) + 0.5f;
		// vertices
		object.verts_.push_back(x);
		object.verts_.push_back(y);
		object.verts_.push_back(z);
		//tex coords
		object.tex_coords_.push_back(u);
		object.tex_coords_.push_back(v);
		// normals
		object.normals_.push_back(0.0f);
		object.normals_.push_back(0.0f);
		object.normals_.push_back(1.0f);
		theta += interval_angle;

		// indices
		int temp_index = num_sides;
		object.indices_.push_back(0);
		temp_index += 1;
		object.indices_.push_back(temp_index);
		if(temp_index < resolution) {
			temp_index += 1;
		} else {
			temp_index = 1;
		}
		object.indices_.push_back(temp_index);

	}
	object.uses_indices_ = true;
	return object;
}

VertexData BuildCubeSphere(int resolution) {
	VertexData object;
	LittleLot::Vector2 tex_coords[] = {
		{0.25, 0.25},{0.25, 0.5},{0.5, 0.5},{0.5, 0.25},//1
		{0.75, 0.25},{0.75, 0.5},{1.0, 0.5},{1.0, 0.25},//2
		{0.5, 0.25},{0.5, 0.5},{0.75, 0.5},{0.75, 0.25},//3
		{0.0, 0.25},{0.0, 0.5},{0.25, 0.5},{0.25, 0.25},//4
		{0.25, 0.0},{0.25, 0.25},{0.5, 0.25},{0.5, 0.0},//5
		{0.25, 0.5},{0.25, 0.75},{0.5, 0.75},{0.5, 0.5} //6
	};

	LittleLot::Vector3 verts[24] = {
		{-0.5f, 0.5f, 0.5f},{-0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, 0.5f},{0.5f, 0.5f, 0.5f},		// front

		{0.5f, 0.5f, -0.5f},{0.5f, -0.5f, -0.5f},{-0.5f, -0.5f, -0.5f},{-0.5f, 0.5f, -0.5f},	// back

		{0.5f, 0.5f, 0.5f},{0.5f, -0.5f, 0.5f},{0.5f, -0.5f, -0.5f},{0.5f, 0.5f, -0.5f},		// right

		{-0.5f, 0.5f, -0.5f},{-0.5f, -0.5f, -0.5f},{-0.5f, -0.5f, 0.5f},{-0.5f, 0.5f, 0.5f},	// left

		{-0.5f, 0.5f, -0.5f},{-0.5f, 0.5f, 0.5f},{0.5f, 0.5f, 0.5f},{0.5f, 0.5f, -0.5f},		// top

		{-0.5f, -0.5f, 0.5f},{-0.5f, -0.5f, -0.5f}, {0.5f, -0.5f, -0.5f},{0.5f, -0.5f, 0.5f}	// bottom
	};
	for(int i = 0; i < 24; i += 4) {
		DivideFace(object, 
				   verts[i], verts[i + 1], verts[i + 2], verts[i + 3], 
				   tex_coords[i], tex_coords[i + 1], tex_coords[i + 2], tex_coords[i + 3],
				   resolution);		
	}
	return object;
}

VertexData BuildGeodesicSphere(int depth) {
	VertexData geo_sphere;

	// create icosahedron
	const float X = 0.525731112119133606f;
	const float Z = 0.850650808352039932f;
	const LittleLot::Vector3 verts[12] = {
		{-X, 0.0, Z},{X, 0.0, Z},{-X, 0.0, -Z},{X, 0.0, -Z},
		{0.0, Z, X},{0.0, Z, -X},{0.0, -Z, X},{0.0, -Z, -X},
		{Z, X, 0.0},{-Z, X, 0.0},{Z, -X, 0.0},{-Z, -X, 0.0}
	};
	int indices[20][3] = {
		{0, 4, 1},{0, 9, 4},{9, 5, 4},{4, 5, 8},{4, 8, 1},
		{8, 10, 1},{8, 3, 10},{5, 3, 8},{5, 2, 3},{2, 7, 3},
		{7, 10, 3},{7, 6, 10},{7, 11, 6},{11, 0, 6},{0, 1, 6},
		{6, 1, 10},{9, 0, 11},{9, 11, 2},{9, 2, 5},{7, 2, 11}
	};

	for(int i = 0; i < 20; i++) {
		SubDivideTriangle(geo_sphere, verts[indices[i][0]], verts[indices[i][1]], verts[indices[i][2]], depth);
	}

	return geo_sphere;
}

VertexData BuildSphere(int resolution) {
	VertexData object;

	float theta = 2 * PI / resolution;
	float delta = PI / resolution;
	bool flip = true;
	for(int longitude = 0; longitude < resolution; longitude++) {
		for(int latitude = 0; latitude < resolution; latitude++) {

			LittleLot::Vector3 v1, v2, v3;

			v1.x = cos(theta*latitude)*sin(delta*longitude);
			v1.y = -cos(delta*longitude);
			v1.z = sin(theta*latitude)*sin(delta*longitude);

			v2.x = cos(theta*(latitude + 1))*sin(delta*longitude);
			v2.y = -cos(delta*longitude);
			v2.z = sin(theta*(latitude + 1))*sin(delta*longitude);

			v3.x = cos(theta*latitude)*sin(delta*(longitude + 1));
			v3.y = -cos(delta*(longitude + 1));
			v3.z = sin(theta*latitude)*sin(delta*(longitude + 1));

			AddTriangle(object, v3, v2, v1);

			v1.x = cos(theta*latitude)*sin(delta*longitude);
			v1.y = -cos(delta*longitude);
			v1.z = sin(theta*latitude)*sin(delta*longitude);

			v2.x = cos(theta*latitude)*sin(delta*(longitude + 1));
			v2.y = -cos(delta*(longitude + 1));
			v2.z = sin(theta*latitude)*sin(delta*(longitude + 1));

			v3.x = cos(theta*(latitude - 1))*sin(delta*(longitude + 1));
			v3.y = -cos(delta*(longitude + 1));
			v3.z = sin(theta*(latitude - 1))*sin(delta*(longitude + 1));

			AddTriangle(object, v3, v2, v1);


		}
	}
	return object;

}

