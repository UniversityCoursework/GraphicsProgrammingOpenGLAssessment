#include "vertex_data.h"

void RenderVDA(VertexData &object) {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, object.verts_.data());
	glNormalPointer(GL_FLOAT, 0, object.normals_.data());
	glTexCoordPointer(2, GL_FLOAT, 0, object.tex_coords_.data());

	glDrawArrays(GL_TRIANGLES, 0, object.verts_.size() / 3);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void RenderVDE(VertexData &object) {
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, object.verts_.data());
	glNormalPointer(GL_FLOAT, 0, object.normals_.data());
	glTexCoordPointer(2, GL_FLOAT, 0, object.tex_coords_.data());

	glDrawElements(GL_TRIANGLES, object.indices_.size(), GL_UNSIGNED_INT, object.indices_.data());

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}
void DebugAxis(){	
	glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, NULL);
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
			glColor3f(0, 0, 1);
			glVertex3f(0, 0, 0);
			glVertex3f(1, 0, 0);

			glColor3f(0, 1, 0);
			glVertex3f(0, 0, 0);
			glVertex3f(0, 0, -1);

			glColor3f(1, 0, 0);
			glVertex3f(0, 0, 0);
			glVertex3f(0, 1, 0);
		glEnd();	
		glEnable(GL_LIGHTING);
	glPopMatrix();
}

void RenderVertexData(VertexData &object) {
	if (object.uses_indices_) {
		RenderVDE(object);
	}
	else {
		RenderVDA(object);
	}	
}
