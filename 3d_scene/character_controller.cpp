#include "character_controller.h"

#include "math_utils.h"
CharacterController::CharacterController(){
	model_ = nullptr;
	update_required_ = true;
	start_ = LittleLot::Vector3::kZero;
	end_ = LittleLot::Vector3::kZero;
}
CharacterController::CharacterController(ModelObject *controled_model, float movespeed, float rotation_speed, Input *input, HWND* hwnd, RECT *rect){
	model_ = controled_model;
	movespeed_ = movespeed;
	rotation_speed_ = rotation_speed;
	input_ = input;
	hwnd_ = hwnd;
	rect_ = rect;
}
CharacterController::~CharacterController(){
	model_ = nullptr;
}

void CharacterController::UpdateInput(float dt){
	if (input_->is_key_down('W')) {
		MoveForward(dt);
	}
	if (input_->is_key_down('S')) {
		MoveBackward(dt);
	}
	if (input_->is_key_down('A')) {
		MoveLeft(dt);
	}
	if (input_->is_key_down('D')) {
		MoveRight(dt);
	}
	// checks if the main windows has focus, i.e. no popups.
	if (GetActiveWindow() == *hwnd_) {
		MouseYaw(input_->mouse_x(), *rect_, dt);
		ResetMouseToCentreScreen(*hwnd_, *rect_);
	}
	if (update_required_) {
		float cos_r, cos_p, cos_y;
		float sin_r, sin_p, sin_y;
		// pre calculated regulary used variables
		cos_y = cosf(LittleLot::DegreesToRadians(-model_->rotation().y));
		cos_p = cosf(LittleLot::DegreesToRadians(model_->rotation().x));
		cos_r = cosf(LittleLot::DegreesToRadians(model_->rotation().z));

		sin_y = sinf(LittleLot::DegreesToRadians(-model_->rotation().y));
		sin_p = sinf(LittleLot::DegreesToRadians(model_->rotation().x));
		sin_r = sinf(LittleLot::DegreesToRadians(model_->rotation().z));

		// forward vector direction of the camera
		forward_.x = sin_y * cos_p;
		forward_.y = sin_p;
		forward_.z = cos_p * -cos_y;		

		// up vector 
		LittleLot::Vector3 up;
		up.x = -cos_y * sin_r - sin_y * sin_p * cos_r;
		up.y = cos_p * cos_r;
		up.z = -sin_y * sin_r - sin_p * cos_r * -cos_y;
				
		right_ = forward_.CrossProduct(up);
		
		update_required_ = false;
	}
		
	model_->set_position(LittleLot::ClampBetweenVectors(model_->position(), start_, end_));
}

void CharacterController::SetCharacterBounds(LittleLot::Vector3 start, LittleLot::Vector3 end){
	start_ = start;
	end_ = end;
}

void CharacterController::OnActivated(){
	LittleLot::DisplayCursor(false);	
	ResetMouseToCentreScreen(*hwnd_, *rect_);
}


void CharacterController::MoveForward(float dt) {
	model_->set_position(model_->position() - forward()*movespeed_*dt);
	update_required_ = true;
}
void CharacterController::MoveBackward(float dt) {
	model_->set_position(model_->position() + forward()*movespeed_*dt);
	update_required_ = true;
}

void CharacterController::MoveLeft(float dt) {
	model_->set_position(model_->position() + right()*movespeed_*dt);
	update_required_ = true;
}
void CharacterController::MoveRight(float dt) {
	model_->set_position(model_->position() - right()*movespeed_*dt);
	update_required_ = true;
}



void CharacterController::MouseYaw(int mouse_x, RECT screen_rect, float dt) {
	float cen_x = (float)screen_rect.right / 2.0f;	
	float error = 0.6f;
	float diff_x = mouse_x - cen_x;		
	if (abs(diff_x) > error) {		
		model_->set_rotation(LittleLot::Vector3(model_->rotation().x,
			model_->rotation().y - rotation_speed_*diff_x*dt,
			model_->rotation().z));
	}
	update_required_ = true;
}

void CharacterController::ResetMouseToCentreScreen(HWND &hwnd_, RECT screen_rect) {
	// stops from being turned on first frame
	POINT temp;
	temp.x = screen_rect.right / 2;
	temp.y = screen_rect.bottom / 2;
	ClientToScreen(hwnd_, &temp);
	SetCursorPos(temp.x, temp.y);
}