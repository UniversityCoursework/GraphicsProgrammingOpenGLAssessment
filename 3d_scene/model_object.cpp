#include "model_object.h"

ModelObject::ModelObject() {
	model_ = nullptr;
	position_ = LittleLot::Vector3::kZero;
	rotation_ = LittleLot::Vector3::kZero;
	scale_ = LittleLot::Vector3(1.0f,1.0f,1.0f);
}

ModelObject::ModelObject(Model * model, LittleLot::Vector3 position, LittleLot::Vector3 rotation, LittleLot::Vector3 scale) {
	model_ = model;
	position_ = position;
	rotation_ = rotation;
	scale_ = scale;
}

ModelObject::ModelObject(Model * model, LittleLot::Vector3 position, LittleLot::Vector3 rotation, float scale)
	:ModelObject(model,position,rotation,LittleLot::Vector3(scale,scale,scale)) {
}

ModelObject::~ModelObject() {
	model_ = nullptr;
}

void ModelObject::Render() {
	glEnable(GL_NORMALIZE);

	glPushMatrix();
	glTranslatef(position_.x, position_.y, position_.z);	
	glRotatef(rotation_.x, 1, 0, 0);
	glRotatef(rotation_.y, 0, 1, 0);
	glRotatef(rotation_.z, 0, 0, 1);
	glScalef(scale_.x, scale_.y, scale_.z);
	model_->Render();
	glPopMatrix();

	glDisable(GL_NORMALIZE);
}
