#include "loading_screen.h"
#include "screen_manager.h"
#include "vector3.h"
#include "font.h"

const float title_offset = 80;

LoadingScreen::LoadingScreen(ScreenManager * screen_manager)
	:Screen(screen_manager) {	
}

void LoadingScreen::Init() {
	
	background_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
	background_.set_width((float)screen_manager_->ScreenWidth() / 2.f);
	background_.set_height((float)screen_manager_->ScreenHeight() / 4.f);	
	
	dodad_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
	dodad_.set_width(32.f);
	dodad_.set_height(32.f);

	Colour colour;
	colour.set_rgba_255(93.f, 19.f, 114.f);
	background_.set_colour(colour);

	colour.set_rgba_255(121.f, 190.f, 0.f);
	dodad_.set_colour(colour);

	percentage_ = 0;
}

void LoadingScreen::CleanUp() {
}

void LoadingScreen::Resize() {
	background_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
	background_.set_width((float)screen_manager_->ScreenWidth() / 2.f);
	background_.set_height((float)screen_manager_->ScreenHeight() / 4.f);
	dodad_.set_position((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() / 2.f);
}

void LoadingScreen::Update(float dt) {
	std::vector<Screen* >::iterator screen = screen_manager_->GetIterTopScreen();
	screen--;
	if(!(*screen)->IsLoading(percentage_)) {
		screen_manager_->PopScreen();
	}
	dodad_.set_rotation(dodad_.rotation() + 0.1f *dt);
}

void LoadingScreen::Render(Font &font) {
	switch(state_) {
		case kActive:
			Begin2D();
			background_.DrawSprite();
			dodad_.DrawSprite();
			font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() / 2.f, ((float)screen_manager_->ScreenHeight() / 2.f) - title_offset), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Loading... %.0f %%", (percentage_ * 100));
			
			End2D();
			break;
		case kOverlayed:

			break;
		default:
			break;
	}
}
