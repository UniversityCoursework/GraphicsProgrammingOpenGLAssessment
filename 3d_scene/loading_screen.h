#ifndef _LOADING_SCREEN_H
#define _LOADING_SCREEN_H

#include "screen.h"

#include "sprite_2d.h"

class LoadingScreen : public Screen {
public:
	LoadingScreen(ScreenManager *screen_manager);

	void Init() override;
	void CleanUp() override;

	void Resize() override;

	virtual void Update(float dt) override;
	void Render(Font &font_) override;


protected:
	Sprite2D background_;
	Sprite2D dodad_;
	
	float percentage_;
};

#endif
