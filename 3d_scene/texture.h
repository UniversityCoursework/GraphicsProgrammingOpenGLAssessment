#ifndef _TEXTURE_H
#define _TEXTURE_H

#include <windows.h>
#include <gl/glu.h>

class Texture {
public:
	~Texture();
	void LoadTexture(const char* texture_name);
	GLuint const &data() const { return texture_; }
private:
	GLuint texture_;
	// Add height/width, etc data?
	// UV coordinates, so just change these before render?
	// Eventually add sprite sheet, which parses information to generate 2d array of sprites?
};

#endif
