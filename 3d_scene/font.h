#ifndef _FONT_H
#define _FONT_H

#include <istream>	//std::istream
#include <fstream>	//std::filebug

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include "vector2.h"
#include "vector3.h"

enum TextJustification {
	kTJ_LEFT = 0,
	kTJ_CENTRE,
	kTJ_RIGHT,
	kTJ_VerticalCentre,
	kTJ_VerticalLeft,
	kTJ_VerticalRight
};

class Font {
public:
	Font();
	~Font();
	void Load(const char* font_name);
	void RenderText(const LittleLot::Vector2& pos, const float scale, const LittleLot::Vector3 colour, const TextJustification justification, const char * text, ...);
		
	void Render3DText(const LittleLot::Vector3& pos,const LittleLot::Vector3& rotation, const float scale, const LittleLot::Vector3 colour, const TextJustification justification, const char * text, ...);
	
	void Begin3DText();
	void End3DText();
private:
	float GetStringLength(const char * text);
	struct CharDescriptor {		
		GLuint x, y;
		GLuint Width, Height;
		GLuint XOffset, YOffset;
		GLuint XAdvance;		

		CharDescriptor() : x(0), y(0), Width(0), Height(0), XOffset(0), YOffset(0),
			XAdvance(0) {
		}
	};

	struct Charset {
		GLshort LineHeight;
		GLshort Base;
		GLshort Width, Height;		
		CharDescriptor Chars[256];
	};

	bool ParseFont(std::istream& Stream, Charset& CharsetDesc);
	char* FormatArgsToChar(const char*text, va_list args);
	Charset character_set;
	GLuint font_texture_;
};


#endif // _ABFW_FONT_H