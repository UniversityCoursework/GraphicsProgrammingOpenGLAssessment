#include "earth_room.h"

#include "screen_manager.h"
#include "free_camera.h"
#include "math_utils.h"

#include "security_camera.h"
#include "static_camera.h"

#include "pause_screen.h"
#include "loading_screen.h"

#include "utils.h"

EarthRoom::EarthRoom(ScreenManager * screen_manager)
:Screen(screen_manager) {
}

void EarthRoom::Init() {
	main_light_ = Light(kZero, kPoint, 0, 2, 0);
	main_light_.set_constant_attenuation(0.6f);
	main_light_.set_linear_attenuation(0.0f);

	main_light_.Debug(true);

	main_light_.enable();

	loading_ = true;

	screen_manager_->PushScreen(new LoadingScreen(screen_manager_));
	Pause(false);
	load_barrier_.Init(8);
	screen_manager_->thread_farm().QueueTask([&] {
		main_skybox_.LoadTexture("bin/textures/space_cubemap.png");
		percentage_ = percentage_ + 0.2f;
		load_barrier_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		cloud_texture_.LoadTexture("bin/textures/earth_clouds.png");
		percentage_ = percentage_ + 0.2f;
		load_barrier_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		earth_texture_.LoadTexture("bin/textures/earth_cubemap.png");
		percentage_ = percentage_ + 0.2f;
		load_barrier_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		moon_texture_.LoadTexture("bin/textures/moon.png");
		percentage_ = percentage_ + 0.2f;
		load_barrier_.NotifyAll();
	});
	screen_manager_->thread_farm().QueueTask([&] {
		skybox_ = BuildSkyBox();		
		percentage_ = percentage_ + 0.01f;
		load_barrier_.NotifyAll();
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		cube_sphere_ = BuildCubeSphere(4);
		percentage_ = percentage_ + 0.01f;
		load_barrier_.NotifyAll();
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		geodesic_sphere_ = BuildGeodesicSphere(4);
		percentage_ = percentage_ + 0.01f;
		load_barrier_.NotifyAll();
	}, false);
	screen_manager_->thread_farm().QueueTask([&] {
		sphere_ = BuildSphere(32);
		percentage_ = percentage_ + 0.01f;
		load_barrier_.NotifyAll();
	}, false);

	screen_manager_->thread_farm().QueueTask([&] {
		load_barrier_.Wait();

		camera_manager_.Init(screen_manager_->GetInput(), screen_manager_->GetScreenRect(), screen_manager_->GetWindowHandle());
		camera_manager_.AddCamera("Free Camera", new FreeCam());
		Camera *cam = new SecurityCam(LittleLot::Vector3(16, 16, -16), LittleLot::Vector3(-60, -180, 0), LittleLot::Vector3(0, -100, 0));
		cam->set_yaw(-135);
		cam->set_pitch(-35);
		cam->set_rotation_speed(0.001f);
		camera_manager_.AddCamera("Security Camera, Controlled", cam);

		cam = new SecurityCam(LittleLot::Vector3(16, 16, -16), LittleLot::Vector3(-35, -160, 0), LittleLot::Vector3(-35, -100, 0),0.2f,0.1f);
		cam->set_yaw(-135);
		cam->set_pitch(-35);
		camera_manager_.AddCamera("Security Camera, Automated", cam);
		camera_manager_.AddCamera("Top Down", new StaticCam(LittleLot::Vector3(0, 40, 0), LittleLot::Vector3(-90, 0, 0)));
		camera_manager_.AddCamera("Side view", new StaticCam(LittleLot::Vector3(16, 0, 16), LittleLot::Vector3(0, -45, 0)));

		// Allow camera to call any needed functions
		camera_manager_.Update(0);

		relative_earth_rotation_ = LittleLot::Vector3(0.f, 180.f, 0.f);

		earth_position_ = LittleLot::Vector3(-2.f, 0.f, 12.f);
		earth_rotation_ = LittleLot::Vector3(0.f, 0.f, -23.44f);

		moon_relative_rotation_ = LittleLot::Vector3(0.f, 0.f, 5.14f);
		moon_relative_position_ = LittleLot::Vector3(3.f, 0.f, 0.f);
		moon_axis_rotation_ = LittleLot::Vector3(0.f, 0.f, -6.68f);
		cloud_uv_offset_ = 0.f;

		earth_rotation_amount_ = 0.1f;
		moon_relative_rotation_amount_ = 0.02f;
		earth_relative_rotation_amount_ = 0.001f;
		cloud_uv_amount_ = 0.0001f;


		// allows moon to rotate on unqiue axis.	
		float cos_r, cos_p, cos_y;
		float sin_r, sin_p, sin_y;
		// pre calculated regulary used variables
		cos_y = cosf(LittleLot::DegreesToRadians(moon_relative_rotation_.y));
		cos_p = cosf(LittleLot::DegreesToRadians(moon_relative_rotation_.x));
		cos_r = cosf(LittleLot::DegreesToRadians(moon_relative_rotation_.z));

		sin_y = sinf(LittleLot::DegreesToRadians(moon_relative_rotation_.y));
		sin_p = sinf(LittleLot::DegreesToRadians(moon_relative_rotation_.x));
		sin_r = sinf(LittleLot::DegreesToRadians(moon_relative_rotation_.z));
		// up vector 
		offset_axis_.x = -cos_y * sin_r - sin_y * sin_p * cos_r;
		offset_axis_.y = cos_p * cos_r;
		offset_axis_.z = -sin_y * sin_r - sin_p * cos_r * -cos_y;
		cloud_uv_offset_ = 0;
		loading_ = false;
	});	
	screen_manager_->thread_farm().StartThreads();
}

void EarthRoom::CleanUp() {
	camera_manager_.CleanUp();
}

bool EarthRoom::IsLoading(float & percentage) {
	percentage = percentage_;	
	return loading_;
}

void EarthRoom::Resize() {
}

void EarthRoom::Resume() {
	state_ = kActive;
	camera_manager_.GetCurrentCamera()->OnActivated();
}

void EarthRoom::Update(float dt) {
	switch(state_) {
		case kActive:
			if(screen_manager_->GetInput()->is_key_down(VK_F1)) {
				camera_manager_.ChangeCamera("Free Camera");
			}
			if(screen_manager_->GetInput()->is_key_down(VK_F2)) {
				camera_manager_.ChangeCamera("Security Camera, Controlled");
			}
			if(screen_manager_->GetInput()->is_key_down(VK_F3)) {
				camera_manager_.ChangeCamera("Security Camera, Automated");
			}
			if(screen_manager_->GetInput()->is_key_down(VK_F4)) {
				camera_manager_.ChangeCamera("Top Down");
			}
			if(screen_manager_->GetInput()->is_key_down(VK_F5)) {
				camera_manager_.ChangeCamera("Side view");
			}
			if(screen_manager_->GetInput()->is_key_down('P')) {
				screen_manager_->PushScreen(new PauseScreen(screen_manager_));
				screen_manager_->GetInput()->set_key_up('P');
				LittleLot::DisplayCursor(true);
				Pause(true);
			}
			earth_rotation_.y += earth_rotation_amount_*dt;
			moon_relative_rotation_.y += moon_relative_rotation_amount_*dt;
			relative_earth_rotation_.y += earth_relative_rotation_amount_*dt;
			cloud_uv_offset_ += cloud_uv_amount_*dt;
			// update rotations and positions
			camera_manager_.Update(dt);
			break;
		case kOverlayed:
			break;
		case kHidden:
			break;
		default:
			break;
	}
}

void EarthRoom::Render(Font &font) {
	switch(state_) {
		case kActive:
			RenderScene(font);
			break;
		case kOverlayed:
			RenderScene(font);
			break;
		case kHidden:
			break;
		default:
			break;
	}

}

void EarthRoom::RenderScene(Font & font) {
	
	Begin3D(*camera_manager_.GetCurrentCamera());
	//Skybox
	glPushMatrix();
		glTranslatef(camera_manager_.GetCurrentCamera()->position().x, camera_manager_.GetCurrentCamera()->position().y, camera_manager_.GetCurrentCamera()->position().z);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, main_skybox_.data());
		glDisable(GL_DEPTH_TEST);
		RenderVertexData(skybox_);
		glEnable(GL_DEPTH_TEST);
		glBindTexture(GL_TEXTURE_2D, NULL);
	glPopMatrix();

	glEnable(GL_LIGHTING);
	main_light_.Render();

	glPushMatrix();
		// offset earth axis
		glRotatef(relative_earth_rotation_.x, 1, 0, 0);
		glRotatef(relative_earth_rotation_.y, 0, 1, 0);
		glRotatef(relative_earth_rotation_.z, 0, 0, 1);
		glPushMatrix();
			// move earth into orbit
			glTranslatef(earth_position_.x, earth_position_.y, earth_position_.z);

			glPushMatrix();
				// change earths rotational axis
				glRotatef(earth_rotation_.x, 1, 0, 0);
				glRotatef(earth_rotation_.y, 0, 1, 0);
				glRotatef(earth_rotation_.z, 0, 0, 1);

				glPushMatrix();
					// earth !
					glBindTexture(GL_TEXTURE_2D, earth_texture_.data());
					glScalef(0.96f, 0.96f, 0.96f);
					RenderVertexData(cube_sphere_);
				glPopMatrix();

				glPushMatrix();
					// earth's clouds
					glMatrixMode(GL_TEXTURE);
					// translate to u,v offset
					glTranslatef(cloud_uv_offset_, 0.f, 0.0f);
					glMatrixMode(GL_MODELVIEW);
					glEnable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D, cloud_texture_.data());
					RenderVertexData(sphere_);
					glDisable(GL_BLEND);
					glBindTexture(GL_TEXTURE_2D, NULL);
					glMatrixMode(GL_TEXTURE);
					// translate back to u,v
					glTranslatef(-cloud_uv_offset_, 0.f, 0.0f);
					glMatrixMode(GL_MODELVIEW);
				glPopMatrix();

				font.Begin3DText();
				font.Render3DText(LittleLot::Vector3(0, 1.2f, 0), LittleLot::Vector3(0, 0, 0), .6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Earth - CubeSphere");
				font.Render3DText(LittleLot::Vector3(0, 1.5f, 0), LittleLot::Vector3(0, 0, 0), .6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Clouds - Long/Lat Sphere UV translation");
				font.End3DText();

			glPopMatrix();

			glPushMatrix();
					// off set moons axis from earth
				glRotatef(moon_relative_rotation_.x, 1, 0, 0);
				glRotatef(moon_relative_rotation_.y, offset_axis_.x, offset_axis_.y, offset_axis_.z);
				glRotatef(moon_relative_rotation_.z, 0, 0, 1);
				// move into orbit
				glTranslatef(moon_relative_position_.x, moon_relative_position_.y, moon_relative_position_.z);
				// change earths axis
				glRotatef(moon_axis_rotation_.x, 1, 0, 0);
				glRotatef(moon_axis_rotation_.y, 0, 1, 0);
				glRotatef(moon_axis_rotation_.z, 0, 0, 1);

				font.Begin3DText();
				font.Render3DText(LittleLot::Vector3(0, 0.5f, 0), LittleLot::Vector3(0, 0, 0), .6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE, "Moon - Geodesic sphere");
				font.End3DText();

				// the mooooooon!!! always facing the right way towards earth.
				glScalef(0.2f, 0.2f, 0.2f);
				glBindTexture(GL_TEXTURE_2D, moon_texture_.data());
				RenderVertexData(geodesic_sphere_);
				glBindTexture(GL_TEXTURE_2D, NULL);
			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	Begin2D();

	font.RenderText(LittleLot::Vector2(10, 80), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F1 - Free Cam");
	font.RenderText(LittleLot::Vector2(10, 100), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F2 - Security Camera, Controlled");
	font.RenderText(LittleLot::Vector2(10, 120), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F3 - Security Camera, Automated");
	font.RenderText(LittleLot::Vector2(10, 140), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F4 - Top Down");					
	font.RenderText(LittleLot::Vector2(10, 160), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "F5 - Side View");

	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() / 2.f, (float)screen_manager_->ScreenHeight() - 40),
					1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_CENTRE,
					"Current Camera: %s", camera_manager_.GetCurrentCameraTag().c_str());

	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 40), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "pitch %.3f", camera_manager_.GetCurrentCamera()->pitch());
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 60), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "yaw %.3f", camera_manager_.GetCurrentCamera()->yaw());
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 80), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "roll %.3f", camera_manager_.GetCurrentCamera()->roll());
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 100), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "X: %.3f", camera_manager_.GetCurrentCamera()->position().x);
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 120), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "Y: %.3f", camera_manager_.GetCurrentCamera()->position().y);
	font.RenderText(LittleLot::Vector2((float)screen_manager_->ScreenWidth() - 80, 140), 0.6f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_LEFT, "Z: %.3f", camera_manager_.GetCurrentCamera()->position().z);
	End2D();

	if((camera_manager_.GetCurrentCameraTag() == "Security Camera, Controlled") || (camera_manager_.GetCurrentCameraTag() == "Security Camera, Automated")) {
		RenderGrayScale();
	}
	
}

void EarthRoom::RenderGrayScale() {
	glDisable(GL_DEPTH_TEST);	

	GLint viewport[4];
	// Get viewport data.
	glGetIntegerv(GL_VIEWPORT, viewport);

	// height*width for array of pixels on screen
	GLubyte *pixels = new GLubyte[viewport[2] * viewport[3]];
	// Green works, read pixel data 
	glReadPixels(0, 0, viewport[2], viewport[3], GL_GREEN, GL_UNSIGNED_BYTE, pixels);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
		glLoadIdentity();
		gluOrtho2D(0.0, (GLfloat)viewport[2], 0.0, (GLfloat)viewport[3]);		

		glDrawPixels(viewport[2], viewport[3], GL_LUMINANCE, GL_UNSIGNED_BYTE, pixels);
	glPopMatrix();

	glEnable(GL_DEPTH_TEST);
	delete[] pixels;
}
