/*
	Default Material Values taken from this handy site, french dude, public domain.
	http://www.nicoptere.net/dump/materials.html
*/
#ifndef _MATERIAL_H
#define _MATERIAL_H

#include <Windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

enum DefaultMaterials {	
	kDefault,
	kBrass,
	kBronze,
	kPolishedBronze,
	kChrome,
	kCopper,	
	kGold,
	kPewter,
	kSilver,
	kEmerald,
	kJade,
	kObsidian,
	kRuby,
	kTurquoise,	
	kBlackPlastic,
	kBlackRubber,	
};

// Call if you want to reset to default opengl material.
void ApplyDefaultMaterial();

class Material {
public:	
	// Must be intiailized before use, default kBrass
	Material();	
	// Creates Default material based on the template name pased in.
	Material(DefaultMaterials selection);
	// Changes the material to the based on the template name passed in.
	void SetByDefault(DefaultMaterials selection);

	// Applies the materials settings,
	// If wanting applied to only one object call ApplyDefaultMaterial() after object rendered.
	// Will reset it to the opengl default material.
	void Apply();
	
	// The faces to generate lighting etc on. Default GL_FRONT.
	void set_face(GLenum face);
	void set_ambient(float r, float g, float b, float a);
	void set_diffuse(float r, float g, float b, float a);
	void set_specular(float r, float g, float b, float a);
	// Default {0,0,0,0}
	void set_emission(float r, float g, float b, float a);
	void set_shininess(float shininess);	

	Material& operator=(const Material& other);

protected:
	
	GLfloat ambient_[4];
	GLfloat diffuse_[4];
	GLfloat specular_[4];	
	GLfloat emission_[4];
	GLfloat shininess_;

	GLenum face_;

};
#endif
