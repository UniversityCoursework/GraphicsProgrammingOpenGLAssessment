#include "suspended_light.h"


#include "math_utils.h"
#include "utils.h"

SuspendedLight::SuspendedLight() {
	vertex_data_ = nullptr;
	light_ = nullptr;

}

SuspendedLight::SuspendedLight(VertexData * vertex_data, Material material, Light * light, LittleLot::Vector3 position) {
	vertex_data_ = vertex_data;
	material_ = material;
	position_ = position;
	light_ = light;
	rotation_ = LittleLot::Vector3::kZero;
	scale_ = LittleLot::Vector3(1, 1, 1);
	rotation_speed_ = 0.06f;
	beam_length_ = 10;
	rotation_.z = 5;
}

SuspendedLight::~SuspendedLight() {
	vertex_data_ = nullptr;
	light_ = nullptr;
}

void SuspendedLight::Update(float dt) {
	LittleLot::Vector3 up;
	float cos_r, cos_p, cos_y;
	float sin_r, sin_p, sin_y;
	cos_y = cosf(LittleLot::DegreesToRadians(-rotation_.y));
	cos_p = cosf(LittleLot::DegreesToRadians(rotation_.x));
	cos_r = cosf(LittleLot::DegreesToRadians(rotation_.z));

	sin_y = sinf(LittleLot::DegreesToRadians(-rotation_.y));
	sin_p = sinf(LittleLot::DegreesToRadians(rotation_.x));
	sin_r = sinf(LittleLot::DegreesToRadians(rotation_.z));
	// up vector 
	up.x = -cos_y * sin_r - sin_y * sin_p * cos_r;
	up.y = cos_p * cos_r;
	up.z = -sin_y * sin_r - sin_p * cos_r * -cos_y;
	// calculate light position.
		// position + beam length by rotation_ *scale
	up.Normalise();
	rotation_.y += rotation_speed_*dt;
	LittleLot::Vector3 light_position = position_ - (up*(beam_length_));
	light_->set_position(light_position.x, light_position.y, light_position.z);
	light_->set_spot_direction(-up.x, -up.y, -up.z);
	
}

void SuspendedLight::Render() {	
	glEnable(GL_NORMALIZE);

	glPushMatrix();
		material_.Apply();
		glTranslatef(position_.x, position_.y, position_.z);

		// base of light
		glPushMatrix();
			glScalef(2, 2, 2);
			RenderVertexData(*vertex_data_);
		glPopMatrix();

		glRotatef(rotation_.x, 1, 0, 0);
		glRotatef(rotation_.y, 0, 1, 0);
		glRotatef(rotation_.z, 0, 0, 1);
		glScalef(scale_.x, scale_.y, scale_.z);

		// beam arm
		glPushMatrix();
			glTranslatef(0, -beam_length_/2 , 0);
			glScalef(0.5, beam_length_, 0.5);
			RenderVertexData(*vertex_data_);
		glPopMatrix();
		// light casing
		glPushMatrix();
			glTranslatef(0, -beam_length_, 0);
			glScalef(1, 1, 1);
			RenderVertexData(*vertex_data_);
		glPopMatrix();

		ApplyDefaultMaterial();	
	glPopMatrix();

	glDisable(GL_NORMALIZE);
}
