#ifndef _GAMEOBJECT_H
#define _GAMEOBJECT_H

#include "vector3.h"

#include "model.h"
#include "vertex_data.h"

class GameObject {
public:	
		
	void set_position(LittleLot::Vector3 pos) { position_ = pos; }
	LittleLot::Vector3 &position() { return position_; }

	void set_rotation(LittleLot::Vector3 rot) { rotation_ = rot; }
	LittleLot::Vector3 &rotation() { return rotation_; }

	void set_scale(LittleLot::Vector3 scale) { scale_ = scale; }
	LittleLot::Vector3 scale() { return scale_; }
	virtual void Update(float dt) {};
	virtual void Render() = 0;
	

protected:
	LittleLot::Vector3 position_;
	LittleLot::Vector3 rotation_;	
	LittleLot::Vector3 scale_;
	
};

#endif
