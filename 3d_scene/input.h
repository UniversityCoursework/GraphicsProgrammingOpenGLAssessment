#ifndef _INPUT_H
#define _INPUT_H

#include <Windows.h>
#include <math.h>  // for calculating drag

class Input {
	struct Mouse {
		int x, y;
		bool left, right;
		bool double_left, double_right;
		int drag_x, drag_y;
	};
public:	
	void set_key_down(WPARAM);
	void set_key_up(WPARAM);	
	bool const  is_key_down(int) const;
	

	void set_mouse_x(int);
	void set_mouse_y(int);
	int const mouse_x() const;
	int const mouse_y() const;

	void set_drag_x(int);
	void set_drag_y(int);
	int const drag_x() const;
	int const drag_y() const;

	void set_mouse_left_down(bool);
	void set_mouse_right_down(bool);
	bool const is_mouse_left_down() const;
	bool const is_mouse_right_down() const;

	void set_mouse_double_left_clicked(bool);
	void set_mouse_double_right_clicked(bool);
	bool const is_mouse_double_left_clicked() const;
	bool const is_mouse_double_right_clicked() const;

	float CalculateDrag();
private:
	bool keys_[256];	
	Mouse mouse_;
};

#endif