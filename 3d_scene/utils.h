#ifndef _UTILS_H
#define _UTILS_H

#include <windows.h>
#include <sstream>

namespace LittleLot {
// Output text to output debug box Object:,Name:,Error:
// @param data_type Object followed by data type.
// @param name Name followed by name.
// @param error_message Error: followed by error_message.
inline void OutputDebugText(const char* data_type, const char* name, const char* error_message) {
//#ifdef _DEBUG	
	std::wstringstream ss;
	ss << "Object: " << data_type << std::endl
		<<"Name: " << name << std::endl
		<< "Error: " << error_message << std::endl
		<< std::endl;
	OutputDebugString(ss.str().c_str());
//#endif // DEBUG
}

// Output text to output debug box.
// @param output Just output data.
inline void OutputDebugText(const char* output) {
//#ifdef _DEBUG	
	std::wstringstream ss;
	ss <<  output << std::endl
		<< std::endl;
	OutputDebugString(ss.str().c_str());
//#endif // DEBUG
}

// Ensures it sets cursor on or off no matter where you call DisplayCursor.
// So much easier than trying to catch all your show and hides for cursor.
inline void DisplayCursor(bool val){
	int count = ShowCursor(val);
	if (val){
		while (count < 0){
			count = ShowCursor(true);
		}
	}
	else {
		while (count >= 0){
			count = ShowCursor(false);
		}
	}
}

}

#endif
