#include "model.h"

#include "utils.h"

bool Model::Load(const char* model_filename) {
	bool result;

	// Load in the model data,
	result = LoadModel(model_filename);
	if(!result) {
		LittleLot::OutputDebugText("Model", model_filename, "Failed to load");
		return false;
	}

	return true;
}

void Model::LoadTexture(const char * texture_filename) {
	// Load the texture_ for this model.
	texture_.LoadTexture(texture_filename);
}

void Model::AssignTexture(Texture & texture) {
	texture_ = texture;
}

void Model::Render() {
	glBindTexture(GL_TEXTURE_2D, texture_.data());
	RenderModel();
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void Model::RenderModel() {
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, vertex_.data());
	glNormalPointer(GL_FLOAT, 0, normals_.data());
	glTexCoordPointer(2, GL_FLOAT, 0, tex_coords_.data());

	glDrawArrays(GL_TRIANGLES, 0, vertex_.size() / 3);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

GLuint Model::texture() {
	return texture_.data();
}

bool Model::LoadModel(const char* filename) {
	std::ifstream fileStream;
	int fileSize = 0;

	fileStream.open(filename, std::ifstream::in);

	if(fileStream.is_open() == false)
		return false;

	fileStream.seekg(0, std::ios::end);
	fileSize = (int)fileStream.tellg();
	fileStream.seekg(0, std::ios::beg);

	if(fileSize <= 0)
		return false;

	char *buffer = new char[fileSize];

	if(buffer == 0)
		return false;

	memset(buffer, '\0', fileSize);

	TokenStream tokenStream, lineStream, faceStream;
	string tempLine, token;

	fileStream.read(buffer, fileSize);
	tokenStream.SetTokenStream(buffer);

	delete[] buffer;

	tokenStream.ResetStream();

	float tempx, tempy, tempz;
	vector<LittleLot::Vector3> verts, norms, texC;
	vector<int> faces;


	char lineDelimiters[2] = {'\n', ' '};

	while(tokenStream.MoveToNextLine(&tempLine)) {
		lineStream.SetTokenStream((char*)tempLine.c_str());
		tokenStream.GetNextToken(0, 0, 0);

		if(!lineStream.GetNextToken(&token, lineDelimiters, 2))
			continue;

		if(strcmp(token.c_str(), "v") == 0) {
			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempx = (float)atof(token.c_str());

			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempy = (float)atof(token.c_str());

			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempz = (float)atof(token.c_str());

			verts.push_back(LittleLot::Vector3(tempx, tempy, tempz));
		} else if(strcmp(token.c_str(), "vn") == 0) {

			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempx = (float)atof(token.c_str());

			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempy = (float)atof(token.c_str());

			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempz = (float)atof(token.c_str());

			norms.push_back(LittleLot::Vector3(tempx, tempy, tempz));
		} else if(strcmp(token.c_str(), "vt") == 0) {

			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempx = (float)atof(token.c_str());

			lineStream.GetNextToken(&token, lineDelimiters, 2);
			tempy = (float)atof(token.c_str());

			texC.push_back(LittleLot::Vector3(tempx, tempy, 0));
		} else if(strcmp(token.c_str(), "f") == 0) {

			char faceTokens[3] = {'\n', ' ', '/'};
			std::string faceIndex;

			faceStream.SetTokenStream((char*)tempLine.c_str());
			faceStream.GetNextToken(0, 0, 0);

			for(int i = 0; i < 3; i++) {
				faceStream.GetNextToken(&faceIndex, faceTokens, 3);
				faces.push_back((int)atoi(faceIndex.c_str()));

				faceStream.GetNextToken(&faceIndex, faceTokens, 3);
				faces.push_back((int)atoi(faceIndex.c_str()));

				faceStream.GetNextToken(&faceIndex, faceTokens, 3);
				faces.push_back((int)atoi(faceIndex.c_str()));
			}
		} else if(strcmp(token.c_str(), "#") == 0) {
			//skip
		}

		token[0] = '\0';
	}
	fileStream.close();
	// "Unroll" the loaded obj information into a list of triangles.

	// Model data is stored in stored in vectors verts, norms, texC and faces
	// Sort through the data and store it in the vectors provided (see header file)

	for(std::size_t face = 0; face < faces.size(); face += 3) {

		vertex_.push_back(verts[faces[face] - 1].x);
		vertex_.push_back(verts[faces[face] - 1].y);
		vertex_.push_back(verts[faces[face] - 1].z);

		tex_coords_.push_back(texC[faces[face + 1] - 1].x);
		tex_coords_.push_back(texC[faces[face + 1] - 1].y);

		normals_.push_back(norms[faces[face + 2] - 1].x);
		normals_.push_back(norms[faces[face + 2] - 1].y);
		normals_.push_back(norms[faces[face + 2] - 1].z);
	}

	verts.clear();
	norms.clear();
	texC.clear();
	faces.clear();

	return true;
}


