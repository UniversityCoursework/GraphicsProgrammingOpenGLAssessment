#include "tp_camera.h"

#include "math_utils.h"
#include "utils.h"

TPCamera::TPCamera(const LittleLot::Vector3 &object_position, const LittleLot::Vector3 &object_rotation, float boom_length)
	: object_position_(object_position), object_rotation_(object_rotation){
	boom_length_ = boom_length;

	// defaults
	up_ = LittleLot::Vector3(0, 1, 0);
	camera_offset_ = LittleLot::Vector3::kZero;
	boom_angle_ = 0.f;
	camera_follow_speed_ = 1.0f;	
}

TPCamera::TPCamera(const LittleLot::Vector3 &object_position, const LittleLot::Vector3 &object_rotation, float boom_length, LittleLot::Vector3 camera_offset, float boom_angle)
	:TPCamera(object_position,object_rotation,boom_length) {
	camera_offset_ = camera_offset;
	boom_angle_ = boom_angle;	
}



TPCamera::TPCamera(const LittleLot::Vector3 & object_position, const LittleLot::Vector3 & object_rotation, float boom_length, LittleLot::Vector3 camera_offset, float boom_angle, float camera_follow_speed)
	:TPCamera(object_position, object_rotation, boom_length, camera_offset, boom_angle) {
	camera_follow_speed_ = camera_follow_speed;
}

void TPCamera::Update(float dt) {
	
	float lerp_amount = camera_follow_speed_*dt;
	// Ensure lerp_amount is within reasonable figures, < > than -1/1 will cause to rotate past
	if(lerp_amount > 1.0f) lerp_amount = 1.0f;
	if(lerp_amount < -1.0f) lerp_amount = -1.0f;

	// allows lerping between previous rotation and new rotation
	rotation_ = LittleLot::Lerp(rotation_,object_rotation_, lerp_amount);
	//rotation_ = object_rotation_;
	
	position_.x = camera_offset_.x + sinf(LittleLot::DegreesToRadians(yaw()-180))*boom_length_ + object_position_.x;
	position_.y = camera_offset_.y + object_position_.y + sinf(LittleLot::DegreesToRadians(-pitch() + boom_angle_));
	position_.z = camera_offset_.z + cosf(LittleLot::DegreesToRadians(yaw()-180))*boom_length_ + object_position_.z;

	look_at_ = camera_offset_ + object_position_;

	float cos_y = cosf(LittleLot::DegreesToRadians(yaw()));
	float cos_p = cosf(LittleLot::DegreesToRadians(pitch()));	

	float sin_y = sinf(LittleLot::DegreesToRadians(yaw()));
	float sin_p = sinf(LittleLot::DegreesToRadians(pitch()));

	// forward vector direction of the camera
	forward_.x = sin_y * cos_p;
	forward_.y = sin_p;
	forward_.z = cos_p * -cos_y;

}

void TPCamera::OnActivated() {
	rotation_ = object_rotation_;
	LittleLot::DisplayCursor(false);
}
