#ifndef SCENE3D_H
#define SCENE3D_H

#include <windows.h>
#include <tchar.h> // _T in wndclassex declartions, and CreateWindow() Expects wide character string
#include <stdio.h>
#include <mmsystem.h>
#include <math.h>
#include <string>
#include <gl/gl.h>
#include <gl/glu.h>

#include <map>
#include <string>

#include "vector3.h"
#include "vector2.h"
#include "SOIL.h"

#include "input.h"
#include "font.h"
#include "screen_manager.h"


#define COLOUR_DEPTH 16	//Colour depth

class Scene3D {
public:
	~Scene3D();
	void Initialise(HWND* hwnd_, Input* in);	//initialse function
	void Update(float dt);
	void Render();	// render scene

	void Resize();

	void ToggleFullScreen();

protected:
	bool CreatePixelFormat(HDC hdc_);
	void ResizeGLWindow(int width, int height);	//width and height
	void InitializeOpenGL(int width, int height); // width and height
		
	void SetVSync(bool sync);

	void UpdateRenderMode();
	void SaveWindowPosition();

	void SetToFullScreen();
	void SetToWindowed();

	inline void ToggleWireFrame() {
		wireframe_render = !wireframe_render;
	}
	inline void ToggleCullFace() {
		cullface_ = !cullface_;
	}

	bool wireframe_render;
	bool cullface_;

	//vars
	HWND* hwnd_;
	Input* input_;
	RECT screen_rect_;
	HDC	hdc_;
	HGLRC hrc_;			//hardware RENDERING CONTEXT		
	
	Font font_;

	ScreenManager screen_manager_;

	// frame rate	
	float frame_rate_;
	float readable_frame_rate_;
	float ticks_;


private:

	// before fullscreen saved window position
	int prev_left_;
	int prev_top_;

	int prev_width_;
	int prev_height_;
};

#endif
