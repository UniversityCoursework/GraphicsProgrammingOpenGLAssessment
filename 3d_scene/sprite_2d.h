#ifndef _SPRITE_2D_H
#define _SPRITE_2D_H

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include "vector2.h"

#include "colour.h"

class Sprite2D {
public:
	Sprite2D();
	Sprite2D(LittleLot::Vector2 position, float half_width, float half_height, Colour colour);
	Sprite2D(LittleLot::Vector2 position, float half_width, float half_height, Colour colour, GLuint *texture);
	void set_position(float x, float y) {
		position_.x = x;
		position_.y = y;
	}
	void set_position(LittleLot::Vector2 position) {
		position_ = position;
	}
	void set_width(float half_width) {
		half_width_ = half_width;
	}
	void set_height(float half_height) {
		half_height_ = half_height;
	}	
	void set_colour(float r, float g, float b, float a) {
		colour_.set_rgba(r, g, b, a);
	}
	void set_colour(Colour colour) {
		colour_ = colour;
	}
	void set_texture(GLuint *texture) {
		texture_ = texture;
	}
	void set_rotation(float val) {
		rotation_ = val;
	}
	float rotation() { return rotation_; }

	void DrawSprite();

protected:
	LittleLot::Vector2 position_;
	float half_width_, half_height_;
	float rotation_;
	Colour colour_;
	GLuint *texture_;
		
};

#endif
