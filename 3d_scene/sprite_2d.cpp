#include "sprite_2d.h"

Sprite2D::Sprite2D() {	
	texture_ = nullptr;
	rotation_ = 0;	
}

Sprite2D::Sprite2D(LittleLot::Vector2 position, float half_width, float half_height, Colour colour)
	:Sprite2D() {
	position_ = position;
	half_width_ = half_width;
	half_height_ = half_height;
	colour_ = colour;
	
}

Sprite2D::Sprite2D(LittleLot::Vector2 position, float half_width, float half_height, Colour colour, GLuint * texture) :
	Sprite2D(position, half_width, half_height, colour) {
	texture_ = texture;
}

void Sprite2D::DrawSprite() {
	if(texture_ != nullptr) {
		glBindTexture(GL_TEXTURE_2D, *texture_);
	} else {
		glBindTexture(GL_TEXTURE_2D, NULL);
	}
	glPushMatrix();
		glTranslatef(position_.x,position_.y, 0.0f);
		glRotatef(rotation_, 0, 0, 1);
		
		glColor4f(colour_.r(), colour_.g(), colour_.b(), colour_.a());

		glBegin(GL_TRIANGLES);
		glTexCoord2f(0.f, 0.f);
		glVertex2f(-half_width_, -half_height_);
		glTexCoord2f(0.f, 1.f);
		glVertex2f(-half_width_, half_height_);
		glTexCoord2f(1.f, 0.f);
		glVertex2f(half_width_, -half_height_);
		//+
		glTexCoord2f(1.f, 1.f);
		glVertex2f(half_width_, half_height_);
		glTexCoord2f(1.f, 0.f);
		glVertex2f(half_width_, -half_height_);
		glTexCoord2f(0.f, 1.f);
		glVertex2f(-half_width_, half_height_);
		glEnd();
	glPopMatrix();
}

