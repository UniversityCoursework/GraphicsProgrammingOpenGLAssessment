#ifndef _MODEL_ROOM_H
#define _MODEL_ROOM_H

#include <vector>
#include <atomic>
#include <condition_variable>

#include "screen.h"

#include "texture.h"
#include "primitives.h"
#include "model.h"
#include "font.h"
#include "light.h"
#include "material.h"
#include "camera_manager.h"
#include "gameobject.h"
#include "vertex_data_object.h"
#include "model_object.h"
#include "character_controller.h"

#include "suspended_light.h"

#include "shadow_matrix.h"
#include "barrier.h"
#include "object_manager.h"
class ModelRoom : public Screen {
public:
	ModelRoom(ScreenManager *screen_manager);

	~ModelRoom() {};
	void Init() override;
	void CleanUp() override;

	bool IsLoading(float &percentage);

	void Resize() override;
	void Resume() override;

	void Update(float dt) override;
	void Render(Font &font) override;
	
protected:
	void RenderScene(Font &font);

	void RenderWorld();
	// Renders the already rendered scene over the top in grayscale image.
	void RenderGrayScale();
	void ForbiddenCrates();
	void BuildTableTop();
	void BuildRoom();
	CameraManager camera_manager_;

	Texture main_skybox_;
	Texture crate_tile_;
	Texture brick_;
	Texture rock_floor_;

	Texture t_tree_;
	Texture t_rock_;

	VertexData skybox_;
	VertexData plane_;
	VertexData quad_;
	VertexData wall_1_;
	VertexData wall_2_;
	VertexData floor_;
	VertexData cube_;

	Model campfire_;
	Model tree_01_;
	Model tree_02_;
	Model tree_03_;
	Model tree_04_;
	Model tree_pine_;
	Model sheep_;
	Model rock_set_01_;
	Model rock_set_02_;
	Model rock_set_03_;
	Model rock_set_04_;
	Model shock_trooper_;
	Model mech_;

	// unique rendering order so seperate from object managers
	VertexDataObject table_;
	VertexDataObject room_floor_;
	VertexDataObject window_;
		
	ObjectManager dynamic_objects_;	
	ObjectManager table_top_objects_;
	ObjectManager static_objects_;

	CharacterController character_controller_;
	ModelObject character_model_;	
	
	GLuint table_top_list_;
	GLuint room_list_;

	Light point_light_;	
	Light spot_light_;
	Light spot_light_2_;
	
	Barrier texture_load_;
	Barrier object_load_;

	Barrier creation_done_;
	
};
#endif
