#ifndef _LIGHT_H
#define _LIGHT_H

#include <Windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include "vector3.h"

enum LightType {
	kSpot,
	kPoint,
	kDirectional
};
enum LightNumber {
	kZero=0,
	kOne,
	kTwo,
	kThree,
	kFour,
	kFive,
	kSix,
	kSeven
};

class Light {
public:
	// Generates basic point light at (0,0,0), still needs to be enabled in init, once gl is initiated
	Light();
	
	Light(LightNumber light_number,LightType light_type, float x, float y, float z);
	
	void Render();
	
	void set_ambient(float r, float g, float b, float a = 1.0f);
	void set_diffuse(float r, float g, float b, float a = 1.0f);
	void set_specular(float r, float g, float b, float a = 1.0f);

	// Sets position at (x,y,z), or if directional; direction = (x,y,z)
	void set_position(float x, float y, float z);
	// If directional will give direction of light
	LittleLot::Vector3 const position() const {
		return LittleLot::Vector3(position_[0], position_[1], position_[2]);
	}
	// fill glfloat array with light position information
	void get_position(GLfloat position[4]) {
		position[0] = position_[0];
		position[1] = position_[1];
		position[2] = position_[2];
		position[3] = position_[3];
	}

	void set_spot_direction(float x, float y, float z);
	// Default zero,controls light concentration, similiar to attenuation
	void set_spot_exponent(float exponent);
	// Between 0 and 90 angle of the cone
	void set_spot_cutoff(float cutoff);

	void set_constant_attenuation(float attenuation);
	void set_linear_attenuation(float attenuation);
	void set_quadratic_attenuation(float attenuation);

	void enable();
	void disable();

	// Renders a bulb* where the light is, will add overhead to the render function.
	// Default false.
	void Debug(bool val);
	
protected:
	// all
	GLfloat ambient_[4];
	GLfloat diffuse_[4];
	GLfloat specular_[4];
	GLfloat position_[4];

	// spot light	
	GLfloat spot_direction_[3];	// direction of the spot light
	GLfloat spot_exponent_;		// default zero,controls light concentration, similiar to attenuation
	GLfloat spot_cutoff_;		// between 0 and 90 angle of the cone
	
	// fall of
	GLfloat constant_attenuation_;
	GLfloat linear_attenuation_;
	GLfloat quadratic_attenuation_;

	bool debug_render_;

	GLuint light_id_;
	LightType light_type_;

	
};


#endif
