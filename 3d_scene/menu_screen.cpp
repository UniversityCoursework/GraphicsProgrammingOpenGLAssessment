#include "menu_screen.h"

#include "screen_manager.h"


MenuScreen::MenuScreen(ScreenManager * screen_manager)
:Screen(screen_manager){
	
}

void MenuScreen::UpdateMenu(float dt) {

	// Menu Select	
	current_button_ = -1;	
	for(size_t i = 0; i < buttons_.size(); i++) {
		if(buttons_[i].ContainsPoint(LittleLot::Vector2((float)screen_manager_->GetInput()->mouse_x(), (float)screen_manager_->GetInput()->mouse_y()))) {
			// set as current selection
			current_button_ = i;
		}
	}	
	if(screen_manager_->GetInput()->is_mouse_left_down()) {
		for(size_t i = 0; i < buttons_.size(); i++) {
			if(buttons_[i].ContainsPoint(LittleLot::Vector2((float)screen_manager_->GetInput()->mouse_x(), (float)screen_manager_->GetInput()->mouse_y()))) {
				// do button stuff
				UseButton(i);
			}
		}
		screen_manager_->GetInput()->set_mouse_left_down(false);
		current_button_ = -1;
	}

	// check within buttons range
	if(current_button_ < -1) {
		current_button_ = buttons_.size();
	} else if(current_button_ >static_cast<int>(buttons_.size())) {
		current_button_ = 0;
	}

	// reset all buttons
	for(size_t i = 0; i < buttons_.size(); i++) {
		buttons_[i].set_is_active(false);
	}
	if(current_button_ >= 0) {
		buttons_[current_button_].set_is_active(true);
	}

}

void MenuScreen::RenderButtons(Font &font) {
	for(Button_It button = buttons_.begin(); button != buttons_.end(); button++) {
		button->Render(font);
	}
}
