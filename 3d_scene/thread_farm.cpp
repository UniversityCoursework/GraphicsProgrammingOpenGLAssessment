#include "thread_farm.h"

#include "utils.h"

ThreadFarm::ThreadFarm() {

}
// auto initialise just needs hrc/hdc and will derive it from the hardware.
//int i = std::thread::hardware_concurrency();
void ThreadFarm::Init(HGLRC _hrc, HDC hdc, int num_of_threads, int num_of_hrc) {
	CleanUp();
	hdc_ = hdc;
	hrc_ = _hrc;
	num_of_threads_ = num_of_threads;
	num_of_hrc_ = num_of_hrc;

	for(int i = 0; i < num_of_hrc; i++) {
		HGLRC hrc_creation= wglCreateContext(hdc_);
		_ASSERT(hrc_creation);
		hrcs_.push_back(hrc_creation);
	}
	for(HGLRC &new_hrc : hrcs_) {		
		// ? true:false; terinary used as wglShareLists returns c BOOL
		// Fixes a warning that was coming up by checking it then return a bool.
		bool share_list_failed = wglShareLists(hrc_, new_hrc) ? true : false;;
		_ASSERT(share_list_failed);
	}
	is_running_ = true;
}


ThreadFarm::~ThreadFarm() {
	CleanUp();
}

void ThreadFarm::StartThreads() {
	CleanUpThreads();
	is_running_ = true;
	for(HGLRC &new_hrc : hrcs_) {
		threads_.push_back(std::thread(std::mem_fn(&ThreadFarm::RenderContextConsumer), this, new_hrc));
	}
	for(int i = 0; i < num_of_threads_; i++) {
		threads_.push_back(std::thread(std::mem_fn(&ThreadFarm::Consumer), this));
	}	
}

void ThreadFarm::CleanUp() {	
	CleanUpThreads();
	CleanUpHrcs();	
	CleanUpTasks();
}

void ThreadFarm::QueueTask(std::function<void()> function, bool requires_hrc) {
	if(!is_running_) {
		LittleLot::OutputDebugText("Queing task to thread farm that isnt running");
		return;
	}
	if(requires_hrc) {
		{
			std::unique_lock<std::mutex> hrclock(hrcs_mutex_);
			hrc_tasks_.push(function);
		}
		hrc_cond_.notify_one();
	} else {
		{
			std::unique_lock<std::mutex> tasklock(tasks_mutex_);
			tasks_.push(function);
		}
		task_cond_.notify_one();
	}
}

void ThreadFarm::RenderContextConsumer(HGLRC hrc) {
	// Acquire hrc
	wglMakeCurrent(hdc_, hrc);
	//while(is_running_) {
	while(!hrc_tasks_.empty()) {
		std::function<void()> newfunc = GetHrcTask();
		if(!is_running_) {
			return;
		}
		newfunc();
	}
	wglMakeCurrent(NULL, NULL);
}

void ThreadFarm::Consumer() {
	//while(is_running_) {
	while(!tasks_.empty()) {
		std::function<void()> newfunc = GetTask();
		if(!is_running_) {
			return;
		}
		newfunc();
	}
}

std::function<void()> ThreadFarm::GetHrcTask() {
	std::unique_lock<std::mutex> hrclock(hrcs_mutex_);
	if(is_running_) {
		//while(is_running_){
		if(!hrc_tasks_.empty()) {
			std::function<void()> new_hrc_task = hrc_tasks_.front();
			hrc_tasks_.pop();
			return new_hrc_task;
		}
		//hrc_cond_.wait(hrclock);
	}
	return std::function<void()>();
}

std::function<void()> ThreadFarm::GetTask() {
	std::unique_lock<std::mutex> tasklock(hrcs_mutex_);
	if(is_running_) {
		//while(is_running_) {
		if(!tasks_.empty()) {
			std::function<void()> new_task = tasks_.front();
			tasks_.pop();
			return new_task;
		}
		//task_cond_.wait(tasklock);
	}
	return  std::function<void()>();
}

void ThreadFarm::CleanUpThreads() {
	is_running_ = false;
	hrc_cond_.notify_all();
	task_cond_.notify_all();
	for(std::thread &thread : threads_) {
		if(thread.joinable()) {
			thread.join();
		}
	}
	threads_.clear();
}

void ThreadFarm::CleanUpHrcs() {
	for(HGLRC &hrc : hrcs_) {
		wglDeleteContext(hrc);
	}
	hrcs_.clear();
}

void ThreadFarm::CleanUpTasks() {
	while(!tasks_.empty()) {
		tasks_.pop();
	}

	while(!hrc_tasks_.empty()) {
		hrc_tasks_.pop();
	}
}

