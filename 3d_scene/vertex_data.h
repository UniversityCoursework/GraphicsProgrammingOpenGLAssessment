#ifndef _VERTEX_DATA_H
#define _VERTEX_DATA_H

#include <vector>
#include <windows.h>
#include <gl/glu.h>

struct VertexData {
	VertexData() :uses_indices_(false) {};
	std::vector<GLuint> indices_;
	std::vector<float> verts_;
	std::vector<float> normals_;
	std::vector<float> tex_coords_;	
	// Default false.
	bool uses_indices_;	
};
void DebugAxis();
void RenderVertexData(VertexData &object);
#endif