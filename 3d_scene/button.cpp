#include "button.h"

Button::Button(LittleLot::Vector2 position, float half_width, float half_height, Colour bg_colour, Colour highlight_colour, std::string text) {
	x_ = position.x;
	y_ = position.y;
	half_width_ = half_width;
	half_height_ = half_height;
	set_position(position);

	background_.set_width(half_width);
	background_.set_height(half_height);
	background_.set_colour(bg_colour);

	highlight_.set_width(half_width);
	highlight_.set_height(half_height);
	highlight_.set_colour(highlight_colour);

	text_position_ = LittleLot::Vector2(position.x, position.y -half_height);
	text_ = text;
}

bool Button::ContainsPoint(const LittleLot::Vector2 & point) {
	if((x_ + half_width_) < (point.x)) return false;
	if((x_ - half_width_) > (point.x)) return false;
	if((y_ + half_height_) < (point.y)) return false;
	if((y_ - half_height_) > (point.y)) return false;
	return true;
}

void Button::Render(Font & font) {
	background_.DrawSprite();
	if(is_active_) {
		highlight_.DrawSprite();
	}	
	font.RenderText(LittleLot::Vector2(x_, y_), 1.0f, LittleLot::Vector3(1.0f, 1.0f, 1.0f), kTJ_VerticalCentre, text_.c_str());
}


void Button::set_position(LittleLot::Vector2 position) {
	background_.set_position(position);
	highlight_.set_position(position);
	x_ = position.x;
	y_ = position.y;
}
