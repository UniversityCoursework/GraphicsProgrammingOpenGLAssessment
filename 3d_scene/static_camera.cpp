#include "static_camera.h"

#include <cmath>

#include "math_utils.h"
#include "utils.h"

StaticCam::StaticCam(LittleLot::Vector3 position, LittleLot::Vector3 rotation) {
	position_ = position;
	rotation_ = rotation;

	float cos_r, cos_p, cos_y;
	float sin_r, sin_p, sin_y;
	// pre calculated regulary used variables
	cos_y = cosf(LittleLot::DegreesToRadians(yaw()));
	cos_p = cosf(LittleLot::DegreesToRadians(pitch()));
	cos_r = cosf(LittleLot::DegreesToRadians(roll()));

	sin_y = sinf(LittleLot::DegreesToRadians(yaw()));
	sin_p = sinf(LittleLot::DegreesToRadians(pitch()));
	sin_r = sinf(LittleLot::DegreesToRadians(roll()));

	// forward vector direction of the camera
	forward_.x = sin_y * cos_p;
	forward_.y = sin_p;
	forward_.z = cos_p * -cos_y;

	// up vector 
	up_.x = -cos_y * sin_r - sin_y * sin_p * cos_r;
	up_.y = cos_p * cos_r;
	up_.z = -sin_y * sin_r - sin_p * cos_r * -cos_y;

	right_ = forward_.CrossProduct(up_);
	look_at_ = position_ + forward_;
}

void StaticCam::Update(float dt) {
}

void StaticCam::OnActivated() {
	LittleLot::DisplayCursor(true);
}
