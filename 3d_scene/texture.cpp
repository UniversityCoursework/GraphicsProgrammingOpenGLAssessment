#include "texture.h"

#include "SOIL.h"
#include "utils.h"

#define MAX_TEXTURE_ATTEMPTS 64


Texture::~Texture() {
	glDeleteTextures(1, &texture_);
}


void Texture::LoadTexture(const char * texture_name) {	
	int attempts = 0;
	while(texture_ == 0) {
		texture_ = SOIL_load_OGL_texture(
			texture_name,
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB |
			SOIL_FLAG_COMPRESS_TO_DXT
			);

		if(texture_ == 0) {
			LittleLot::OutputDebugText("Texture", texture_name, SOIL_last_result());			
		}	
		attempts++;
		if(attempts > MAX_TEXTURE_ATTEMPTS) {
			LittleLot::OutputDebugText("Texture", texture_name,"Ran out of Attempts");
			return;
		}
	}
}
