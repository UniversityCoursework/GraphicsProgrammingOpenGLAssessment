#ifndef _MODEL_H
#define _MODEL_H

#include <Windows.h>
#include <fstream>
#include <gl/gl.h>
#include <gl/glu.h>
#include <vector>
using namespace std;

#include "token_stream.h"
#include "SOIL.h"
#include "vector3.h"
#include "texture.h"

class Model {
public:

	bool Load(const char* model_filename);
	void LoadTexture(const char* texture_filename);
	void AssignTexture(Texture &texture);
	void Render();
	void RenderModel();
	GLuint texture();

private:

	bool LoadModel(const char* model_name);	

	Texture texture_;
	vector<float> vertex_, normals_, tex_coords_;
};

#endif