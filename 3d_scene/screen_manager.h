#ifndef _SCREEN_MANAGER_H
#define _SCREEN_MANAGER_H

#include <vector>

#include "input.h"

#include "screen.h"
#include "thread_farm.h"

class ScreenManager {
	typedef std::vector<Screen*>::iterator Screen_It;
public:
	void Init(Input *input,RECT *screen_rect,HWND *hwnd, HGLRC hrc, HDC hdc);
	void CleanUp();	
	// Push a screen to the top of the stack, calling its initialise function.
	void PushScreen(Screen* screen);
	// Remove top most screen. Calling resume on screen below.
	// Will be removed after all screens updates are finished.
	void PopScreen();
	void PopScreen(int number_of_screens);

	// Causes all screens to be rezied on next render
	void Resize() { 
		screen_resized_ = true; 
	}

	void Update(float dt);
	void Render(Font &font);

	Input* const GetInput() const { return input_; }
	
	long const ScreenWidth() const { return screen_rect_->right; }
	long const ScreenHeight() const { return screen_rect_->bottom; }

	RECT* const GetScreenRect() const { return screen_rect_; }
	HWND* const GetWindowHandle() const { return hwnd_; }

	Screen_It const GetIterTopScreen() {
		return screens_.end() - 1;
	}

	ThreadFarm &thread_farm() { return thread_farm_; }
	
protected:
	std::vector<Screen* > screens_;
	
	Input* input_;
	RECT *screen_rect_;
	HWND *hwnd_;
	bool screen_resized_;

	ThreadFarm thread_farm_;

};

#endif