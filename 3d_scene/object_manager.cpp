#include "object_manager.h"

void ObjectManager::AddObject(GameObject * object) {
	objects_.push_back(object);
}

void ObjectManager::CleanUp() {
	for(auto object : objects_) {	
		delete object;
		object = nullptr;
	}
	objects_.clear();
}

void ObjectManager::Update(float dt) {
	for(auto& object : objects_) {
		object->Update(dt);
	}
}

void ObjectManager::Render() {
	for(auto& object : objects_) {
		object->Render();
	}
}
