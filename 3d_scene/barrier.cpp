#include "barrier.h"

void Barrier::Init(int barrier_count) {
	count_ = barrier_count;
}

void Barrier::NotifyOne() {
	count_--;
	cond_.notify_one();
}

void Barrier::NotifyAll() {
	count_--;
	cond_.notify_all();
}

void Barrier::Wait() {
	std::unique_lock<std::mutex> lck(mutex_);
	while(count_ > 0) {
		cond_.wait(lck);
	}
	lck.unlock();
}
