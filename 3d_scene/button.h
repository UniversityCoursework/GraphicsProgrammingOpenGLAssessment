#ifndef _BUTTON_H
#define _BUTTON_H

#include <string>

#include "vector2.h"
#include "font.h"
#include "sprite_2d.h"

// Very basic button, 2 layers simple 2-d point intersetion test
class Button {
public:
	Button() {};	
	// Set position, half_width,half_height, background and highlight colour. and the text.
	Button(LittleLot::Vector2 position, float half_width, float half_height, Colour bg_colour,Colour highlight_colour, std::string text);
		
	bool ContainsPoint(const LittleLot::Vector2 &point);
		
	// Handles rendering the seperate layers of the button.
	void Render(Font &font);

	void set_position(LittleLot::Vector2 position);

	bool is_active() { return is_active_; }
	void set_is_active(bool val) { is_active_ = val; }

protected:
	bool is_active_;
	LittleLot::Vector2 text_position_;
	std::string text_;

private:
	float x_, y_, half_width_, half_height_;
	Sprite2D background_;
	Sprite2D highlight_;

};

#endif