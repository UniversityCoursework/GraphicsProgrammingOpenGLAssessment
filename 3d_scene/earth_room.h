#ifndef _EARTH_ROOM_H
#define _EARTH_ROOM_H

#include "screen.h"

#include "texture.h"
#include "primitives.h"
#include "model.h"
#include "font.h"
#include "light.h"
#include "material.h"
#include "camera_manager.h"
#include "barrier.h"

class EarthRoom : public Screen {

public:
	EarthRoom(ScreenManager *screen_manager);

	~EarthRoom() {};
	void Init() override;
	void CleanUp() override;

	bool IsLoading(float &percentage);

	void Resize() override;
	void Resume() override;

	void Update(float dt) override;
	void Render(Font &font) override;

protected:
	CameraManager camera_manager_;
	
	Texture main_skybox_;
	Texture cloud_texture_;
	Texture earth_texture_;
	Texture moon_texture_;

	Texture sun_;
	Texture mars_;
		
	VertexData skybox_;
	VertexData cube_sphere_;
	VertexData geodesic_sphere_;
	VertexData sphere_;

	Light main_light_;

	LittleLot::Vector3 relative_earth_rotation_;
	float earth_relative_rotation_amount_;
	LittleLot::Vector3 earth_position_;
	LittleLot::Vector3 earth_rotation_;
	float earth_rotation_amount_;
	
	LittleLot::Vector3 moon_relative_position_;
	float moon_relative_rotation_amount_;	
	LittleLot::Vector3 offset_axis_;
	LittleLot::Vector3 moon_axis_rotation_;	
	LittleLot::Vector3 moon_relative_rotation_;
		
	float cloud_uv_offset_;
	float cloud_uv_amount_;

	void RenderScene(Font & font);
	// Renders the already rendered scene over the top in grayscale image.
	void RenderGrayScale();

	Barrier load_barrier_;

};
#endif

