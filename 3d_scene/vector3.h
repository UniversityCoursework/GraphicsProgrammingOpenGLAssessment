#ifndef _VECTOR3_H
#define _VECTOR3_H

namespace LittleLot {
class Vector3 {
public:

	Vector3() {
	};
	~Vector3() {
	};
	Vector3(const float new_x, const float new_y, const float new_z);


	// operator overloads, simple inline functions
	// Vector3& used when (+*/ =) means it can edit the value on the first side of the = 
	// const used on (+- etc) as just returning a value not needing to edit it

	const Vector3 operator - (const Vector3& _vec) const;
	const Vector3 operator + (const Vector3& _vec) const;
	Vector3& operator -= (const Vector3& _vec);
	Vector3& operator += (const Vector3& _vec);
	const Vector3 operator * (const float _scalar) const;
	const Vector3 operator / (const float _scalar) const;
	Vector3& operator *= (const float _scalar);
	Vector3& operator /= (const float _scalar);

	void Normalise();
	float LengthSqr() const;
	float Length() const;
	float DotProduct(const Vector3& _vec) const;
	const Vector3 CrossProduct(const Vector3& _vec) const;
	void Lerp(const Vector3& start, const Vector3& end, const float time);

	//currently dont implement matrices
	/*
	const Vector3 Transform(const class Matrix44& _mat) const;
	const Vector3 TransformNoTranslation(const class Matrix44& _mat) const;
	*/

	//variables

	float x;
	float y;
	float z;


	// unsure what this is really for?
	static const Vector3 kZero;
};

}

#include "vector3.inl"

#endif
