#ifndef _TP_CAMERA_H
#define _TP_CAMERA_H

#include <windows.h>

#include <vector>
#include "vector3.h"

#include "camera.h"

// Simple Third person Camera class.
class TPCamera : public Camera {
public:	
	// Sets camera to follow centred behind the object, with matching rotation.
	// @param boom_length distance from object.
	TPCamera(const LittleLot::Vector3 &object_position, const LittleLot::Vector3 &object_rotation, float boom_length);

	// Sets camera following object in third person.
	// @param camera_offset off sets the camera lookout and camera position, allows for the object to not be in the centre of the screen.
	// @param boom_angle angle to offset the camera position in the y-axis.
	TPCamera(const LittleLot::Vector3 &object_position, const LittleLot::Vector3 &object_rotation, float boom_length, LittleLot::Vector3 camera_offset, float boom_angle);

	// Sets camera following object in third person.
	// @param camera_offset off sets the camera lookout and camera position, allows for the object to not be in the centre of the screen.
	// @param boom_angle angle to offset the camera position in the y-axis.
	TPCamera(const LittleLot::Vector3 &object_position, const LittleLot::Vector3 &object_rotation, float boom_length, LittleLot::Vector3 camera_offset, float boom_angle, float camera_follow_speed);

	void Update(float dt) override;

	void OnActivated() override;

protected:
	const LittleLot::Vector3 &object_position_;
	const LittleLot::Vector3 &object_rotation_;
	LittleLot::Vector3 camera_offset_;
	float boom_length_;
	float boom_angle_;
	float camera_follow_speed_;
};

#endif
