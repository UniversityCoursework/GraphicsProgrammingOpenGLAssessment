#ifndef _SUSPENDED_LIGHT_H
#define _SUSPENDED_LIGHT_H

#include "gameobject.h"
#include "material.h"
#include "light.h"

class SuspendedLight : public GameObject {
public:
	
	SuspendedLight();
	SuspendedLight(VertexData* vertex_data, Material material, Light *light, LittleLot::Vector3 position);	
	~SuspendedLight();

	void set_vertex_data(VertexData *vertex_data) { vertex_data_ = vertex_data; }
	VertexData* const vertex_data() const { return vertex_data_; }

	void set_light(Light *light) { light_ = light; }
	Light* const light() const { return light_; }

	void set_material(Material material) { material_ = material; }
	Material const material() const { return material_; }

	void set_beam_length(float length) { beam_length_ = length; }
	float beam_length() { return beam_length_; }
	
	void Update(float dt) override;
	void Render() override;

protected:
	VertexData *vertex_data_;
	Light *light_;
	Material material_;

	float rotation_speed_;
	float beam_length_;
	
};

#endif
