# GraphicsProgramming - OpenGL Assessment
Old Project from second year at uni, this was also used for arhcitectures and performance assessment, focusing on the multi-threaded aspect of the application.

VS 2015 - Stable multi-threading in release, SOIL library throws error in debug.

 - Multi-threaded loading with std::thread
 - SOIL library for texture loading
 - using OpenGl for rendering.
 - Lighting
 - Planar shadows
 - Bitmap Font Renderer
 - Scene based loading
 - Model loading
 - Runtime primitives
 - Free Camera
 - 3rd Person Camera
 - Limited Rotation position Camera

## Public Domain - http://creativecommons.org/publicdomain/zero/1.0/
	http://opengameart.org/node/45590
	http://opengameart.org/content/mech-sniper
	http://opengameart.org/content/shock-trooper
	http://opengameart.org/content/free-lowpoly-rocks-set02
## CC by 3.0 - http://creativecommons.org/licenses/by-sa/3.0/
	The Chayed – KIIRA - http://opengameart.org/content/hand-painted-mountain-texture
	zvetraly_kamen - http://opengameart.org/content/voxel-sheep
	Quaternius - http://opengameart.org/content/5-low-poly-trees

Camp Fire – Alex Rowand – Flatmate, provided for free just credit.
	
